from openerp.osv import fields, osv
from datetime import datetime,timedelta
import time
from mx import DateTime
import openerp.addons.decimal_precision as dp

# ====================== Popup Class Object ================================= #


#class project_perilaku_propose_review(osv.Model):
    #_name = 'project.perilaku.propose.review'
    #_columns = {
        #'review_notes'                : fields.text('Catatan Review', required=True ),
        #}
#project_perilaku_propose_review()

class project_perilaku_propose_review_byatasan(osv.Model):
    _name = 'project.perilaku.propose.review.byatasan'
    _description = 'Realisasi Perilaku , Pengajuan Review Kegiatan'
    _columns = {
        'review_notes'                : fields.text('Catatan Review', required=True ),
        
        'review_jumlah_konsumen_pelayanan'     : fields.integer('Jumlah Pelayanan',required=True),
        'review_jumlah_tidakpuas_pelayanan'     : fields.integer('Jumlah Ketidakpuasan Pelayanan',required=True),
        'review_satuan_jumlah_konsumen_pelayanan': fields.many2one('satuan.hitung', 'Satuan Nilai Konsumen',required=True),
        'review_ketepatan_laporan_spj'     : fields.many2one('acuan.penilaian', 'Ketepatan Laporan SPJ', domain=[('type', '=', 'orientasi'),('kategori_orientasi', '=', 'ketepatan_laporan_spj'),('active', '=', True)] ),
        'review_ketepatan_laporan_ukp4'     : fields.many2one('acuan.penilaian', 'Ketepatan Laporan UKP4', domain=[('type', '=', 'orientasi'),('kategori_orientasi', '=', 'ketepatan_laporan_ukp4'),('active', '=', True)] ), 
        'review_efisiensi_biaya_operasional'     : fields.many2one('acuan.penilaian', 'Efisiensi Biaya Operasional Kantor', domain=[('type', '=', 'orientasi'),('kategori_orientasi', '=', 'efisiensi_biaya_operasional'),('active', '=', True)] ),
        
        'review_integritas_presiden': fields.boolean('Presiden'),
        'review_integritas_gubernur': fields.boolean('Gubernur'),
        'review_integritas_kepalaopd': fields.boolean('Kepala BALAI'),
        'review_integritas_atasan': fields.boolean('Atasan Langsung'),
        'review_integritas_lainlain': fields.boolean('Lain Lain'),
        'review_integritas_es1': fields.boolean('Pejabat Eselon I'),
        'review_integritas_es2': fields.boolean('Pejabat Eselon II'),
        'review_integritas_es3': fields.boolean('Pejabat Eselon III'),
        'review_integritas_es4': fields.boolean('Pejabat Eselon IV'),
        'review_integritas_hukuman': fields.selection([('ya', 'Terkena Hukuman Disiplin'), ('tidak', 'Tidak Terkena Hukuman Disiplin')],'Ada Hukuman Disiplin',help="Jika Ada Hukuman Disiplin, Atau Tidak " ,required=True),
        'review_integritas_hukuman_ringan': fields.boolean('Ringan'),
        'review_integritas_hukuman_sedang': fields.boolean('Sedang'),
        'review_integritas_hukuman_berat': fields.boolean('Berat'),
        'review_hadir_apel_pagi'     : fields.integer('Kehadiran Apel Pagi',required=True),
        'review_hadir_hari_kerja'     : fields.integer('Kehadiran Hari Kerja',required=True),
        'review_hadir_jam_kerja': fields.integer('Kehadiran Jam Kerja',required=True),
        'review_hadir_upacara_hari_besar': fields.integer('Kehadiran Upacara Hari Besar',required=True),
        'review_kerjasama_nasional': fields.boolean('Nasional'),
        'review_kerjasama_gubernur': fields.boolean('Provinsi'),
        'review_kerjasama_kepalaopd': fields.boolean('BALAI'),
        'review_kerjasama_atasan': fields.boolean('Atasan Langsung'),
        'review_kerjasama_lainlain': fields.boolean('Lain Lain'),
        'review_kerjasama_rapat_atasan': fields.boolean('Rapat Atasan Langsung'),
        'review_kerjasama_rapat_perangkat_daerah': fields.boolean('Rapat Perangkat Daerah'),
        'review_kerjasama_rapat_provinsi': fields.boolean('Rapat Provinsi'),
        'review_kerjasama_rapat_nasional': fields.boolean('Rapat Nasional'),
        'review_kepemimpinan_nasional': fields.boolean('Nasional'),
        'review_kepemimpinan_gubernur': fields.boolean('Provinsi'),
        'review_kepemimpinan_kepalaopd': fields.boolean('BALAI'),
        'review_kepemimpinan_atasan': fields.boolean('Atasan Langsung'),
        'review_kepemimpinan_lainlain': fields.boolean('Lain Lain'),
        'review_kepemimpinan_narsum_unitkerja': fields.boolean('Narasumber Kerja Atasan Langsung'),
        'review_kepemimpinan_narsum_perangkat_daerah': fields.boolean('Narasumber Perangkat Daerah'),
        'review_kepemimpinan_narsum_provinsi': fields.boolean('Narasumber Provinsi'),
        'review_kepemimpinan_narsum_nasional': fields.boolean('Narasumber Nasional'),
        'is_kepala_opd': fields.boolean('Kepala BALAI'),
        'employee_job_type':fields.char('Jenis Jabatan',size=50),
        'is_review'                : fields.boolean('Direview', ),
        'is_orientasi_pelayanan_review'                : fields.boolean('Review Orientasi Pelayanan', ),
        'is_disiplin_review'                : fields.boolean('Review Disiplin', ),
        'is_komitmen_review'                : fields.boolean('Review Komitmen', ),
        'is_integritas_review'                : fields.boolean('Review Integritas', ),
        'is_kerjasama_review'                : fields.boolean('Review Kerjasama', ),
        'is_kepemimpinan_review'                : fields.boolean('Review Kepemimpinan', ),
        'task_id'     : fields.many2one('project.perilaku', 'Realisasi', readonly=True),
        'nilai_sebelum_review': fields.float('Nilai Sebelum Review'),
        }
        
  
    def action_propose_perilaku_review(self, cr, uid, ids, context=None):
        
        vals = {}
        task_pool = self.pool.get('project.perilaku')
        review_obj = self.browse(cr, uid, ids[0], context=context)
        if review_obj.is_orientasi_pelayanan_review:
            vals.update({
                                'review_realisasi_jumlah_konsumen_pelayanan'       : review_obj.review_jumlah_konsumen_pelayanan,
                                'review_realisasi_satuan_jumlah_konsumen_pelayanan': review_obj.review_satuan_jumlah_konsumen_pelayanan.id or None,
                                'review_realisasi_jumlah_tidakpuas_pelayanan'      : review_obj.review_jumlah_tidakpuas_pelayanan,
                                'review_realisasi_ketepatan_laporan_spj'           : review_obj.review_ketepatan_laporan_spj.id or None,
                                'review_realisasi_ketepatan_laporan_ukp4'          : review_obj.review_ketepatan_laporan_ukp4.id or None,
                                'review_realisasi_efisiensi_biaya_operasional'     : review_obj.review_efisiensi_biaya_operasional.id or None,
        })
        if review_obj.is_disiplin_review:
           vals.update({
                                'review_realisasi_hadir_hari_kerja'     : review_obj.review_hadir_hari_kerja,
                                'review_realisasi_hadir_jam_kerja': review_obj.review_hadir_jam_kerja,
                                
         })
        if review_obj.is_komitmen_review:
             vals.update({                        
                                'review_realisasi_hadir_upacara_hari_besar': review_obj.review_hadir_upacara_hari_besar,
                                'review_realisasi_hadir_apel_pagi': review_obj.review_hadir_apel_pagi,
            })
        if review_obj.is_integritas_review:
            vals.update({
                                'review_realisasi_integritas_hukuman': review_obj.review_integritas_hukuman,
                                'review_realisasi_integritas_hukuman_ringan': review_obj.review_integritas_hukuman_ringan,
                                'review_realisasi_integritas_hukuman_sedang': review_obj.review_integritas_hukuman_sedang,
                                'review_realisasi_integritas_hukuman_berat': review_obj.review_integritas_hukuman_berat,
        })
            vals.update({                       
                                'review_realisasi_integritas_presiden': review_obj.review_integritas_presiden,
                                'review_realisasi_integritas_gubernur': review_obj.review_integritas_gubernur,
                                'review_realisasi_integritas_kepalaopd': review_obj.review_integritas_kepalaopd,
                                'review_realisasi_integritas_atasan': review_obj.review_integritas_atasan,
                                'review_realisasi_integritas_es1': review_obj.review_integritas_es1,
                                'review_realisasi_integritas_es2': review_obj.review_integritas_es2,
                                'review_realisasi_integritas_es3': review_obj.review_integritas_es3,
                                'review_realisasi_integritas_es4': review_obj.review_integritas_es4,
        })
        if review_obj.is_kerjasama_review:
            vals.update({
                                'review_realisasi_kerjasama_nasional': review_obj.review_kerjasama_nasional,
                                'review_realisasi_kerjasama_gubernur': review_obj.review_kerjasama_gubernur,
                                'review_realisasi_kerjasama_kepalaopd': review_obj.review_kerjasama_kepalaopd,
                                'review_realisasi_kerjasama_atasan':review_obj.review_kerjasama_atasan,
                                'review_realisasi_kerjasama_rapat_nasional': review_obj.review_kerjasama_rapat_nasional,
                                'review_realisasi_kerjasama_rapat_provinsi': review_obj.review_kerjasama_rapat_provinsi,
                                'review_realisasi_kerjasama_rapat_perangkat_daerah': review_obj.review_kerjasama_rapat_perangkat_daerah,
                                'review_realisasi_kerjasama_rapat_atasan': review_obj.review_kerjasama_rapat_atasan,
        })
        if review_obj.is_kepemimpinan_review:
            vals.update({
                                'review_realisasi_kepemimpinan_nasional': review_obj.review_kepemimpinan_nasional,
                                'review_realisasi_kepemimpinan_gubernur': review_obj.review_kepemimpinan_gubernur,
                                'review_realisasi_kepemimpinan_kepalaopd': review_obj.review_kepemimpinan_kepalaopd,
                                'review_realisasi_kepemimpinan_atasan':  review_obj.review_kepemimpinan_atasan,
                                'review_realisasi_kepemimpinan_narsum_provinsi': review_obj.review_kepemimpinan_narsum_provinsi,
                                'review_realisasi_kepemimpinan_narsum_perangkat_daerah': review_obj.review_kepemimpinan_narsum_perangkat_daerah,
                                'review_realisasi_kepemimpinan_narsum_unitkerja': review_obj.review_kepemimpinan_narsum_unitkerja,
        })
        
        vals.update({           'is_orientasi_pelayanan_review': review_obj.is_orientasi_pelayanan_review,
                                'is_disiplin_review': review_obj.is_disiplin_review,
                                'is_komitmen_review': review_obj.is_komitmen_review,
                                'is_integritas_review': review_obj.is_integritas_review,
                                'is_kerjasama_review': review_obj.is_kerjasama_review,
                                'is_kepemimpinan_review': review_obj.is_kepemimpinan_review,
                                'is_review' : True,
                                'review_notes' : review_obj.review_notes,
                                'nilai_sebelum_review' : review_obj.nilai_sebelum_review,
                                'state':'propose_to_review',
                                })               
        task_pool.write(cr, review_obj.task_id.user_id_bkd.id, [review_obj.task_id.id], vals, context)
    
project_perilaku_propose_review_byatasan()
