# -*- encoding: utf-8 -*-
##############################################################################
#
#    Darmawan Fatriananda
#    BKD Pemprov Jabar
#    Copyright (c) 2014 <http://www.asdarfat.wordpress.com.com>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see http://www.gnu.org/licenses/.
#
##############################################################################

from openerp.osv import fields, osv
from datetime import date,datetime,timedelta
import time
from mx import DateTime
from openerp import SUPERUSER_ID
#==== Res Company

# ====================== res partner ================================= #

class res_partner(osv.Model):
    _inherit = 'res.partner'
    _description ='Data Kepegawaian'
    
    def _get_is_kepala_opd(self, cr, uid, ids, field_names, args, context=None):
        res = {}
        for employee in self.browse(cr, uid, ids, context=context):
            if not employee.company_id :
                res[employee.id]=False
            else :
                company_obj = employee.company_id ;
                if not company_obj.kepala_opd :
                    res[employee.id]=False
                elif company_obj.kepala_opd.id == employee.id  :
                    res[employee.id]=True
                else :
                    res[employee.id]=False
                    
                
           
        return res
  
    def onchange_atasan_banding_nip(self, cr, uid, ids, user_id, context=None):
        res = {'value': {'user_id_banding_nip': False}}
        if user_id:
            user_obj = self.browse(cr, uid, [user_id,])
            res['value']['user_id_banding_nip'] = user_obj and user_obj[0] and user_obj[0].nip or False
        return res
    
    def onchange_atasan(self, cr, uid, ids, user_id_atasan, context=None):
        res = {'value': {'user_id_banding': False,
                         'user_id_atasan_nip': False
                         }}
        if user_id_atasan:
            user_obj = self.browse(cr, uid, [user_id_atasan,])
            res['value']['user_id_banding'] = user_obj and user_obj[0] and user_obj[0].user_id_atasan and user_obj[0].user_id_atasan.id or False
            res['value']['user_id_atasan_nip'] = user_obj and user_obj[0] and user_obj[0].nip  or False
        return res
    def _get_status_atasan_data_pegawai(self, cr, uid, ids, field_names, args, context=None):
        res = {}
        for employee in self.browse(cr, uid, ids, context=context):
            message=True
            
            if not employee.user_id_banding :
                message=False
            if not employee.user_id_atasan :
                message=False
            if not employee.company_id :
                message=False
            
            if not message:
                res[employee.id] = 'False'
            else :
                res[employee.id] = ''
        return res
    def _get_fullname(self, cr, uid, ids, field_names, args, context=None):
        res = {}
        for employee in self.browse(cr, uid, ids, context=context):
            fullname=employee.name
            
            if employee.gelar_depan and employee.gelar_depan.name :
                fullname=employee.gelar_depan.name + ' ' +fullname
            if employee.gelar_blk and employee.gelar_blk.name :
                fullname=fullname + ' '+employee.gelar_blk.name
           
            res[employee.id] = fullname
        return res
    def _get_lower_fullname(self, cr, uid, ids, field_names, args, context=None):
        res = {}
        for employee in self.browse(cr, uid, ids, context=context):
            if employee.name:
                res[employee.id] = employee.name.lower()
            
        return res
    def _current_masa_kerja(self, cr, uid, ids, name, arg, context=None):
        if context is None:
            context = {}
        res = {}
        
        masa_kerja=0
        for id in ids:
            golongan_ids=self.pool.get('partner.employee.golongan.history').search(cr,uid,[('partner_id','=',id),('jenis','=','pns')],order="date asc,id asc")
            if golongan_ids:
                golongan_hist=self.pool.get('partner.employee.golongan.history').browse(cr,uid,golongan_ids[0])
                if golongan_hist.date :
                    today = date.today()
                    awal_masa_kerja = golongan_hist.date and time.strftime('%Y', time.strptime(golongan_hist.date,'%Y-%m-%d'))
                    masa_kerja = today.year - int(awal_masa_kerja)
            res[id]=masa_kerja
        return res
    
    def _tahun_pengangkatan_pns(self, cr, uid, ids, name, arg, context=None):
        if context is None:
            context = {}
        res = {}
        tahun_awal_masa_kerja=1900
        for id in ids:
            golongan_ids=self.pool.get('partner.employee.golongan.history').search(cr,uid,[('partner_id','=',id),('jenis','=','pns')],order="date asc,id asc")
            if golongan_ids:
                golongan_hist=self.pool.get('partner.employee.golongan.history').browse(cr,uid,golongan_ids[0])
                if golongan_hist.date :
                    today = date.today()
                    tahun_awal_masa_kerja = golongan_hist.date and time.strftime('%Y', time.strptime(golongan_hist.date,'%Y-%m-%d'))
            res[id]=tahun_awal_masa_kerja
        return res
    def _sk_pengangkatan_pns(self, cr, uid, ids, name, arg, context=None):
        if context is None:
            context = {}
        res = {}
        no_sk='-'
        for id in ids:
            golongan_ids=self.pool.get('partner.employee.golongan.history').search(cr,uid,[('partner_id','=',id),('jenis','=','pns')],order="date asc,id asc")
            if golongan_ids:
                golongan_hist=self.pool.get('partner.employee.golongan.history').browse(cr,uid,golongan_ids[0])
                if golongan_hist.name :
                    no_sk = golongan_hist.name
            res[id]=no_sk
        return res
    def _current_golongan_id(self, cr, uid, ids, name, arg, context=None):
        if context is None:
            context = {}
        res = {}
        
        golongan_id=False
        for id in ids:
            golongan_ids=self.pool.get('partner.employee.golongan.history').search(cr,uid,[('partner_id','=',id)],order="date desc,id desc")
            if golongan_ids:
                golongan_hist=self.pool.get('partner.employee.golongan.history').browse(cr,uid,golongan_ids[0])
                if golongan_hist.golongan_id_history :
                    golongan_id= golongan_hist.golongan_id_history.id
            res[id]=golongan_id
        return res
    def _current_tahun_golongan_id(self, cr, uid, ids, name, arg, context=None):
        if context is None:
            context = {}
        res = {}
        
        current_tahun=1900
        for id in ids:
            golongan_ids=self.pool.get('partner.employee.golongan.history').search(cr,uid,[('partner_id','=',id)],order="date desc,id desc")
            if golongan_ids:
                golongan_hist=self.pool.get('partner.employee.golongan.history').browse(cr,uid,golongan_ids[0])
                if golongan_hist.date :
                    current_tahun = golongan_hist.date and time.strftime('%Y', time.strptime(golongan_hist.date,'%Y-%m-%d'))
            res[id]=current_tahun
        return res
    def _current_sk_golongan_id(self, cr, uid, ids, name, arg, context=None):
        if context is None:
            context = {}
        res = {}
        
        no_sk='-'
        for id in ids:
            golongan_ids=self.pool.get('partner.employee.golongan.history').search(cr,uid,[('partner_id','=',id)],order="date desc,id desc")
            if golongan_ids:
                golongan_hist=self.pool.get('partner.employee.golongan.history').browse(cr,uid,golongan_ids[0])
                if golongan_hist.name :
                    no_sk = golongan_hist.name
            res[id]=no_sk
        return res
    def _current_jenjang_pendidikan_id(self, cr, uid, ids, name, arg, context=None):
        if context is None:
            context = {}
        res = {}
        lookup_pool=self.pool.get('partner.employee.study.history')
        pendidikan_id=False
        for id in ids:
            pendidikan_ids=lookup_pool.search(cr,uid,[('partner_id','=',id),],order="tahun_lulus desc,id desc")
            if pendidikan_ids:
                pendidikan_hist=lookup_pool.browse(cr,uid,pendidikan_ids[0])
                if  pendidikan_hist.jenjang_pendidikan_id_history :
                    pendidikan_id=  pendidikan_hist.jenjang_pendidikan_id_history.id
            res[id]=pendidikan_id
        return res
    def _current_nama_sekolah_id(self, cr, uid, ids, name, arg, context=None):
        if context is None:
            context = {}
        res = {}
        lookup_pool=self.pool.get('partner.employee.study.history')
        pendidikan_id=False
        for id in ids:
            pendidikan_ids=lookup_pool.search(cr,uid,[('partner_id','=',id)],order="tahun_lulus desc,id desc")
            if pendidikan_ids:
                pendidikan_hist=lookup_pool.browse(cr,uid,pendidikan_ids[0])
                if  pendidikan_hist.nama_sekolah_id_history :
                    pendidikan_id=  pendidikan_hist.nama_sekolah_id_history.id
            res[id]=pendidikan_id
        return res
    def _current_tahun_pendidikan(self, cr, uid, ids, name, arg, context=None):
        if context is None:
            context = {}
        res = {}
        lookup_pool=self.pool.get('partner.employee.study.history')
        tahun_lulus=1900
        for id in ids:
            pendidikan_ids=lookup_pool.search(cr,uid,[('partner_id','=',id)],order="tahun_lulus desc,id desc")
            if pendidikan_ids:
                pendidikan_hist=lookup_pool.browse(cr,uid,pendidikan_ids[0])
                if  pendidikan_hist.tahun_lulus :
                    tahun_lulus=  pendidikan_hist.tahun_lulus
            res[id]=tahun_lulus
        return res
    def _current_jurusan_id(self, cr, uid, ids, name, arg, context=None):
        if context is None:
            context = {}
        res = {}
        lookup_pool=self.pool.get('partner.employee.study.history')
        pendidikan_id=False
        for id in ids:
            pendidikan_ids=lookup_pool.search(cr,uid,[('partner_id','=',id)],order="tahun_lulus desc,id desc")
            if pendidikan_ids:
                pendidikan_hist=lookup_pool.browse(cr,uid,pendidikan_ids[0])
                if  pendidikan_hist.jurusan_id_history :
                    pendidikan_id=  pendidikan_hist.jurusan_id_history.id
            res[id]=pendidikan_id
        return res
    def _current_hukuman_disiplin(self, cr, uid, ids, name, arg, context=None):
        #jika True = Tidak Terkena Hukuman Disiplin, False = Terkena Hukuman Disiplin
        if context is None:
            context = {}
        res = {}
        lookup_pool=self.pool.get('partner.employee.hukuman.disiplin.history')
        status_hukuman_disiplin=True
        for id in ids:
            today = date.today()
            hukuman_disiplin_ids=lookup_pool.search(cr,uid,[('partner_id','=',id),('date','<=',today),('date_end','>=',today)])
            if hukuman_disiplin_ids:
                hukuman_hist=lookup_pool.browse(cr,uid,hukuman_disiplin_ids[0])
                if  hukuman_hist:
                    status_hukuman_disiplin=  False
            res[id]=status_hukuman_disiplin
        return res
    
    #jabatan default
    def _current_job_type(self, cr, uid, ids, name, arg, context=None):
        if context is None:
            context = {}
        res = {}
        
        
        val=None
        for id in ids:
            hist_ids=self.pool.get('partner.employee.job.history').search(cr,uid,[('partner_id','=',id)],order="date desc,id desc")
            if hist_ids:
                hist_obj=self.pool.get('partner.employee.job.history').browse(cr,uid,hist_ids[0])
                if hist_obj and hist_obj.job_type :
                    val = hist_obj.job_type
            res[id]=val
        return res
    def _current_eselon_id(self, cr, uid, ids, name, arg, context=None):
        if context is None:
            context = {}
        res = {}
        
        val=None
        for id in ids:
            hist_ids=self.pool.get('partner.employee.job.history').search(cr,uid,[('partner_id','=',id)],order="date desc,id desc")
            if hist_ids:
                hist_obj=self.pool.get('partner.employee.job.history').browse(cr,uid,hist_ids[0])
                if hist_obj and hist_obj.eselon_id :
                    val = hist_obj.eselon_id.id
            res[id]=val
        return res
    def _current_biro_id(self, cr, uid, ids, name, arg, context=None):
        if context is None:
            context = {}
        res = {}
        
        val=None
        for id in ids:
            hist_ids=self.pool.get('partner.employee.job.history').search(cr,uid,[('partner_id','=',id)],order="date desc,id desc")
            if hist_ids:
                hist_obj=self.pool.get('partner.employee.job.history').browse(cr,uid,hist_ids[0])
                if hist_obj and hist_obj.biro_id :
                    val = hist_obj.biro_id.id
            res[id]=val
        return res
    def _current_job_id(self, cr, uid, ids, name, arg, context=None):
        if context is None:
            context = {}
        res = {}
        
        val=None
        for id in ids:
            hist_ids=self.pool.get('partner.employee.job.history').search(cr,uid,[('partner_id','=',id)],order="date desc,id desc")
            if hist_ids:
                hist_obj=self.pool.get('partner.employee.job.history').browse(cr,uid,hist_ids[0])
                if hist_obj and hist_obj.job_id_history :
                    val = hist_obj.job_id_history.id
            res[id]=val
        return res
    def _current_department_id(self, cr, uid, ids, name, arg, context=None):
        if context is None:
            context = {}
        res = {}
        val=None
        for id in ids:
            hist_ids=self.pool.get('partner.employee.job.history').search(cr,uid,[('partner_id','=',id)],order="date desc,id desc")
            if hist_ids:
                hist_obj=self.pool.get('partner.employee.job.history').browse(cr,uid,hist_ids[0])
                if hist_obj and hist_obj.department_id :
                    val = hist_obj.department_id.id
            res[id]=val
        return res
    
    def _get_usia(self, cr, uid, ids, name, arg, context=None):
        if context is None:
            context = {}
        res = {}
        
        usia=0
        today = date.today()
        for record in self.read(cr,uid,ids,["id","tanggal_lahir"],context):
            if record['tanggal_lahir'] :
                tahun_lahir = time.strftime('%Y', time.strptime(record['tanggal_lahir'],'%Y-%m-%d'))
                usia = today.year - int(tahun_lahir)
            res[record['id']]=usia
        return res
    #attachment :
    def _current_sk_golongan_id(self, cr, uid, ids, name, arg, context=None):
        if context is None:
            context = {}
        res = {}
        
        attachment_file=False
        for id in ids:
            golongan_ids=self.pool.get('partner.employee.golongan.history').search(cr,uid,[('partner_id','=',id)],order="date desc,id desc")
            if golongan_ids:
                golongan_hist=self.pool.get('partner.employee.golongan.history').browse(cr,uid,golongan_ids[0])
                if golongan_hist.golongan_id_history and golongan_hist.attachment_file:
                    attachment_file= golongan_hist.attachment_file
            res[id]=attachment_file
        return res
    def _current_ijazah_pendidikan_id(self, cr, uid, ids, name, arg, context=None):
        if context is None:
            context = {}
        res = {}
        lookup_pool=self.pool.get('partner.employee.study.history')
        attachment_file=False
        for id in ids:
            pendidikan_ids=lookup_pool.search(cr,uid,[('partner_id','=',id),],order="tahun_lulus desc,id desc")
            if pendidikan_ids:
                pendidikan_hist=lookup_pool.browse(cr,uid,pendidikan_ids[0])
                if  pendidikan_hist.jenjang_pendidikan_id_history and pendidikan_hist.attachment_file:
                    attachment_file=  pendidikan_hist.attachment_file
            res[id]=attachment_file
        return res
    job_type_list =[('struktural', 'Jabatan Struktural'), ('jft', 'Jabatan Fungsional Tertentu'), ('jfu', 'Jabatan Fungsional Umum')]
    
    
    _columns = {
        'nip'     : fields.char('NIP',size=20),    
        'fullname' :fields.function(_get_fullname, method=True, string='Nama Lengkap Beserta Gelar', type='char', readonly=True,store=False),
        'lower_fullname' :fields.function(_get_lower_fullname, method=True, string='Nama Lengkap Beserta Gelar', type='char', readonly=True,store=True),
        
        'tempat_lahir'     : fields.char('Tempat Lahir',size=128),
        'tanggal_lahir'     : fields.date('Tanggal Lahir'),
        'agama'     : fields.selection([('islam', 'Islam'),  
                                                      ('katolik', 'Katolik'), 
                                                      ('protestan', 'Protestan'), 
                                                      ('hindu', 'Hindu'), 
                                                      ('budha', 'Budha') 
                                                     ,
                                                     ],'Agama', 
                                                     ),
        'department_id': fields.function(_current_department_id, method=True,readonly=True , store=True,
                                         type="many2one",relation='partner.employee.department', string='Unit Kerja'),
        'job_id': fields.function(_current_job_id, method=True,readonly=True , store=True,
                                         type="many2one",relation='partner.employee.job', string='Jabatan'),
        'eselon_id': fields.function(_current_eselon_id, method=True,readonly=True , store=True,
                                         type="many2one",relation='partner.employee.eselon', string='Eselon'),
        'job_type': fields.function(_current_job_type, method=True,readonly=True , store=True,
                                         type="selection",selection=job_type_list, string='Jenis Jabatan'),
        'biro_id': fields.many2one('partner.employee.biro', 'Biro'),
        
        
        'user_id_atasan': fields.many2one('res.partner', 'Pejabat Penilai' ,domain="[('employee','=',True)]" ),
        'user_id_atasan_nip': fields.related('user_id_atasan','nip', type='char', string='NIP Pejabat Penilai', store=False,readonly=True),
        'user_id_banding': fields.many2one('res.partner', 'Atasan Pejabat Penilai' ,domain="[('employee','=',True)]"),   
        'user_id_banding_nip': fields.related('user_id_banding','nip', type='char', string='NIP Atasan Pejabat Penilai', store=False,readonly=True),
   
        'gelar_depan': fields.many2one('partner.employee.title', 'Gelar Depan',domain="[('title_type','=','gelar_depan')]"),
        'gelar_blk': fields.many2one('partner.employee.title', 'Gelar Belakang',domain="[('title_type','=','gelar_belakang')]"),   
             
         
         'tmt_cpns'     : fields.date('TMT CPNS'),
         'tmt_pns'     : fields.date('TMT PNS'),
         'tmt_golongan_akhir'     : fields.date('TMT Gol.Akhir'),
         'nip_lama'     : fields.char('NIP Lama',size=20),    
         'tahun_lulus'     : fields.char('Tahun Lulus',size=4),
         'is_head_of_all': fields.boolean('Walikota', ),
         'is_share_users': fields.boolean('Shared', ),
         'data_preparation': fields.boolean('Data Preparation', ),
         'status_data_atasan_pegawai' :fields.function(_get_status_atasan_data_pegawai, method=True, string='Kelengkapan Data Atasan', type='char', readonly=True,store=False),
         #'status_data_pegawai' :fields.function(_get_status_data_pegawai, method=True, string='Kelengkapan Data', type='char', readonly=True,store=False),
         'jenis_kelamin': fields.selection([('L', 'Laki-Laki'), ('P', 'Perempuan'), ], 'Jenis Kelamin'),
         'is_kepala_opd': fields.boolean('Kepala BALAI', readonly=True),
         #'is_kepala_opd' :fields.function(_get_is_kepala_opd, method=True, string='Kepala BALAI', type='boolean', readonly=True,store=True),
         
         'golongan_id': fields.function(_current_golongan_id, method=True,readonly=True , store=True,
                                         type="many2one",relation='partner.employee.golongan', string='Pangkat/Golongan'),
         'golongan_id_history': fields.one2many('partner.employee.golongan.history','partner_id', 'Riwayat Kepangkatan'),
         'tingkat_pendidikan': fields.function(_current_jenjang_pendidikan_id, method=True,readonly=True , store=True,
                                         type="many2one",relation='partner.employee.study.type', string='Tingkat Pendidikan'),
         'nama_sekolah': fields.function(_current_nama_sekolah_id, method=True,readonly=True , store=True,
                                         type="many2one",relation='partner.employee.school', string='Sekolah'),
         'jurusan': fields.function(_current_jurusan_id, method=True,readonly=True , store=True,
                                         type="many2one",relation='partner.employee.study', string='Jurusan Pendidikan'),
         'tahun_lulus': fields.function(_current_tahun_pendidikan, method=True,readonly=True , store=True,
                                         type="char", string='Tahun Lulus'),
         'jenjang_pendidikan_id_history': fields.one2many('partner.employee.study.history','partner_id', 'Riwayat Pendidikan'),
         'masa_kerja': fields.function(_current_masa_kerja, method=True,readonly=True , store=True,
                                         type="integer",string='Masa Kerja'),
        'tahun_pengangkatan_pns': fields.function(_tahun_pengangkatan_pns, method=True,readonly=True , store=True,
                                         type="char",string='Tahun Pengangkatan PNS'),
        'sk_pengangkatan_pns': fields.function(_sk_pengangkatan_pns, method=True,readonly=True , store=True,
                                         type="char",string='NO SK Pengangkatan PNS'),
        'current_tahun_golongan_id': fields.function(_current_tahun_golongan_id, method=True,readonly=True , store=True,
                                         type="char",string='Tahun SK Pangkat Terakhir'),
        'current_sk_golongan_id': fields.function(_current_sk_golongan_id, method=True,readonly=True , store=True,
                                         type="char",string='NO SK Pangkat Terakhir'),
        'status_hukuman_disiplin': fields.function(_current_hukuman_disiplin, method=True,readonly=True , store=True,
                                         type="boolean", string='Status Tidak Ada Hukuman Disiplin'),
        'hukuman_disiplin_id_history': fields.one2many('partner.employee.hukuman.disiplin.history','partner_id', 'Riwayat Hukuman Disiplin'),
        
        'job_id': fields.function(_current_job_id, method=True,readonly=True , store=True,
                                         type="many2one",relation='partner.employee.job', string='Jabatan'),
        'job_id_history': fields.one2many('partner.employee.job.history','partner_id', 'Riwayat Jabatan'),
        'usia': fields.function(_get_usia, method=True,readonly=True , store=False,
                                         type="integer",string='Usia'),
        #diklat
        'diklat_kepemimpinan_id_history': fields.one2many('partner.employee.diklat.kepemimpinan.history','partner_id',string='Riwayat Diklat Kepemimpinan'),
        'diklat_fungsional_id_history': fields.one2many('partner.employee.diklat.fungsional.history','partner_id', string='Riwayat Diklat Fungsional'),
        'diklat_teknik_id_history': fields.one2many('partner.employee.diklat.teknik.history','partner_id' ,string='Riwayat Diklat Teknik'),
        'penataran_id_history': fields.one2many('partner.employee.penataran.history','partner_id', string='Riwayat Penataran'),
        'bidang_kompetensi_id_history': fields.one2many('partner.employee.bidang.kompetensi.history','partner_id' ,string='Riwayat Bidang Kompetensi'),
        
        #attach file
        'sk_golongan_id': fields.function(_current_sk_golongan_id, method=True,readonly=True , store=False,
                                         type="binary",string='SK Pangkat Terakhir'),
        'ijazah_pendidikan_id': fields.function(_current_ijazah_pendidikan_id, method=True,readonly=True , store=False,
                                         type="binary",string='Ijazah Terakhir'),
                
        
    }
    _defaults = {
        'employee' : True,
        'is_head_of_all' : False,
        'data_preparation':False,
    }
    _sql_constraints = [
         ('name_uniq', 'unique (nip)',
             'Data Tidak Bisa Dimasukan, NIP Sudah Tersedia')
     ]
    def init(self, cr):
        pass

res_partner()
