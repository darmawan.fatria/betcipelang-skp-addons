# -*- encoding: utf-8 -*-
##############################################################################
#
#    Darmawan Fatriananda
#    BKD Pemprov Jabar
#    Copyright (c) 2014 <http://www.asdarfat.wordpress.com.com>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see http://www.gnu.org/licenses/.
#
##############################################################################

from openerp.osv import fields, osv
from openerp.tools.translate import _
from datetime import datetime,timedelta
import time
import openerp.addons.decimal_precision as dp

class project_perilaku(osv.Model):
    _inherit = 'project.perilaku'
  
    
    _columns = {
        'state'                 : fields.selection([('draft', 'Draft'), ('realisasi', 'Realisasi'),
                                    ('propose', 'Atasan'), ('rejected_manager', 'Pengajuan Ditolak Atasan'),
                                    ('appeal', 'Banding'), ('evaluated', 'Verifikatur'), ('rejected_bkd', 'Pengajuan Ditolak Verifikatur'),
                                    ('propose_to_close','Pengajuan Closing Target'),('closed','Closed'),
                                    ('propose_to_review','Review'),
                                    ('done', 'Selesai'), 
                                    ('cancelled', 'Cancel')],
                                   'Status Pekerjaan'),
        'is_review'                : fields.boolean('Direview', readonly=True),
        'is_orientasi_pelayanan_review'                : fields.boolean('Review Orientasi Pelayanan', readonly=True),
        'is_disiplin_review'                : fields.boolean('Review Disiplin', readonly=True),
        'is_komitmen_review'                : fields.boolean('Review Komitmen', readonly=True),
        'is_integritas_review'                : fields.boolean('Review Integritas', readonly=True),
        'is_kerjasama_review'                : fields.boolean('Review Kerjasama', readonly=True),
        'is_kepemimpinan_review'                : fields.boolean('Review Kepemimpinan', readonly=True),
        
        'review_realisasi_jumlah_konsumen_pelayanan'     : fields.integer('Jumlah Pelayanan',readonly=True),
        'review_realisasi_jumlah_tidakpuas_pelayanan'     : fields.integer('Jumlah Ketidakpuasan Pelayanan',readonly=True),
        'review_realisasi_satuan_jumlah_konsumen_pelayanan': fields.many2one('satuan.hitung', 'Satuan Nilai Konsumen',readonly=True),
        'review_realisasi_ketepatan_laporan_spj'     : fields.many2one('acuan.penilaian', 'Ketepatan Laporan SPJ', domain=[('type', '=', 'orientasi'),('kategori_orientasi', '=', 'ketepatan_laporan_spj'),('active', '=', True)] , readonly=True),
        'review_realisasi_ketepatan_laporan_ukp4'     : fields.many2one('acuan.penilaian', 'Ketepatan Laporan UKP4', domain=[('type', '=', 'orientasi'),('kategori_orientasi', '=', 'ketepatan_laporan_ukp4'),('active', '=', True)] , readonly=True), 
        'review_realisasi_efisiensi_biaya_operasional'     : fields.many2one('acuan.penilaian', 'Efisiensi Biaya Operasional Kantor', domain=[('type', '=', 'orientasi'),('kategori_orientasi', '=', 'efisiensi_biaya_operasional'),('active', '=', True)] , readonly=True),
        'review_realisasi_apel_pagi'     : fields.integer('Jumlah Apel Pagi', readonly=True),
        'review_realisasi_hadir_apel_pagi'     : fields.integer('Kehadiran Apel Pagi', readonly=True),
        'review_realisasi_upacara_hari_besar': fields.integer('Upacara Hari Besar', readonly=True),
        'review_realisasi_integritas_presiden': fields.boolean('Presiden', readonly=True),
        'review_realisasi_integritas_gubernur': fields.boolean('Gubernur', readonly=True),
        'review_realisasi_integritas_kepalaopd': fields.boolean('Kepala BALAI', readonly=True),
        'review_realisasi_integritas_atasan': fields.boolean('Atasan Langsung', readonly=True),
        'review_realisasi_integritas_lainlain': fields.boolean('Lain Lain', readonly=True),
        'review_realisasi_integritas_es1': fields.boolean('Pejabat Eselon I', readonly=True),
        'review_realisasi_integritas_es2': fields.boolean('Pejabat Eselon II', readonly=True),
        'review_realisasi_integritas_es3': fields.boolean('Pejabat Eselon III', readonly=True),
        'review_realisasi_integritas_es4': fields.boolean('Pejabat Eselon IV', readonly=True),
        'review_realisasi_integritas_hukuman': fields.selection([('ya', 'Terkena Hukuman Disiplin'), ('tidak', 'Tidak Terkena Hukuman Disiplin')],'Ada Hukuman Disiplin',help="Jika Ada Hukuman Disiplin, Atau Tidak ", readonly=True),#fields.boolean('Ada Hukuman Disiplin',help="Jika Ada Hukuman Disiplin, Ceklis Di Kotak Ini "),
        'review_realisasi_integritas_hukuman_ringan': fields.boolean('Ringan', readonly=True),
        'review_realisasi_integritas_hukuman_sedang': fields.boolean('Sedang', readonly=True),
        'review_realisasi_integritas_hukuman_berat': fields.boolean('Berat', readonly=True),
        'review_realisasi_hadir_hari_kerja'     : fields.integer('Kehadiran Hari Kerja', readonly=True),
        'review_realisasi_hadir_jam_kerja': fields.integer('Kehadiran Jam Kerja', readonly=True),
        'review_realisasi_jumlah_hari_kerja'     : fields.integer('Jumlah Hari Kerja', readonly=True),
        'review_realisasi_jumlah_jam_kerja': fields.integer('Jumlah Jam Kerja', readonly=True),
        'review_realisasi_hadir_upacara_hari_besar': fields.integer('Kehadiran Upacara Hari Besar', readonly=True),
        'review_realisasi_kerjasama_nasional': fields.boolean('Nasional', readonly=True),
        'review_realisasi_kerjasama_gubernur': fields.boolean('Provinsi', readonly=True),
        'review_realisasi_kerjasama_kepalaopd': fields.boolean('BALAI', readonly=True),
        'review_realisasi_kerjasama_atasan': fields.boolean('Atasan Langsung', readonly=True),
        'review_realisasi_kerjasama_lainlain': fields.boolean('Lain Lain', readonly=True),
        'review_realisasi_kerjasama_rapat_atasan': fields.boolean('Rapat Atasan Langsung', readonly=True),
        'review_realisasi_kerjasama_rapat_perangkat_daerah': fields.boolean('Rapat Perangkat Daerah', readonly=True),
        'review_realisasi_kerjasama_rapat_provinsi': fields.boolean('Rapat Provinsi', readonly=True),
        'review_realisasi_kerjasama_rapat_nasional': fields.boolean('Rapat Nasional', readonly=True),
        'review_realisasi_kepemimpinan_nasional': fields.boolean('Nasional', readonly=True),
        'review_realisasi_kepemimpinan_gubernur': fields.boolean('Provinsi', readonly=True),
        'review_realisasi_kepemimpinan_kepalaopd': fields.boolean('BALAI', readonly=True),
        'review_realisasi_kepemimpinan_atasan': fields.boolean('Atasan Langsung', readonly=True),
        'review_realisasi_kepemimpinan_lainlain': fields.boolean('Lain Lain', readonly=True),
        'review_realisasi_kepemimpinan_narsum_unitkerja': fields.boolean('Narasumber Kerja Atasan Langsung', readonly=True),
        'review_realisasi_kepemimpinan_narsum_perangkat_daerah': fields.boolean('Narasumber Perangkat Daerah', readonly=True),
        'review_realisasi_kepemimpinan_narsum_provinsi': fields.boolean('Narasumber Provinsi', readonly=True),
        'review_realisasi_kepemimpinan_narsum_nasional': fields.boolean('Narasumber Nasional', readonly=True),
        
        'review_notes'                : fields.text('Catatan Review',  readonly=True),
        'nilai_sebelum_review': fields.float('Nilai Sebelum Review', readonly=True),
    }
    def action_propose_perilaku_review_by_atasan(self, cr, uid, ids, context=None):
        if not ids: return []
        if self.is_manager(cr, uid, ids, context):
            task_obj = self.browse(cr, uid, ids[0], context=context)
            dummy, view_id = self.pool.get('ir.model.data').get_object_reference(cr, uid, 'df_project_consolidation', 'action_perilaku_propose_review_byatasan_popup_form_view')
            return {
                    'name':_("Pengajuan Review Perilaku"),
                    'view_mode': 'form',
                    'view_id': view_id,
                    'view_type': 'form',
                    'res_model': 'project.perilaku.propose.review.byatasan',
                    'type': 'ir.actions.act_window',
                    'nodestroy': True,
                    'target': 'new',
                    'domain': '[]',
                    'context': {
                                'default_review_jumlah_konsumen_pelayanan'       : task_obj.realisasi_jumlah_konsumen_pelayanan,
                                'default_review_satuan_jumlah_konsumen_pelayanan': task_obj.realisasi_satuan_jumlah_konsumen_pelayanan.id or None,
                                'default_review_jumlah_tidakpuas_pelayanan'      : task_obj.realisasi_jumlah_tidakpuas_pelayanan,
                                'default_review_ketepatan_laporan_spj'           : task_obj.realisasi_ketepatan_laporan_spj.id or None,
                                'default_review_ketepatan_laporan_ukp4'          : task_obj.realisasi_ketepatan_laporan_ukp4.id or None,
                                'default_review_efisiensi_biaya_operasional'     : task_obj.realisasi_efisiensi_biaya_operasional.id or None,
                                
                                'default_review_hadir_upacara_hari_besar': task_obj.realisasi_hadir_upacara_hari_besar,
                                'default_review_hadir_hari_kerja'     : task_obj.realisasi_hadir_hari_kerja,
                                'default_review_hadir_jam_kerja': task_obj.realisasi_hadir_jam_kerja,
                                'default_review_hadir_apel_pagi': task_obj.realisasi_hadir_apel_pagi,
                                
                                'default_review_integritas_presiden': task_obj.realisasi_integritas_presiden,
                                'default_review_integritas_gubernur': task_obj.realisasi_integritas_gubernur,
                                'default_review_integritas_kepalaopd': task_obj.realisasi_integritas_kepalaopd,
                                'default_review_integritas_atasan': task_obj.realisasi_integritas_atasan,
                                'default_review_integritas_es1': task_obj.realisasi_integritas_es1,
                                'default_review_integritas_es2': task_obj.realisasi_integritas_es2,
                                'default_review_integritas_es3': task_obj.realisasi_integritas_es3,
                                'default_review_integritas_es4': task_obj.realisasi_integritas_es4,
                                
                                'default_review_integritas_hukuman': task_obj.realisasi_integritas_hukuman,
                                'default_review_integritas_hukuman_ringan': task_obj.realisasi_integritas_hukuman_ringan,
                                'default_review_integritas_hukuman_sedang': task_obj.realisasi_integritas_hukuman_sedang,
                                'default_review_integritas_hukuman_berat': task_obj.realisasi_integritas_hukuman_berat,
                                
                                'default_review_kerjasama_nasional': task_obj.realisasi_kerjasama_nasional,
                                'default_review_kerjasama_gubernur': task_obj.realisasi_kerjasama_gubernur,
                                'default_review_kerjasama_kepalaopd': task_obj.realisasi_kerjasama_kepalaopd,
                                'default_review_kerjasama_atasan':task_obj.realisasi_kerjasama_atasan,
                                'default_review_kerjasama_rapat_nasional': task_obj.realisasi_kerjasama_rapat_nasional,
                                'default_review_kerjasama_rapat_provinsi': task_obj.realisasi_kerjasama_rapat_provinsi,
                                'default_review_kerjasama_rapat_perangkat_daerah': task_obj.realisasi_kerjasama_rapat_perangkat_daerah,
                                'default_review_kerjasama_rapat_atasan': task_obj.realisasi_kerjasama_rapat_atasan,
                                
                                'default_review_kepemimpinan_nasional': task_obj.realisasi_kepemimpinan_nasional,
                                'default_review_kepemimpinan_gubernur': task_obj.realisasi_kepemimpinan_gubernur,
                                'default_review_kepemimpinan_kepalaopd': task_obj.realisasi_kepemimpinan_kepalaopd,
                                'default_review_kepemimpinan_atasan':  task_obj.realisasi_kepemimpinan_atasan,
                                'default_review_kepemimpinan_narsum_nasional': task_obj.realisasi_kepemimpinan_narsum_nasional,
                                'default_review_kepemimpinan_narsum_provinsi': task_obj.realisasi_kepemimpinan_narsum_provinsi,
                                'default_review_kepemimpinan_narsum_perangkat_daerah': task_obj.realisasi_kepemimpinan_narsum_perangkat_daerah,
                                'default_review_kepemimpinan_narsum_unitkerja': task_obj.realisasi_kepemimpinan_narsum_unitkerja,
                                
                                    'default_is_kepala_opd': task_obj.is_kepala_opd,
                                    'default_employee_job_type': task_obj.employee_job_type,
                                    'default_is_review': True,
                                    'default_task_id':task_obj.id,
                                    'default_nilai_sebelum_review':task_obj.nilai_akhir,
                    }
                }
            
        return False
    
    

    def action_review_accepted_byatasan(self, cr, uid, ids, context=None):
        """ Review Dari Atasan Diterima Lalu Hitung Ulang 
        """
        vals = {}
        if self.is_verificator(cr, uid, ids, context) :
            #self.do_recalculate_poin(cr, uid, ids, context=context) #must a list
            for task_obj in self.browse(cr,uid,ids,context=context):
                vals = {}
                # do is komitmen
                isNotSame = False
                if task_obj.realisasi_hadir_apel_pagi < task_obj.realisasi_apel_pagi :
                    isNotSame = True
                if  task_obj.realisasi_hadir_hari_kerja < task_obj.realisasi_jumlah_hari_kerja :
                    isNotSame = True
                if  task_obj.realisasi_hadir_jam_kerja < task_obj.realisasi_jumlah_jam_kerja :
                    isNotSame = True
                if task_obj.realisasi_hadir_upacara_hari_besar < task_obj.realisasi_upacara_hari_besar :
                    isNotSame = True
                #do recalculation

                vals.update({
                                        'nilai_akhir': 0,
                                        'indeks_nilai': False,
                                        'jumlah_perhitungan':0,
                                        'nilai_pelayanan': 0,
                                        'nilai_integritas': 0,
                                        'nilai_komitmen':0,
                                        'nilai_disiplin': 0,
                                        'nilai_kerjasama': 0,
                                        'nilai_kepemimpinan':0,
                                        'use_target_for_calculation': False,
                                        'state':'evaluated',
                                        'komitmen_verifikasi_notsame':isNotSame,
                })

                if task_obj.is_orientasi_pelayanan_review:
                    vals.update({
                                        'realisasi_jumlah_konsumen_pelayanan'       : task_obj.review_realisasi_jumlah_konsumen_pelayanan,
                                        'realisasi_satuan_jumlah_konsumen_pelayanan': task_obj.review_realisasi_satuan_jumlah_konsumen_pelayanan.id or None,
                                        'realisasi_jumlah_tidakpuas_pelayanan'      : task_obj.review_realisasi_jumlah_tidakpuas_pelayanan,
                                        'realisasi_ketepatan_laporan_spj'           : task_obj.review_realisasi_ketepatan_laporan_spj.id or None,
                                        'realisasi_ketepatan_laporan_ukp4'          : task_obj.review_realisasi_ketepatan_laporan_ukp4.id or None,
                                        'realisasi_efisiensi_biaya_operasional'     : task_obj.review_realisasi_efisiensi_biaya_operasional.id or None,
                })
                if task_obj.is_disiplin_review:
                   vals.update({
                                        'realisasi_hadir_hari_kerja'     : task_obj.review_realisasi_hadir_hari_kerja,
                                        'realisasi_hadir_jam_kerja': task_obj.review_realisasi_hadir_jam_kerja,
                                        
                 })
                if task_obj.is_komitmen_review:
                     vals.update({                        
                                        'realisasi_hadir_upacara_hari_besar': task_obj.review_realisasi_hadir_upacara_hari_besar,
                                        'realisasi_hadir_apel_pagi': task_obj.review_realisasi_hadir_apel_pagi,
                    })
                if task_obj.is_integritas_review:
                    vals.update({
                                        'realisasi_integritas_hukuman': task_obj.review_realisasi_integritas_hukuman,
                                        'realisasi_integritas_hukuman_ringan': task_obj.review_realisasi_integritas_hukuman_ringan,
                                        'realisasi_integritas_hukuman_sedang': task_obj.review_realisasi_integritas_hukuman_sedang,
                                        'realisasi_integritas_hukuman_berat': task_obj.review_realisasi_integritas_hukuman_berat,
                })
                    vals.update({                       
                                        'realisasi_integritas_presiden': task_obj.review_realisasi_integritas_presiden,
                                        'realisasi_integritas_gubernur': task_obj.review_realisasi_integritas_gubernur,
                                        'realisasi_integritas_kepalaopd': task_obj.review_realisasi_integritas_kepalaopd,
                                        'realisasi_integritas_atasan': task_obj.review_realisasi_integritas_atasan,
                                        'realisasi_integritas_es1': task_obj.review_realisasi_integritas_es1,
                                        'realisasi_integritas_es2': task_obj.review_realisasi_integritas_es2,
                                        'realisasi_integritas_es3': task_obj.review_realisasi_integritas_es3,
                                        'realisasi_integritas_es4': task_obj.review_realisasi_integritas_es4,
                })
                if task_obj.is_kerjasama_review:
                    vals.update({
                                        'realisasi_kerjasama_nasional': task_obj.review_realisasi_kerjasama_nasional,
                                        'realisasi_kerjasama_gubernur': task_obj.review_realisasi_kerjasama_gubernur,
                                        'realisasi_kerjasama_kepalaopd': task_obj.review_realisasi_kerjasama_kepalaopd,
                                        'realisasi_kerjasama_atasan':task_obj.review_realisasi_kerjasama_atasan,
                                        'realisasi_kerjasama_rapat_nasional': task_obj.review_realisasi_kerjasama_rapat_nasional,
                                        'realisasi_kerjasama_rapat_provinsi': task_obj.review_realisasi_kerjasama_rapat_provinsi,
                                        'realisasi_kerjasama_rapat_perangkat_daerah': task_obj.review_realisasi_kerjasama_rapat_perangkat_daerah,
                                        'realisasi_kerjasama_rapat_atasan': task_obj.review_realisasi_kerjasama_rapat_atasan,
                })
                if task_obj.is_kepemimpinan_review:
                    vals.update({
                                        'realisasi_kepemimpinan_nasional': task_obj.review_realisasi_kepemimpinan_nasional,
                                        'realisasi_kepemimpinan_gubernur': task_obj.review_realisasi_kepemimpinan_gubernur,
                                        'realisasi_kepemimpinan_kepalaopd': task_obj.review_realisasi_kepemimpinan_kepalaopd,
                                        'realisasi_kepemimpinan_atasan':  task_obj.review_realisasi_kepemimpinan_atasan,
                                        'realisasi_kepemimpinan_narsum_provinsi': task_obj.review_realisasi_kepemimpinan_narsum_provinsi,
                                        'realisasi_kepemimpinan_narsum_perangkat_daerah': task_obj.review_realisasi_kepemimpinan_narsum_perangkat_daerah,
                                        'realisasi_kepemimpinan_narsum_unitkerja': task_obj.review_realisasi_kepemimpinan_narsum_unitkerja,
                })
                self.write(cr, uid, task_obj.id, vals, context=context)
            self.action_done(cr, uid, ids, context=context) #must a list
        return True;
    def action_review_rejected(self, cr, uid, ids, context=None):
        """ Review Dinilai Status Kembali Ke Done
        """
        vals = {}
        if self.is_verificator(cr, uid, ids, context) :
            vals.update({            'is_review' : False,
                                     'is_orientasi_pelayanan_review' : False,
                                     'is_disiplin_review' : False,
                                     'is_komitmen_review' : False,
                                     'is_integritas_review' : False,
                                     'is_kerjasama_review' : False,
                                     'is_kepemimpinan_review' : False,
                                     
                                    'review_notes' : None,
                                    'nilai_sebelum_review':None,
                                    
                                      
                                     })
            self.write(cr, uid, ids, vals, context=context)
            return self.set_done(cr, uid, ids, context=context)
project_perilaku()


