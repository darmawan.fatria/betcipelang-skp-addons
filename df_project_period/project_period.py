# -*- encoding: utf-8 -*-
##############################################################################
#
#    Darmawan Fatriananda
#    BKD Pemprov Jabar
#    Copyright (c) 2014 <http://www.asdarfat.wordpress.com.com>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see http://www.gnu.org/licenses/.
#
##############################################################################

from openerp.osv import fields, osv
from openerp.tools.translate import _
from datetime import datetime,timedelta
import time
from mx import DateTime


class project_period(osv.Model):
    _name = 'project.period'
    _description = 'Periode Pelaporan Kegiatan'
    _columns = {
        'name'             : fields.char('Nama',size=50),  
        'target_period_year'     : fields.char('Periode Tahun', size=4, required=True),
        'target_period_month'     : fields.selection([('01', 'Januari'), ('02', 'Februari'),
                                                      ('03', 'Maret'), ('04', 'April'),
                                                      ('05', 'Mei'), ('06', 'Juni'),
                                                      ('07', 'Juli'), ('08', 'Agustus'),
                                                      ('09', 'September'), ('10', 'Oktober'),
                                                      ('11', 'November'), ('12', 'Desember')], 'Periode Bulan'
                                                     , required=True),
        'realisasi_start_date'           : fields.date('Batas Awal Isi Realisasi', required=True),
        'realisasi_end_date'           : fields.date('Batas Akhir Isi Realisasi', required=True),
        #'propose_start_date'           : fields.date('Batas Awal Penilaian Realisasi', required=True),
        'propose_end_date'           : fields.date('Batas Akhir Penilaian Realisasi', required=True),
        #'banding_start_date'           : fields.date('Banding Awal Penilaian Realisasi', required=True),
        'banding_end_date'           : fields.date('Batas Akhir Banding Realisasi', required=True),
        
        'review_start_date'           : fields.date('Batas Awal Review Realisasi', required=True),
        'review_end_date'           : fields.date('Batas Akhir Review Realisasi', required=True),
        
        
        'active'           : fields.boolean('Aktif'),
    }
    _defaults ={
                'active' : 'True',
                }
project_period()


class project_skp(osv.Model):
    _inherit = 'project.skp'
    
    def write(self, cr, uid, ids, vals, context=None):
        if not isinstance(ids, list): ids = [ids]
        period_pool = self.pool.get('project.period')
        #print "vals : ",vals
        if vals.get('state',False):
            now=time.strftime('%Y-%m-%d')
            new_state = vals.get('state',False)
            for project_skp_obj in self.browse(cr,uid,ids,context=None) :
                target_period_month = project_skp_obj.target_period_month
                target_period_year = project_skp_obj.target_period_year
                current_state = project_skp_obj.state
                #print "period state : ",target_period_year+target_period_month
                #print "current state : ",current_state
                #print "new state : ",new_state
                project_period_ids=['x'] #default true
                if current_state in ('realisasi','rejected_manager') and new_state in ('propose','appeal'): # untuk pegawai
                    project_period_ids = period_pool.search(cr,uid,[('target_period_year', '=', target_period_year), ('target_period_month', '=',target_period_month),
                                                                ('realisasi_start_date', '<=', now), ('realisasi_end_date', '>=',now)
                                                                ],context=None)
                    
                elif current_state in ('propose','rejected_bkd','rejected_manager') and new_state in ('evaluated') : #untuk atasan
                    project_period_ids = period_pool.search(cr,uid,[('target_period_year', '=', target_period_year), ('target_period_month', '=',target_period_month),
                                                                ('realisasi_start_date', '<=', now), ('propose_end_date', '>=',now),
                                                                ],context=None)
                elif current_state in ('appeal')  and new_state in ('evaluated'): #untuk atasan banding
                    project_period_ids = period_pool.search(cr,uid,[('target_period_year', '=', target_period_year), ('target_period_month', '=',target_period_month),
                                                                ('realisasi_start_date', '<=', now), ('banding_end_date', '>=',now),
                                                                ],context=None)
                elif current_state in ('done') and new_state in ('propose_to_review') : #review
                    project_period_ids = period_pool.search(cr,uid,[('target_period_year', '=', target_period_year), ('target_period_month', '=',target_period_month),
                                                                ('review_start_date', '<=', now), ('review_end_date', '>=',now),
                                                                ],context=None)
                   
                
                if not project_period_ids:
                      raise osv.except_osv(_('Proses Tidak Dapat Dilanjutkan!'),
                                          _('Periode pelaporan melewati batas yang sudah ditentukan.'))
                        
        return super(project_skp, self).write(cr, uid, ids, vals, context=context)
        
project_skp()

class project_perilaku(osv.Model):
    _inherit = 'project.perilaku'
    
    def write(self, cr, uid, ids, vals, context=None):
        if not isinstance(ids, list): ids = [ids]
        period_pool = self.pool.get('project.period')
        #print "vals : ",vals
        if vals.get('state',False):
            now=time.strftime('%Y-%m-%d')
            new_state = vals.get('state',False)
            for project_perilaku_obj in self.browse(cr,uid,ids,context=None) :
                target_period_month = project_perilaku_obj.target_period_month
                target_period_year = project_perilaku_obj.target_period_year
                current_state = project_perilaku_obj.state
                #print "period state : ",target_period_year+target_period_month
                #print "current state : ",current_state
                #print "new state : ",new_state
                project_period_ids=['x'] #default true
                if current_state in ('realisasi','rejected_manager') and new_state in ('propose','appeal'): # untuk pegawai
                    project_period_ids = period_pool.search(cr,uid,[('target_period_year', '=', target_period_year), ('target_period_month', '=',target_period_month),
                                                                ('realisasi_start_date', '<=', now), ('realisasi_end_date', '>=',now)
                                                                ],context=None)
                    
                elif current_state in ('propose','rejected_bkd','rejected_manager') and new_state in ('evaluated') : #untuk atasan
                    project_period_ids = period_pool.search(cr,uid,[('target_period_year', '=', target_period_year), ('target_period_month', '=',target_period_month),
                                                                ('realisasi_start_date', '<=', now), ('propose_end_date', '>=',now),
                                                                ],context=None)
                elif current_state in ('appeal')  and new_state in ('evaluated'): #untuk atasan banding
                    project_period_ids = period_pool.search(cr,uid,[('target_period_year', '=', target_period_year), ('target_period_month', '=',target_period_month),
                                                                ('realisasi_start_date', '<=', now), ('banding_end_date', '>=',now),
                                                                ],context=None)
                elif current_state in ('done') and new_state in ('propose_to_review') : #review
                    project_period_ids = period_pool.search(cr,uid,[('target_period_year', '=', target_period_year), ('target_period_month', '=',target_period_month),
                                                                ('review_start_date', '<=', now), ('review_end_date', '>=',now),
                                                                ],context=None)
                   
                
                if not project_period_ids:
                      raise osv.except_osv(_('Proses Tidak Dapat Dilanjutkan!'),
                                          _('Periode pelaporan melewati batas yang sudah ditentukan.'))
                        
        return super(project_perilaku, self).write(cr, uid, ids, vals, context=context)
        
project_perilaku()

class project_tambahan_kreatifitas(osv.Model):
    _inherit = 'project.tambahan.kreatifitas'
    
    def write(self, cr, uid, ids, vals, context=None):
        if not isinstance(ids, list): ids = [ids]
        period_pool = self.pool.get('project.period')
        #print "vals : ",vals
        if vals.get('state',False):
            now=time.strftime('%Y-%m-%d')
            new_state = vals.get('state',False)
            for project_tambahan_obj in self.browse(cr,uid,ids,context=None) :
                target_period_month = project_tambahan_obj.target_period_month
                target_period_year = project_tambahan_obj.target_period_year
                current_state = project_tambahan_obj.state
                #print "period state : ",target_period_year+target_period_month
                #print "current state : ",current_state
                #print "new state : ",new_state
                project_period_ids=['x'] #default true
                if current_state in ('realisasi','rejected_manager') and new_state in ('propose','appeal'): # untuk pegawai
                    project_period_ids = period_pool.search(cr,uid,[('target_period_year', '=', target_period_year), ('target_period_month', '=',target_period_month),
                                                                ('realisasi_start_date', '<=', now), ('realisasi_end_date', '>=',now)
                                                                ],context=None)
                    
                elif current_state in ('propose','rejected_bkd','rejected_manager') and new_state in ('evaluated') : #untuk atasan
                    project_period_ids = period_pool.search(cr,uid,[('target_period_year', '=', target_period_year), ('target_period_month', '=',target_period_month),
                                                                ('realisasi_start_date', '<=', now), ('propose_end_date', '>=',now),
                                                                ],context=None)
                elif current_state in ('appeal')  and new_state in ('evaluated'): #untuk atasan banding
                    project_period_ids = period_pool.search(cr,uid,[('target_period_year', '=', target_period_year), ('target_period_month', '=',target_period_month),
                                                                ('realisasi_start_date', '<=', now), ('banding_end_date', '>=',now),
                                                                ],context=None)
                elif current_state in ('done') and new_state in ('propose_to_review') : #review
                    project_period_ids = period_pool.search(cr,uid,[('target_period_year', '=', target_period_year), ('target_period_month', '=',target_period_month),
                                                                ('review_start_date', '<=', now), ('review_end_date', '>=',now),
                                                                ],context=None)
                   
                
                if not project_period_ids:
                      raise osv.except_osv(_('Proses Tidak Dapat Dilanjutkan!'),
                                          _('Periode pelaporan melewati batas yang sudah ditentukan.'))
                        
        return super(project_tambahan_kreatifitas, self).write(cr, uid, ids, vals, context=context)
        
project_tambahan_kreatifitas()
