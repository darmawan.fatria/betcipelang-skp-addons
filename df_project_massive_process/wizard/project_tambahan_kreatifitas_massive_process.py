from openerp.osv import fields, osv
from openerp.tools.translate import _
from openerp import pooler

class project_tambahan_kreatifitas_massive(osv.Model):
    _name = "project.tambahan.kreatifitas.massive"
    _description = "Proses Select All Tambahan Dan Kreatifitas"
    def verify_all_task(self, cr, uid, ids, context=None):
        
        if context is None:
            context = {}
        pool_obj = pooler.get_pool(cr.dbname)
        task_pool = pool_obj.get('project.tambahan.kreatifitas')
        task_ids = task_pool.read(cr, uid, context['active_ids'], ['state','employee_id',], context=context)
        
        update_task_ids=[]
        for record in task_ids:
            if not record['employee_id'] :
                continue;
            if record['state'] not in ('evaluated'):
                continue;
            if task_pool.get_auth_id(cr, uid, record['id'], 'user_id_bkd', context):
                update_task_ids.append(record['id'])
        
        if update_task_ids:
           task_pool.action_done(cr, uid, update_task_ids, context)
                
                
        return {'type': 'ir.actions.act_window_close'}
   

project_tambahan_kreatifitas_massive()


class project_tambahan_kreatifitas_temp_calc_massive(osv.Model):
    _name = "project.tambahan.kreatifitas.temp.calc.massive"
    _description = "Proses Select All Hitung Nilai Sementara Tambahan Dan Kreatifitas"
    def poin_calculation_temporary_all_task(self, cr, uid, ids, context=None):
        
        if context is None:
            context = {}
        pool_obj = pooler.get_pool(cr.dbname)
        task_pool = pool_obj.get('project.tambahan.kreatifitas')
        task_ids = task_pool.read(cr, uid, context['active_ids'], ['state','employee_id',], context=context)
        
        update_task_ids=[]
        for record in task_ids:
            if not record['employee_id'] :
                continue;
            if record['state'] not in ('evaluated'):
                continue;
            
            update_task_ids.append(record['id'])
        
        if update_task_ids:
           task_pool.do_task_poin_calculation_temporary(cr, uid, update_task_ids, context)
                
                
        return {'type': 'ir.actions.act_window_close'}
   

project_tambahan_kreatifitas_temp_calc_massive()


class project_tambahan_kreatifitas_evaluated_massive(osv.Model):
    _name = "project.tambahan.kreatifitas.evaluated.massive"
    _description = "Proses Select All Tugas Tambahan Kreatifitas | Atasan-BKD"
    def evaluated_all_task(self, cr, uid, ids, context=None):
        
        if context is None:
            context = {}
        pool_obj = pooler.get_pool(cr.dbname)
        task_pool = pool_obj.get('project.tambahan.kreatifitas')
        task_ids = task_pool.read(cr, uid, context['active_ids'], ['state',], context=context)
        
        update_task_ids=[]
        for record in task_ids:
            if record['state'] not in ('propose'):
                continue;
            update_task_ids.append(record['id'])
        
        if update_task_ids:
           task_pool.set_evaluated(cr, 1, update_task_ids, context)
                
                
        return {'type': 'ir.actions.act_window_close'}
   

project_tambahan_kreatifitas_evaluated_massive()

