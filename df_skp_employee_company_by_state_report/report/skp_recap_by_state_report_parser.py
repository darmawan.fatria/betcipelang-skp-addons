import openerp.pooler
import time
from datetime import datetime
import openerp.tools
import logging
from openerp.report import report_sxw
from datetime import datetime
from openerp.addons.report_webkit import webkit_report
from openerp.tools.translate import _
from openerp.osv import osv
import math
import locale

class skp_recap_by_state_report_parser(report_sxw.rml_parse):

    def __init__(self, cr, uid, name, context=None):
        super(skp_recap_by_state_report_parser, self).__init__(cr, uid, name, context=context)

    def get_skp_recap_by_state_report_raw(self,filters,context=None):
        
        period_month=filters['form']['period_month']
        period_year=filters['form']['period_year']
        company_id=filters['form']['company_id']
        biro_id=filters['form']['biro_id']
        kepala_opd=filters['form']['is_kepala_opd']
        data_filter= [] 
        
        param_company_id = company_id[0] #because required
        company_id_state = False
        if company_id and company_id[0] == 1:
            company_id_state=True
        
            
        domain = period_month,period_year,period_month,period_year,period_month,period_year,period_month,period_year,period_month,period_year,period_month,period_year \
                 ,company_id_state,param_company_id
         
        query = """
                select p.name,p.nip,
                   c.name BALAI,
                   biro.name biro,
                   dept.name Unit_Kerja,
                   job.name Jabatan,
                   eselon.name Eselon,
                   golongan.name Golongan,
                   p.job_type,
                   sum(realisasi.cnt) skp_realisasi,
                   sum(atasan.cnt) skp_atasan,
                   sum(bkd.cnt) skp_bkd,
                   sum(done.cnt) skp_done,
                   sum(skp_all.cnt) skp_all,
                   sum(skp_emp.nilai_skp_percent) nilai_skp,
                   sum(skp_emp.nilai_perilaku_percent) nilai_perilaku,
                   sum(skp_emp.fn_nilai_tambahan) nilai_tugas_tambahan,
                   sum(skp_emp.fn_nilai_kreatifitas) nilai_kreatifitas,
                   sum(skp_emp.nilai_total) nilai_total,
                   p.user_id
            from res_partner p
            left join res_company c on c.id = p.company_id
            left join partner_employee_biro biro on biro.id = p.biro_id
            left join partner_employee_department dept on dept.id = p.department_id
            left join partner_employee_job job on  job.id = p.job_id
            left join partner_employee_eselon eselon on  eselon.id = p.eselon_id
            left join partner_employee_golongan golongan on  golongan.id = p.golongan_id
            left join 
        ( select user_id, count(id) cnt from project_skp 
            where state='realisasi'
                            and target_period_month = %s and target_period_year = %s and active
                  group by user_id
            )
            realisasi on realisasi.user_id = p.user_id
            left join 
        ( select user_id, count(id) cnt from project_skp 
            where state='propose'
                            and target_period_month = %s and target_period_year = %s and active
                  group by user_id
            )
            atasan on atasan.user_id = p.user_id
            left join 
        ( select user_id, count(id) cnt from project_skp 
            where state='evaluated'
                            and target_period_month = %s and target_period_year = %s and active
                  group by user_id
            )
            bkd on bkd.user_id = p.user_id
            left join 
        ( select user_id, count(id) cnt from project_skp 
            where state='done'
                            and target_period_month = %s and target_period_year = %s and active
                  group by user_id
            )
            done on done.user_id = p.user_id
            left join 
        ( select user_id, count(id) cnt from project_skp 
            where state!='cancelled'
                            and target_period_month = %s and target_period_year = %s and active
                  group by user_id
            )
            skp_all on skp_all.user_id = p.user_id                              
            left join skp_employee skp_emp on skp_emp.user_id = p.user_id
                            and skp_emp.target_period_month = %s and skp_emp.target_period_year =  %s              
            where p.employee 
            and   p.user_id notnull
            and    (%s OR p.company_id = %s)
            group by p.name,p.nip,
                   c.name,
                   biro.name,
                   dept.name,
                   job.name,
                   eselon.name,
                   golongan.name,p.job_type,
                   p.user_id
        """
        list_data = []
        self.cr.execute(query,domain)
        result = self.cr.fetchall()
        no_idx=1;
        for  employee_name,employee_nip,company_name,biro,unit_kerja,jabatan,eselon,golongan,job_type, \
        count_skp_realisasi,count_skp_atasan,count_skp_bkd,count_skp_done,count_skp_all, \
        nilai_skp,nilai_perilaku,nilai_tugas_tambahan,nilai_kreatifitas,nilai_total,\
        user_id in result:
            new_dict = {}
            new_dict['no_idx'] = no_idx
            new_dict['employee_name'] = employee_name
            new_dict['employee_nip'] = employee_nip
            new_dict['company_name'] = company_name
            new_dict['biro'] = biro
            new_dict['unit_kerja'] = unit_kerja
            new_dict['jabatan'] = jabatan
            new_dict['eselon'] = eselon
            new_dict['golongan'] = golongan
            new_dict['job_type'] = job_type
            new_dict['count_skp_realisasi'] = count_skp_realisasi
            new_dict['count_skp_atasan'] = count_skp_atasan
            new_dict['count_skp_bkd'] = count_skp_bkd
            new_dict['count_skp_done'] = count_skp_done
            new_dict['count_skp_all'] = count_skp_all
            new_dict['nilai_skp'] = nilai_skp
            new_dict['nilai_perilaku'] = nilai_perilaku
            new_dict['nilai_tugas_tambahan'] = nilai_tugas_tambahan
            new_dict['nilai_kreatifitas'] = nilai_kreatifitas
            new_dict['nilai_total'] = nilai_total
            new_dict['user_id'] = user_id
            
            skp_state_count=''
            if not count_skp_all:
                count_skp_all=0
                skp_state_count='Tidak Ada Kegiatan'
            if not count_skp_done:
                count_skp_done=0
                    
            if count_skp_all and count_skp_all==0:
                skp_state_count='Tidak Ada Kegiatan'
            elif count_skp_all > 0 and count_skp_all == count_skp_done:
                skp_state_count='Semua Kegiatan Selesai'
            elif count_skp_all > 0 :
                try:
                    skp_state_count= str(int(count_skp_done)) + '/'  +str(int(count_skp_all)) +' Kegiatan'
                except:
                    yyy=1+1
            
            new_dict['skp_state_count'] = skp_state_count
            list_data.append(new_dict)
            no_idx+=1
        return list_data
    def get_opd_name_filter(self,prefiks,filters):
        if not filters['form']['company_id'] : return prefiks;

        title_company_name = filters['form']['company_id'][1]
        return prefiks+ " " + title_company_name
    def get_title(self,prefiks,filters):
        if not filters['form']['company_id'] : return prefiks;

        title_company_name = filters['form']['company_id'][1]
        return prefiks+ " " + title_company_name
    def get_period(self,filters):
        bulan ='';
        year='';
        if filters['form']['period_month']:
            month = filters['form']['period_month']
            if month == '10':
                bulan = "Oktober"
            elif month == '11':
                bulan = "November"
            elif month == '12':
                bulan = "Desember"
            elif month == '01':
                bulan = "Januari"
            elif month == '02':
                bulan = "Februari"
            elif month == '03':
                bulan = "Maret"
            elif month == '04':
                bulan = "April"
            elif month == '05':
                bulan = "Mei"
            elif month == '06':
                bulan = "Juni"
            elif month == '07':
                bulan = "Juli"
            elif month == '08':
                bulan = "Agustus"
            elif month == '09':
                bulan = "September"
        if filters['form']['period_year']:
            year = filters['form']['period_year']
        return bulan+ " " + year
        
   
    
