# -*- encoding: utf-8 -*-
##############################################################################
#
#    Darmawan Fatriananda
#    BKD Pemprov Jabar
#    Copyright (c) 2014 <http://www.asdarfat.wordpress.com.com>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see http://www.gnu.org/licenses/.
#
##############################################################################

from datetime import datetime, date
from lxml import etree
import time

from openerp import SUPERUSER_ID
from openerp import tools
from openerp.osv import fields, osv
import openerp.addons.decimal_precision as dp
from openerp import workflow
from openerp.tools.translate import _

class project_skp(osv.Model):
    _name = 'project.skp'
    _description='Realisasi Kegiatan SKP' 
    
    _inherit = ['mail.thread', 'ir.needaction_mixin']
    def write(self, cr, uid, ids, vals, context=None):
        if not isinstance(ids, list): ids = [ids]
        for task_obj in self.browse(cr, uid, ids, context=context):
            task_id = task_obj.id
            if not task_obj.project_state :
                if uid != SUPERUSER_ID:
                    if task_obj.state in ('draft', 'realisasi', 'rejected_manager'):
                        if not self.is_user(cr, uid, ids, context) :
                            return False
                    if task_obj.state in ('propose', 'rejected_bkd'):
                        if not self.is_manager(cr, uid, ids, context) :
                            return False
                    if task_obj.state in ('appeal'):
                        if not self.is_appeal_manager(cr, uid, ids, context) :
                            return False
                    if task_obj.state in ('evaluated'):
                        if not self.is_verificator(cr, uid, ids, context) :
                            return False
                    if task_obj.state in ('done', 'cancelled'):
                        if not self.is_verificator(cr, uid, ids, context) :
                            return False
            elif  task_obj.project_state == 'confirm' :   
                if uid != SUPERUSER_ID:
                    if task_obj.state in ('draft', 'realisasi', 'rejected_manager', 'cancelled'):
                        if not self.is_user(cr, uid, ids, context) :
                            return False
                    if task_obj.state in ('propose', 'rejected_bkd'):
                        if not self.is_manager(cr, uid, ids, context) :
                            return False
                    if task_obj.state in ('appeal'):
                        if not self.is_appeal_manager(cr, uid, ids, context) :
                            return False
                    if task_obj.state in ('evaluated'):
                        if not self.is_verificator(cr, uid, ids, context) :
                            return False
                    if task_obj.state in ('done'):
                        if not self.is_verificator(cr, uid, ids, context) :
                            return False
            elif  task_obj.project_state == 'closed' :   
                if uid != SUPERUSER_ID:
                    if task_obj.state in ('draft', 'realisasi', 'rejected_manager', 'cancelled'):
                        if not self.is_user(cr, uid, ids, context) :
                            return False
                    if task_obj.state in ('propose', 'rejected_bkd'):
                        if not self.is_manager(cr, uid, ids, context) :
                            return False
                    if task_obj.state in ('appeal'):
                        if not self.is_appeal_manager(cr, uid, ids, context) :
                            return False
                    if task_obj.state in ('evaluated'):
                        if not self.is_verificator(cr, uid, ids, context) :
                            return False
                    if task_obj.state in ('done','closed'):
                        if not self.is_verificator(cr, uid, ids, context) :
                            return False
        super(project_skp, self).write(cr, uid, ids, vals, context=context)           
        return True
    
    def _get_status_suggest_realiasi_notsame(self, cr, uid, ids, name, arg, context=None):
        res = {}
        if not ids:
            return res
        for target in self.browse(cr, uid, ids, context=context):
            res[target.id] = False
            suggest_realiasi_notsame = False
            if target.suggest_jumlah_kuantitas_output != 0 and target.suggest_jumlah_kuantitas_output != target.target_jumlah_kuantitas_output :
                suggest_realiasi_notsame = True
            if target.suggest_kualitas != 0 and target.suggest_kualitas != target.target_kualitas :
                suggest_realiasi_notsame = True
            if target.suggest_waktu != 0 and target.suggest_waktu != target.target_waktu :
                suggest_realiasi_notsame = True
            if target.suggest_biaya != 0 and target.suggest_biaya != target.target_biaya :
                suggest_realiasi_notsame = True
            res[target.id] = suggest_realiasi_notsame
            # print "target_jumlah_kuantitas_output_notsame : ",target_jumlah_kuantitas_output_notsame 
        return res 
    
    _columns = {
        'name': fields.char('Nama Kegiatan', size=500, readonly=True,),
        'code'     : fields.char('Kode Rekening', size=25,),
        'code_opd'     : fields.char('Kode BALAI',size=2,readonly=True,
                            states={'draft': [('readonly', False)],'new': [('readonly', False)]},),
        'code_program'     : fields.char('Kode Program',size=3,readonly=True,
                            states={'draft': [('readonly', False)],'new': [('readonly', False)]},),
        'code_kegiatan'     : fields.char('Kode Kegiatan',size=2,readonly=True,
                            states={'draft': [('readonly', False)],'new': [('readonly', False)]},),
        'active': fields.boolean('Active', help="If the active field is set to False, it will allow you to hide the project without removing it."),
        'project_id': fields.many2one('project.project', 'Target', ondelete='set null', select=True, track_visibility='onchange', change_default=True),
        'target_type_id': fields.selection([
                                                      ('dpa_opd_biro', 'Kegiatan APBD - DPA'),       
                                                      ('dipa_apbn', 'Kegiatan APBN - DIPA'),                                                  
                                                      ('sotk', 'Kegiatan Tupoksi (Non DPA)'),
                                                      ('lain_lain', 'Kegiatan Target Pendapatan Daerah'),  
                                                      ], 'Jenis Kegiatan', required=True
                                                     ),
        'target_category_id': fields.selection([     ('program', 'Program'), 
                                                      ('kegiatan', 'Kegiatan'), 
                                                      ],
                                                      'Kategori',readonly=True,states={'draft': [('readonly', False)],'new': [('readonly', False)],'correction': [('readonly', False)]},
                                                     ),
        'target_period_month'     : fields.selection([('01', 'Januari'), ('02', 'Februari'),
                                                      ('03', 'Maret'), ('04', 'April'),
                                                      ('05', 'Mei'), ('06', 'Juni'),
                                                      ('07', 'Juli'), ('08', 'Agustus'),
                                                      ('09', 'September'), ('10', 'Oktober'),
                                                      ('11', 'November'), ('12', 'Desember')], 'Periode Bulan'
                                                     ),
        'state': fields.selection([('draft', 'Draft'), ('realisasi', 'Realisasi'),
                                        ('propose', 'Atasan'), ('rejected_manager', 'Pengajuan Ditolak Atasan'),
                                        ('appeal', 'Banding'), ('evaluated', 'Verifikatur'), ('rejected_bkd', 'Pengajuan Ditolak Verifikatur'),
                                        ('propose_to_close','Pengajuan Closing Target'),('closed','Closed'),
                                        ('done', 'Selesai'), 
                                        ('cancelled', 'Cancel')], 'Status Pekerjaan'),
        'target_period_year'     : fields.char('Periode Tahun', size=4, required=True),
        
        'user_id': fields.many2one('res.users', 'Pegawai Yang Dinilai',),
        'user_id_atasan': fields.many2one('res.users', 'Pejabat Penilai',),
        'user_id_banding': fields.many2one('res.users', 'Atasan Pejabat Penilai',),
        'user_id_bkd': fields.many2one('res.users', 'Pejabat Pengevaluasi (BKD)',),
        'notes'     : fields.text('Catatan', states={'draft': [('required', False)]}),
        'notes_atasan'     : fields.text('Catatan Atasan', ),
        'notes_atasan_banding'     : fields.text('Catatan Atasan Banding', ),
        'notes_bkd'     : fields.text('Catatan Petugas Verifikasi', ),
        
        'is_suggest': fields.boolean('Tambahkan Koreski Penilaian'),
        'is_appeal': fields.boolean('Tambahkan Koreski Banding'),
        'is_control': fields.boolean('Verifikasi Penilaian'),
        
        
        'target_jumlah_kuantitas_output'     : fields.integer('Kuantitas Output', required=True, readonly=True, states={'draft': [('readonly', False)]}),
        'target_satuan_kuantitas_output'     : fields.many2one('satuan.hitung', 'Jenis Kuantitas Output', readonly=True, states={'draft': [('readonly', False)]}),
        'target_angka_kredit'     : fields.float('Angka Kredit', digits_compute=dp.get_precision('angka_kredit') ), 
        'target_kualitas'     : fields.float('Kualitas', required=True, readonly=True, states={'draft': [('readonly', False)]},  digits_compute=dp.get_precision('no_digit'),), 
        'target_waktu'     : fields.integer('Waktu', required=True, readonly=True, states={'draft': [('readonly', False)]},  digits_compute=dp.get_precision('no_digit'),),
        'target_satuan_waktu'     : fields.selection([('hari', 'Hari')], 'Satuan Waktu', select=1, readonly=True, states={'draft': [('readonly', False)]}),
        'target_biaya'     : fields.float('Biaya', readonly=True, states={'draft': [('readonly', False)]}),
        
        'realisasi_jumlah_kuantitas_output'     : fields.integer('Kuantitas Output'),
        'realisasi_satuan_kuantitas_output'     : fields.many2one('satuan.hitung', 'Jenis Kuantitas Output'),
        'realisasi_angka_kredit'     : fields.float('Angka Kredit',digits_compute=dp.get_precision('angka_kredit') ), # 
        'realisasi_kualitas'     : fields.float('Kualitas', digits_compute=dp.get_precision('no_digit')), # 
        'realisasi_waktu'     : fields.integer('Waktu', digits_compute=dp.get_precision('no_digit') ),
        'realisasi_satuan_waktu'     : fields.selection([('hari', 'Hari')], 'Satuan Waktu', select=1),
        'realisasi_biaya'     : fields.float('Biaya'),
        
        'suggest_jumlah_kuantitas_output'     : fields.integer('Kuantitas Output', readonly=False),
        'suggest_satuan_kuantitas_output'     : fields.many2one('satuan.hitung', 'Jenis Kuantitas Output', readonly=False),
        'suggest_angka_kredit'     : fields.float('Angka Kredit', readonly=False, digits_compute=dp.get_precision('angka_kredit')), 
        'suggest_kualitas'     : fields.float('Kualitas', readonly=False, digits_compute=dp.get_precision('no_digit')), #
        'suggest_waktu'     : fields.integer('Waktu', readonly=False, digits_compute=dp.get_precision('no_digit')),
        'suggest_satuan_waktu'     : fields.selection([('hari', 'Hari')], 'Satuan Waktu', select=1, readonly=False),
        'suggest_biaya'     : fields.float('Biaya', readonly=False),
        
        'appeal_jumlah_kuantitas_output'     : fields.integer('Kuantitas Output'),
        'appeal_satuan_kuantitas_output'     : fields.many2one('satuan.hitung', 'Jenis Kuantitas Output'),
        'appeal_angka_kredit'     : fields.float('Angka Kredit', digits_compute=dp.get_precision('angka_kredit')), #
        'appeal_kualitas'     : fields.float('Kualitas', digits_compute=dp.get_precision('no_digit')), #
        'appeal_waktu'     : fields.integer('Waktu',digits_compute=dp.get_precision('no_digit')),
        'appeal_satuan_waktu'     : fields.selection([('hari', 'Hari')], 'Satuan Waktu', select=1),
        'appeal_biaya'     : fields.float('Biaya'),
        
        
        
        'control_count': fields.integer('Jumlah Pengajuan Verifikasi', readonly=True),
        'nilai_akhir': fields.float('Nilai', readonly=True),
        'nilai_sementara': fields.float('Nilai Sementara', readonly=True),
        'jumlah_perhitungan': fields.float('Jumlah Perhitungan', readonly=True),
        'indeks_nilai': fields.char('Indeks', size=20, readonly=True),
        
        'project_state': fields.related('project_id', 'state', type='char', string='Target Status', store=False),
        'company_id': fields.many2one('res.company', 'BALAI'),
        'currency_id': fields.many2one('res.currency', 'Currency',invisible="True"),
        'suggest_realiasi_notsame' :fields.function(_get_status_suggest_realiasi_notsame, method=True, string='Status Suggest Target Output', type='boolean', readonly=True, store=False),
        'employee_id': fields.related('user_id', 'partner_id',   type="many2one", relation='res.partner', string='Employee', store=True),
        'employee_job_type': fields.selection([('struktural', 'Jabatan Struktural'), ('jft', 'Jabatan Fungsional Tertentu'), ('jfu', 'Jabatan Fungsional Umum')], 'Tipe Jabatan'),
        'is_kepala_opd': fields.related('employee_id', 'is_kepala_opd', type='boolean', string='Tugas Untuk Kepala BALAI', store=True),
        'use_target_for_calculation': fields.boolean('Perhitungan Menggunakan Target',help="Pengakuan perhitungan berdasar target, Bukan Realisasi"),
        
        'target_realiasi_notsame': fields.boolean('Realisasi Tidak Sama Target',help="Realisasi Tidak Sama Target"),
        'biaya_verifikasi_notsame': fields.boolean('Status Verifikasi Biaya',help="Realisasi Tidak Sama Target"),
    }
    _defaults = {
        'user_id': lambda self, cr, uid, ctx: uid,
        'state':'draft',
        'name':'',
        'target_period_year':lambda *args: time.strftime('%Y'),
        'use_target_for_calculation':False,
        
    }
    _order = "target_period_year,target_period_month"
    
    #VALIDATOR
    def get_auth_id(self, cr, uid, ids, type, context=None):
        if not isinstance(ids, list): ids = [ids]
        for task in self.browse(cr, uid, ids, context=context):
            
            if type == 'user_id' :
                if task.user_id :
                    if task.user_id.id != uid :
                        raise osv.except_osv(_('Invalid Action!'),
                                             _('Anda Tidak Memiliki Priviledge Untuk Proses Ini.'))
            elif type == 'user_id_atasan' :
                if task.user_id_atasan :
                    if task.user_id_atasan.id != uid :
                        raise osv.except_osv(_('Invalid Action!'),
                                             _('Anda Tidak Memiliki Priviledge Untuk Proses Ini.'))
            elif type == 'user_id_banding' :
                if task.user_id_banding :
                    if task.user_id_banding.id != uid :
                        raise osv.except_osv(_('Invalid Action!'),
                                             _('Anda Tidak Memiliki Priviledge Untuk Proses Ini.'))
            elif type == 'user_id_bkd' :
                if task.user_id_bkd :
                    if task.user_id_bkd.id != uid :
                        raise osv.except_osv(_('Invalid Action!'),
                                             _('Anda Tidak Memiliki Priviledge Untuk Proses Ini.'))
                
            else : 
               return False;
            
        return True;
    def is_user(self,cr,uid,ids,context=None):
        task_id = len(ids) and ids[0] or False
        ret_val = False
        if not task_id: return False
        return self.get_auth_id(cr, uid, [task_id],'user_id', context=context) 
    def is_manager(self,cr,uid,ids,context=None):
        task_id = len(ids) and ids[0] or False
        ret_val = False
        if not task_id: return False
        return self.get_auth_id(cr, uid, [task_id],'user_id_atasan', context=context) 
    def is_appeal_manager(self,cr,uid,ids,context=None):
        task_id = len(ids) and ids[0] or False
        ret_val = False
        if not task_id: return False
        return self.get_auth_id(cr, uid, [task_id],'user_id_banding', context=context) 
    def is_verificator(self,cr,uid,ids,context=None):
        task_id = len(ids) and ids[0] or False
        ret_val = False
        if not task_id: return False
        if uid == SUPERUSER_ID : return True;
        return self.get_auth_id(cr, uid, [task_id],'user_id_bkd', context=context)
    def is_user_or_is_verificator(self,cr,uid,ids,context=None):
        task_id = len(ids) and ids[0] or False
        if not task_id: return False
        accepted = self.get_auth_id(cr, uid, [task_id],'user_id', context=context) or self.get_auth_id(cr, uid, [task_id],'user_id_bkd', context=context)
        return accepted
    
    def validate_ajukan_atasan_state(self,cr,uid,ids,context=None):
         for task_obj in self.browse(cr, uid, ids, context=context):
                 if not task_obj.project_id:
                     raise osv.except_osv(_('Invalid Action, Proses Tidak Dapat Dilanjutkan'),
                                        _('Karena Kegiatan Tidak Memiliki Target Tahunan.'))
                 if task_obj.project_id:
                     if task_obj.project_id.state=='confirm':
                         return True;
                     else :
                         raise osv.except_osv(_('Invalid Action, Proses Tidak Dapat Dilanjutkan'),
                                        _('Proses Tidak Dapat Dilanjutkan Karena Target Belum Di Terima.'))
         return True;
    def validate_target_state(self,cr,uid,ids,context=None):
         for task_obj in self.browse(cr, uid, ids, context=context):
                 if task_obj.project_id:
                     if task_obj.project_id.state=='confirm':
                         return True;
                     else :
                         raise osv.except_osv(_('Invalid Action, Proses Tidak Dapat Dilanjutkan'),
                                        _('Proses Tidak Dapat Dilanjutkan Karena Target Belum Di Terima.'))
         return True;
    # WORKFLOW    
    def set_draft(self, cr, uid, ids, context=None):
        return self.write(cr, uid, ids, {'state':'draft'}, context=context) 
    def set_realisasi(self, cr, uid, ids, context=None):
        return self.write(cr, uid, ids, {'state':'realisasi'}, context=context)
    def set_cancelled(self, cr, uid, ids, context=None):
        return self.write(cr, uid, ids, {'state':'cancelled','active':False}, context=context)
    def set_closed(self, cr, uid, ids, context=None):
        return self.write(cr, uid, ids, {'state':'closed','active':False}, context=context)
    def set_propose(self, cr, uid, ids, context=None):
        return self.write(cr, uid, ids, {'state':'propose'}, context=context)
    def set_appeal(self, cr, uid, ids, context=None):
        return self.write(cr, uid, ids, {'state':'appeal'}, context=context)
    def set_evaluated(self, cr, uid, ids, context=None):
        for task_id in ids :
            self.write(cr, uid, task_id, {'state':'evaluated',
                                         'target_realiasi_notsame':self.is_status_target_realiasi_notsame(cr,uid,task_id,context),
                                         'biaya_verifikasi_notsame':self.is_status_biaya_verifikasi_notsame(cr,uid,task_id,context), 
                                         },
                          context=context)
        return True
    def set_done(self, cr, uid, ids, context=None):
        return self.write(cr, uid, ids, {'state':'done'}, context=context)
    def set_rejected_bkd(self, cr, uid, ids, context=None):
        return self.write(cr, uid, ids, {'state':'rejected_bkd'}, context=context)
    def action_realisasi(self, cr, uid, ids, context=None):
        """ Khusus untuk kegiatan perilaku dan  tambahan & kreatifitas.
            Kondisi pegawai siap mengisikan realisasi
        """
        if self.is_user(cr, uid, ids, context) :
            if self.validate_target_state(cr, uid, ids, context) :
                    self.fill_target_perilaku(cr, uid, ids, context);
                    self.fill_lookup_perilaku_komitmen(cr, uid, ids, context);
                    return self.set_realisasi(cr, uid, ids, context)
                    #self.do_target_done_notification(cr, uid, [task_id], context=context)
        
    def action_propose(self, cr, uid, ids, context=None):
        """ Selesai mengerjakan input realisasi, 
        """
        if self.is_user(cr, uid, ids, context) :
            if self.validate_ajukan_atasan_state(cr, uid, ids, context) :
                self.fill_target_automatically_with_task(cr, uid, ids, context);
                return self.set_propose(cr, uid, ids, context=context)
    def action_propose_rejected_popup(self, cr, uid, ids, context=None):
        
        if not ids: return []
        dummy, view_id = self.pool.get('ir.model.data').get_object_reference(cr, uid, 'df_project', 'action_propose_rejected_popup_form_view')
        

        if self.is_manager(cr, uid, ids, context):
            task_obj = self.browse(cr, uid, ids[0], context=context)
            return {
                'name':_("Pengajuan Di Tolak"),
                'view_mode': 'form',
                'view_id': view_id,
                'view_type': 'form',
                'res_model': 'project.skp.propose.rejected',
                'type': 'ir.actions.act_window',
                'nodestroy': True,
                'target': 'new',
                'domain': '[]',
                'context': {
                    'default_jumlah_kuantitas_output': task_obj.realisasi_jumlah_kuantitas_output,
                    'default_satuan_kuantitas_output':task_obj.realisasi_satuan_kuantitas_output.id,
                    'default_kualitas':task_obj.realisasi_kualitas ,
                    'default_waktu': task_obj.realisasi_waktu,
                    'default_satuan_waktu': task_obj.realisasi_satuan_waktu,
                    'default_biaya': task_obj.realisasi_biaya,
                    'default_angka_kredit': task_obj.realisasi_angka_kredit,
                    'default_is_suggest': True,
                    'default_task_id':task_obj.id
    
                }
            }
        return False
    
    def action_appeal(self, cr, uid, ids, context=None):
        """ Penolakan di Banding ke Atasan 
        """
        if self.is_user(cr, uid, ids, context) :
            self.fill_task_appeal_automatically_with_suggest(cr, uid, ids, context);
            return self.set_appeal(cr, uid, ids, context=context)
    def action_dont_appeal(self, cr, uid, ids, context=None):
        """ Penolakan Diterima, Langsung ajukan Verifikasi,
        """
        if self.is_user(cr, uid, ids, context) :
            self.fill_task_automatically_with_suggest(cr, uid, ids, context);
            return self.set_evaluated(cr, uid, ids, context=context)
    def action_appeal_approve(self, cr, uid, ids, context=None):
        """ Banding DIterima, Langsung ajukan Verifikasi,
        """
        if self.is_appeal_manager(cr, uid, ids, context) : 
            return self.set_evaluated(cr, uid, ids, context=context)

    def action_appeal_reject_popup(self, cr, uid, ids, context=None):
        """ Banding Ditolak
        """
        if not ids: return []
        dummy, view_id = self.pool.get('ir.model.data').get_object_reference(cr, uid, 'df_project', 'action_appeal_rejected_popup_form_view')
        

        if self.is_appeal_manager(cr, uid, ids, context):
            task_obj = self.browse(cr, uid, ids[0], context=context)
            return {
                'name':_("Pengajuan Banding Di Tolak"),
                'view_mode': 'form',
                'view_id': view_id,
                'view_type': 'form',
                'res_model': 'project.skp.appeal.rejected',
                'type': 'ir.actions.act_window',
                'nodestroy': True,
                'target': 'new',
                'domain': '[]',
                'context': {
                    'default_jumlah_kuantitas_output': task_obj.suggest_jumlah_kuantitas_output,
                    'default_satuan_kuantitas_output':task_obj.suggest_satuan_kuantitas_output.id,
                    'default_kualitas':task_obj.suggest_kualitas ,
                    'default_waktu': task_obj.suggest_waktu,
                    'default_satuan_waktu': task_obj.suggest_satuan_waktu,
                    'default_biaya': task_obj.suggest_biaya,
                    'default_angka_kredit': task_obj.suggest_angka_kredit,
                    'default_is_appeal': True,
                    'default_task_id':task_obj.id
    
                }
            }
        return False
    def action_evaluated(self, cr, uid, ids, context=None):
        """ Ajukan Verifikasi
        """
        if self.is_manager(cr, uid, ids, context) :
            return self.set_evaluated(cr, uid, ids, context=context)
    def action_done(self, cr, uid, ids, context=None):
        """ Selesai Perhitungan
        """
        for task_id in ids : 
            if self.is_verificator(cr, uid, [task_id], context) :
                self.do_task_poin_calculation(cr, uid, [task_id], context=context)
                self.set_done(cr, uid,  [task_id], context=context)
        return True
    def action_work_rejected(self, cr, uid, ids, context=None):
        """ Verifikasi Ditolak
        """
        
        if not ids: return []
        
        if self.is_verificator(cr, uid, ids, context) :
            dummy, view_id = self.pool.get('ir.model.data').get_object_reference(cr, uid, 'df_project', 'action_verificate_rejected_popup_form_view')
            task_obj = self.browse(cr, uid, ids[0], context=context)
            if task_obj.control_count >= 2 :
                raise osv.except_osv(_('Invalid Action, Limit Action'),
                                         _('Hasil Verifikasi Tidak Dapat Ditolak, Karena Sudah Dilakukan Pengajuan Dan Verifikasi Sebanyak 2 Kali.'))
            return {
                'name':_("Verifikasi Ditolak"),
                'view_mode': 'form',
                'view_id': view_id,
                'view_type': 'form',
                'res_model': 'project.skp.verificate.rejected',
                'type': 'ir.actions.act_window',
                'nodestroy': True,
                'target': 'new',
                'domain': '[]',
                'context': {
                    'default_control_count':task_obj.control_count + 1,
                    'default_is_control': True,
                    'default_task_id':task_obj.id
    
                }
            }
        return False
    def action_done_use_target(self, cr, uid, ids, context=None):
        """ Selesai Perhitungan
        """
        use_target = {'use_target_for_calculation': True,}
        for task_id in ids : 
            if self.is_verificator(cr, uid, [task_id], context) :
                self.write(cr, uid, [task_id], use_target, context=context)
                self.do_task_poin_calculation(cr, uid, [task_id], context=context)
                self.set_done(cr, uid, [task_id], context=context)
        return True
    def do_recalculate_poin(self, cr, uid, ids, context=None):
         if self.is_verificator(cr, uid, ids, context) :
            
            update_poin = {             'nilai_akhir': 0,
                                        'indeks_nilai': False,
                                        'jumlah_perhitungan':0,
                                        'use_target_for_calculation': False,
                                         }
            self.write(cr, uid, ids, update_poin)
            return self.set_evaluated(cr, uid, ids, context=context)
                 
         return True;
    def action_verificate_revision(self, cr, uid, ids, context=None):
        """ Selesai Perhitungan
        """
        if self.is_manager(cr, uid, ids, context) :
            self.fill_task_automatically_with_suggest(cr, uid, ids, context);
            return self.set_evaluated(cr, uid, ids, context=context)
    #Calculation
    def do_task_poin_calculation(self, cr, uid, ids, context=None):
        """ BKD->Done (Keuangan) """
        for task_id in ids : 
            update_poin = self.prepare_task_poin_calculation(cr, uid, [task_id], context=context)
            if update_poin:
                self.write(cr, uid, [task_id], update_poin,context)
        return True
    def do_task_poin_calculation_temporary(self, cr, uid, ids, context=None):
        for task_id in ids :
            update_poin = self.prepare_task_poin_calculation(cr, uid, [task_id], context=context)
            if update_poin:
                nilai_sementara = update_poin.get('nilai_akhir',0)
                self.write(cr, uid, [task_id], {
                                    'nilai_sementara': nilai_sementara}
                           ,context)
        return True
    
    def prepare_task_poin_calculation(self, cr, uid, ids, context=None):
        """ BKD->Done (Keuangan) """
        #print "Poin Calculation"
        target_pool = self.pool.get('project.project')
        lookup_nilai_pool = self.pool.get('acuan.penilaian')
        if not isinstance(ids, list): ids = [ids]
        for task_obj in self.browse(cr, uid, ids, context=context):
            vals = {}
            
            # close task...
            nilai_akhir = 0
            jumlah_perhitungan = 0
            indeks_nilai = 'a'
            try :
                pengali_waktu_ids = self.pool.get('config.skp').search(cr, uid, [('code', '=', 'x_waktu'), ('active', '=', True), ('type', '=', 'nilai')], context=None)
                pengali_waktu = self.pool.get('config.skp').browse(cr, uid, pengali_waktu_ids)[0].config_value_float
                pengali_biaya_ids = self.pool.get('config.skp').search(cr, uid, [('code', '=', 'x_biaya'), ('active', '=', True), ('type', '=', 'nilai')], context=None)
                pengali_biaya = self.pool.get('config.skp').browse(cr, uid, pengali_biaya_ids)[0].config_value_float
                efektifitas_biaya_ids = self.pool.get('config.skp').search(cr, uid, [('code', '=', 'x_std_b'), ('active', '=', True), ('type', '=', 'nilai')], context=None)
                efektifitas_biaya = self.pool.get('config.skp').browse(cr, uid, efektifitas_biaya_ids)[0].config_value_int or 0
                efektifitas_waktu_ids = self.pool.get('config.skp').search(cr, uid, [('code', '=', 'x_std_w'), ('active', '=', True), ('type', '=', 'nilai')], context=None)
                efektifitas_waktu = self.pool.get('config.skp').browse(cr, uid, efektifitas_waktu_ids)[0].config_value_int or 0
            except :
                raise osv.except_osv(_('Invalid Action, Data Konfigurasi Belum Diisi'),
                                _('Proses Tidak Dapat Dilanjutkan Karena User Konfigurasi Penilaian belum dibuat, Silahkan Hubungi admin'))
            
            employee = task_obj.user_id and task_obj.user_id.partner_id
            if not employee :
                 raise osv.except_osv(_('Invalid Action, Data Pegawai Tidak Lengkap'),
                                _('Proses Tidak Dapat Dilanjutkan Karena User Login Belum Dikaitkan Dengan Data Pegawai.'))
            else :
                job_type = employee.job_type
                if not job_type :
                     raise osv.except_osv(_('Invalid Action, Data Pegawai Tidak Lengkap'),
                                _('Jenis Jabatan Pegawai Belum Diisi, Harap Dilengkapi Terlebih Dahulu Di Data Pegawai, Atau Data Jabatan.'))
            if task_obj:
                if task_obj.project_id :
                    #validator Project
                    if not task_obj.project_id:
                        raise osv.except_osv(_('Invalid Action, Realisasi SKP Tidak Bisa Dinilai'),
                                    _('Realisasi SKP Tidak Bisa Dinilai, Karena Kegiatan Ini Tidak Memiliki Target Tahunan'))
                    else :
                        if task_obj.project_state not in  ('confirm','closed'):
                            raise osv.except_osv(_('Invalid Action, Realisasi SKP Tidak Bisa Dinilai'),
                                        _('Realisasi SKP Tidak Bisa Dinilai, Karena Target Tahunan Belum Diterima'))
                    
                    # (target/realisasi)*100
                    # =((1.76*I16-O16)/I16)*100
                    # =(((2.24*J16-P16)/J16)*100)
                    pembagi = 0
                    a = b = c = d = e = 0
                    if job_type in ('struktural', 'jft', 'jfu') :
                        #kuantitas
                        if task_obj.target_jumlah_kuantitas_output != 0:
                            x = float (self.get_value_realisasi_or_target(task_obj.target_jumlah_kuantitas_output,
                                                                          task_obj.realisasi_jumlah_kuantitas_output,
                                                                          task_obj.use_target_for_calculation)
                                       )
                            y = float (task_obj.target_jumlah_kuantitas_output)
                            a = (x / y) * 100
                            if a > 100 :
                                a=100
                            pembagi += 1
                        elif task_obj.target_jumlah_kuantitas_output == 0 and task_obj.realisasi_jumlah_kuantitas_output > 0:
                            a=100
                            pembagi += 1
                        elif task_obj.target_jumlah_kuantitas_output == 0 and task_obj.realisasi_jumlah_kuantitas_output == 0:
                            a=0
                            pembagi += 1
                        #kualitas
                        if task_obj.target_kualitas != 0:
                            b = (float(self.get_value_realisasi_or_target(task_obj.target_kualitas,
                                                                          task_obj.realisasi_kualitas,
                                                                          task_obj.use_target_for_calculation) 
                                       / task_obj.target_kualitas)) * 100
                            if b > 100 :
                                b=100
                            pembagi += 1
                        elif task_obj.target_kualitas == 0 and task_obj.realisasi_kualitas > 0:
                            b=100
                            pembagi += 1
                        elif task_obj.target_kualitas == 0 and task_obj.realisasi_kualitas == 0:
                            b=0
                            pembagi += 1
                        #waktu    
                        if task_obj.target_waktu != 0:
                            c = float (0)
                            x = float (self.get_value_realisasi_or_target(task_obj.target_waktu,
                                                                          task_obj.realisasi_waktu,
                                                                          task_obj.use_target_for_calculation))
                            y = float (task_obj.target_waktu)
                            if efektifitas_waktu == 1:
                                x_efektifitas = 100 - ((x / y) * 100)
                               
                                if x_efektifitas <= 24.0 : #x_efektifitas != 0.0 and 
                                    c = ((pengali_waktu * y - x) / y) * 100
                                if  x_efektifitas > 24.0 :
                                    c = ((pengali_waktu * y - x) / y) * 100
                                    c = 76 - (c - 100)
                            else :
                                c= (x / y) * 100
                            if c > 100 :
                                c=100
                            pembagi += 1
                        elif task_obj.target_waktu == 0 and task_obj.realisasi_waktu > 0: 
                            c=100
                            pembagi += 1
                        elif task_obj.target_waktu == 0 and task_obj.realisasi_waktu == 0: 
                            c=0
                            pembagi += 1
                            
                    if job_type in ('struktural') :
                        if task_obj.target_biaya != 0:
                            d =float(0)
                            x = float (self.get_value_realisasi_or_target(task_obj.target_biaya,
                                                                          task_obj.realisasi_biaya,
                                                                          task_obj.use_target_for_calculation))
                            y = float (task_obj.target_biaya)
                            if efektifitas_biaya == 1:
                                x_efektifitas = 100 - ((x / y) * 100)
                                
                                if  x_efektifitas <= 24.0 : #x_efektifitas != 0.0 and
                                    d = ((pengali_biaya * y - x) / y) * 100
                                if  x_efektifitas > 24.0 : 
                                    d = ((pengali_biaya * y - x) / y) * 100
                                    d = 76 - (d - 100)
                            else :
                                d= (x / y) * 100
                            if d > 100 :
                               d=100
                            pembagi += 1
                        elif task_obj.target_biaya == 0 and task_obj.realisasi_biaya > 0:  
                            d=100
                            pembagi += 1
                            
                    if job_type in ('jft') :
                        if task_obj.target_angka_kredit != 0:
                            x = float (self.get_value_realisasi_or_target(task_obj.target_angka_kredit,
                                                                          task_obj.realisasi_angka_kredit,
                                                                          task_obj.use_target_for_calculation))
                            y = float (task_obj.target_angka_kredit)
                            e = (x / y) * 100
                            if e > 100 :
                                e=100
                            pembagi += 1
                        elif task_obj.target_angka_kredit == 0  and task_obj.realisasi_angka_kredit > 0 :
                             e=100
                             pembagi += 1
                            
                    
                    #print " Calc : ", a, " ,", b, " ,", c, " ,", d, " ,", e , " = ", (a + b + c + d + e)," | pembagi : ",pembagi
                    if pembagi > 0 :
                        jumlah_perhitungan = (a + b + c + d +e)
                        nilai_akhir = (jumlah_perhitungan / pembagi)
                        if nilai_akhir>100:
                            nilai_akhir=100;
                            
                 # end if          
            update_poin = {
                                'nilai_akhir': nilai_akhir,
                                'indeks_nilai': self.get_value_poin(cr, uid, ids, nilai_akhir),
                                'jumlah_perhitungan':jumlah_perhitungan,
                                 }
            return update_poin;
        return False
    def get_value_realisasi_or_target(self, target,realisasi, use_target_for_calculation):
        if use_target_for_calculation : 
           return target
        return realisasi
    def get_value_poin(self, cr, uid, ids, nilai_akhir, context=None):
            # Tingkat kepuasan sebanyak 91 s.d 100 persen, maka nilai perilaku orientasi pelayanan Sangat Baik, dengan nilai antara 91 s.d 100 poin sesuai prosentase kepuasan    
            # Tingkat kepuasan sebanyak 76 s.d 90 persen, maka nilai perilaku orientasi pelayanan Baik, 
            # Tingkat kepuasan sebanyak 61 s.d 75 persen, maka nilai perilaku orientasi pelayanan Cukup, dengan nilai antara 61 s.d 75 poin sesuai prosentase kepuasan       
            # Tingkat kepuasan sebanyak 51 s.d 60 persen, maka nilai perilaku orientasi pelayanan Kurang, dengan nilai antara 51 s.d 60 poin sesuai prosentase kepuasan            
            # Tingkat kepuasan sebanyak kurang dari 50 persen, maka nilai perilaku orientasi pelayanan Buruk, dengan nilai antara 0 s.d 50 poin sesuai prosentase kepuasan
            value = '-'
            if nilai_akhir >= 91.0 and nilai_akhir <= 100.0 :
                value = 'Sangat Baik'; 
            if nilai_akhir >= 76.0 and nilai_akhir <= 90.0 :
                value = 'Baik'; 
            if nilai_akhir >= 61.0 and nilai_akhir <= 75.0 :
                value = 'Cukup'; 
            if nilai_akhir >= 51.0 and nilai_akhir <= 60.0 :
                value = 'Kurang'; 
            if nilai_akhir <= 50.0 :
                value = 'Buruk'; 
            return value;
    #Automation
    def fill_skp_task_automatically_with_target(self, cr, uid, ids, context=None):
        """ Jika selesai mebuat target. Maka secara default realisasi akan otomatis terisi dengan nilai target tersebut"""
        if not isinstance(ids, list): ids = [ids]
        for task_obj in self.browse(cr, uid, ids, context=context):
                vals = {}
                vals.update({
                                    #'realisasi_jumlah_kuantitas_output'     : task_obj.target_jumlah_kuantitas_output,
                                    'realisasi_satuan_kuantitas_output'     : task_obj.target_satuan_kuantitas_output.id or None,
                                    #'realisasi_angka_kredit'     : task_obj.target_angka_kredit,
                                    #'realisasi_kualitas'     : task_obj.target_kualitas,
                                    #'realisasi_waktu'     : task_obj.target_waktu,
                                    'realisasi_satuan_waktu'     : 'hari',
                                    #'realisasi_biaya'     : task_obj.target_biaya,
                                })
                # end if               
                self.write(cr, uid, [task_obj.id], vals, context)
        # end for
        return True  
    def fill_target_automatically_with_task(self, cr, uid, ids, context=None):
        """ Default Target di isi task"""
        if not isinstance(ids, list): ids = [ids]
        for task_obj in self.browse(cr, uid, ids, context=context):
                vals = {}
            
                vals.update({
                                    'suggest_jumlah_kuantitas_output'     : task_obj.realisasi_jumlah_kuantitas_output,
                                    'suggest_satuan_kuantitas_output'     : task_obj.realisasi_satuan_kuantitas_output.id or None,
                                    'suggest_angka_kredit'     : task_obj.realisasi_angka_kredit,
                                    'suggest_kualitas'     : task_obj.realisasi_kualitas,
                                    'suggest_waktu'     : task_obj.realisasi_waktu,
                                    'suggest_satuan_waktu'     : task_obj.realisasi_satuan_waktu,
                                    'suggest_biaya'     : task_obj.realisasi_biaya,
                                })
                # end if               
                self.write(cr, uid, [task_obj.id], vals, context)
        # end for
        return True
    def fill_task_appeal_automatically_with_suggest(self, cr, uid, ids, context=None):
        """ Jika selesai mebuat target. Maka secara default realisasi akan otomatis terisi dengan nilai target tersebit"""
        if not isinstance(ids, list): ids = [ids]
        for task_obj in self.browse(cr, uid, ids, context=context):
            vals = {}
            
            if task_obj.is_suggest:
                vals.update({
                                    'appeal_jumlah_kuantitas_output'     : task_obj.suggest_jumlah_kuantitas_output,
                                    'appeal_satuan_kuantitas_output'     : task_obj.suggest_satuan_kuantitas_output.id or None,
                                    'appeal_angka_kredit'     : task_obj.suggest_angka_kredit,
                                    'appeal_kualitas'     : task_obj.suggest_kualitas,
                                    'appeal_waktu'     : task_obj.suggest_waktu,
                                    'appeal_satuan_waktu'     : task_obj.suggest_satuan_waktu,
                                    'appeal_biaya'     : task_obj.suggest_biaya,
                                })
                # end if               
                self.write(cr, uid, [task_obj.id], vals, context)
        # end for
        return True
    def fill_task_automatically_with_suggest(self, cr, uid, ids, context=None):
        """ Jika selesai mebuat target. Maka secara default realisasi akan otomatis terisi dengan nilai target tersebit"""
        if not isinstance(ids, list): ids = [ids]
        for task_obj in self.browse(cr, uid, ids, context=context):
            vals = {}
            if task_obj.is_suggest:
                vals.update({
                                    'realisasi_jumlah_kuantitas_output'     : task_obj.suggest_jumlah_kuantitas_output,
                                    'realisasi_satuan_kuantitas_output'     : task_obj.suggest_satuan_kuantitas_output.id or None,
                                    'realisasi_angka_kredit'                : task_obj.suggest_angka_kredit,
                                    'realisasi_kualitas'                    : task_obj.suggest_kualitas,
                                    'realisasi_waktu'                       : task_obj.suggest_waktu,
                                    'realisasi_satuan_waktu'                : task_obj.suggest_satuan_waktu,
                                    'realisasi_biaya'                       : task_obj.suggest_biaya,
                                })
                        
                # end if               
                self.write(cr, uid, [task_obj.id], vals, context)
        # end for
        return True
    def fill_task_automatically_with_appeal(self, cr, uid, ids, context=None):
        """ Jika selesai mebuat target. Maka secara default realisasi akan otomatis terisi dengan nilai target tersebit"""
        if not isinstance(ids, list): ids = [ids]
        for task_obj in self.browse(cr, uid, ids, context=context):
            vals = {}
            if task_obj.is_appeal:
                vals.update({
                                    'realisasi_jumlah_kuantitas_output'     : task_obj.appeal_jumlah_kuantitas_output,
                                    'realisasi_satuan_kuantitas_output'     : task_obj.appeal_satuan_kuantitas_output.id or None,
                                    'realisasi_angka_kredit'                : task_obj.appeal_angka_kredit,
                                    'realisasi_kualitas'                    : task_obj.appeal_kualitas,
                                    'realisasi_waktu'                       : task_obj.appeal_waktu,
                                    'realisasi_satuan_waktu'                : task_obj.appeal_satuan_waktu,
                                    'realisasi_biaya'                       : task_obj.appeal_biaya,
                                })
                              
                # end if               
                self.write(cr, uid, [task_obj.id], vals, context)
        # end for
        return True
    
    # Optimasi Fungsi kategori Verifikasi :
    def is_status_target_realiasi_notsame(self, cr, uid, task_id, context=None):
        
        if not task_id:
            return False
        target_realiasi_notsame=False
        task_obj  = self.browse(cr, uid, task_id, context=context)
        if task_obj  :
                 if task_obj.target_jumlah_kuantitas_output > task_obj.realisasi_jumlah_kuantitas_output :
                    target_realiasi_notsame = True
                 if task_obj.target_kualitas != 0 and task_obj.target_kualitas > task_obj.realisasi_kualitas :
                    target_realiasi_notsame = True
                 if task_obj.target_waktu > task_obj.realisasi_waktu :
                    target_realiasi_notsame = True
                 if task_obj.target_biaya > task_obj.realisasi_biaya and target_realiasi_notsame:
                    target_realiasi_notsame = True
        return target_realiasi_notsame 
    def is_status_biaya_verifikasi_notsame(self, cr, uid, task_id, context=None):
        if not task_id:
            return False
        task_obj  = self.browse(cr, uid, task_id, context=context)
        if task_obj  :
                if  (task_obj.target_jumlah_kuantitas_output <= task_obj.realisasi_jumlah_kuantitas_output and \
                        task_obj.target_kualitas <= task_obj.realisasi_kualitas and \
                        task_obj.target_waktu <= task_obj.realisasi_waktu) and \
                        task_obj.realisasi_biaya != task_obj.target_biaya :
                        return True
        return False 
    
project_skp()