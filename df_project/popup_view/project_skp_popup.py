from openerp.osv import fields, osv
from datetime import datetime,timedelta
import time
from mx import DateTime
import openerp.addons.decimal_precision as dp

# ====================== Popup Class Object ================================= #

class project_skp_propose_rejected(osv.Model):
    _name = 'project.skp.propose.rejected'
    _description = 'Realisasi SKP , Pengajuan Ditolak Atasan'
    _columns = {
        'is_suggest': fields.boolean('Tambahkan Koreski Penilaian'),
        'notes'      : fields.text('Catatan Koreksi',required=True),
        'jumlah_kuantitas_output'     : fields.integer('Kuantitas Output', required=True),
        'satuan_kuantitas_output'     : fields.many2one('satuan.hitung', 'Jenis Kuantitas Output', required=True),
        'angka_kredit'     : fields.float('Angka Kredit', readonly=False,digits_compute=dp.get_precision('angka_kredit')),
        'kualitas'     : fields.float('Kualitas', required=True,digits_compute=dp.get_precision('no_digit')),
        'waktu'     : fields.integer('Waktu', required=True,digits_compute=dp.get_precision('no_digit')),
        'satuan_waktu'     : fields.selection([('hari', 'Hari')], 'Satuan Waktu', select=1, required=True),
        'biaya'     : fields.float('Biaya', readonly=False),
        'task_id'     : fields.many2one('project.skp', 'Realisasi', readonly=True),
       
    }
    def action_propose_rejected(self, cr, uid, ids, context=None):
        """  Pengajuan Di tolak
        """
        vals = {}
        task_pool = self.pool.get('project.skp')
        task_obj = self.browse(cr, uid, ids[0], context=context)
        vals.update({  'suggest_jumlah_kuantitas_output'     : task_obj.jumlah_kuantitas_output,
                                    'suggest_satuan_kuantitas_output'     : task_obj.satuan_kuantitas_output.id or None,
                                    'suggest_angka_kredit'     : task_obj.angka_kredit,
                                    'suggest_kualitas'     : task_obj.kualitas,
                                    'suggest_waktu'     : task_obj.waktu,
                                    'suggest_satuan_waktu'     : task_obj.satuan_waktu,
                                    'suggest_biaya'     : task_obj.biaya,
                                    'is_suggest' : True,
                                    'notes_atasan' : task_obj.notes,
                                    'state':'rejected_manager',
                                     })
        task_pool.write(cr, uid, [task_obj.task_id.id], vals, context)
        
    
project_skp_propose_rejected()

class project_skp_appeal_rejected(osv.Model):
    _name = 'project.skp.appeal.rejected'
    _description = 'Realisasi SKP , Pengajuan Banding Ditolak'
    _columns = {
        'is_appeal': fields.boolean('Tambahkan Koreski Penilaian'),
        'notes'      : fields.text('Catatan Koreksi',required=True),
        'jumlah_kuantitas_output'     : fields.integer('Kuantitas Output', required=True),
        'satuan_kuantitas_output'     : fields.many2one('satuan.hitung', 'Jenis Kuantitas Output', required=True),
        'angka_kredit'     : fields.float('Angka Kredit', readonly=False,digits_compute=dp.get_precision('angka_kredit')),
        'kualitas'     : fields.float('Kualitas', required=True,digits_compute=dp.get_precision('no_digit')),
        'waktu'     : fields.integer('Waktu', required=True,digits_compute=dp.get_precision('no_digit')),
        'satuan_waktu'     : fields.selection([('hari', 'Hari')], 'Satuan Waktu', select=1, required=True),
        'biaya'     : fields.float('Biaya', readonly=False),
        'task_id'     : fields.many2one('project.skp', 'Realisasi', readonly=True),
       
    }
    def action_appeal_rejected(self, cr, uid, ids, context=None):
        """  Pengajuan Di tolak
        """
        vals = {}
        task_pool = self.pool.get('project.skp')
        task_obj = self.browse(cr, uid, ids[0], context=context)
        vals.update({  'appeal_jumlah_kuantitas_output'     : task_obj.jumlah_kuantitas_output,
                                    'appeal_satuan_kuantitas_output'     : task_obj.satuan_kuantitas_output.id or None,
                                    'appeal_angka_kredit'     : task_obj.angka_kredit,
                                    'appeal_kualitas'     : task_obj.kualitas,
                                    'appeal_waktu'     : task_obj.waktu,
                                    'appeal_satuan_waktu'     : task_obj.satuan_waktu,
                                    'appeal_biaya'     : task_obj.biaya,
                                    'is_appeal' : True,
                                    'notes_atasan_banding' : task_obj.notes,
                                     })
        
        task_pool.write(cr, uid, [task_obj.task_id.id], vals, context)
        task_pool.fill_task_automatically_with_appeal(cr, uid, [task_obj.task_id.id], context);
        task_pool.set_evaluated(cr, uid, [task_obj.task_id.id], context)
    
project_skp_appeal_rejected()


class project_skp_verificate_rejected(osv.Model):
    _name = 'project.skp.verificate.rejected'
    _description = 'Realisasi Bulanan , Verifikasi Ditolak'
    _columns = {
        'is_control': fields.boolean('Tambahkan Koreski Verifikasi'),
        'notes'      : fields.text('Kesimpulan Catatan Koreksi',required=True),
        'control_count': fields.integer('Jumlah Koreksi Verifikasi', readonly=True),
        'task_id'     : fields.many2one('project.skp', 'Realisasi', readonly=True),
       
    }
    def action_verificate_rejected(self, cr, uid, ids, context=None):
        """  Pengajuan Di tolak
        """
        vals = {}
        task_pool = self.pool.get('project.skp')
        task_obj = self.browse(cr, uid, ids[0], context=context)
        vals.update({               'control_count'     : task_obj.control_count,
                                    'is_control' : True,
                                    'is_suggest' : True,
                                    'notes_bkd' : task_obj.notes,
                                    'state':'rejected_bkd',
                                     })
        task_pool.write(cr, uid, [task_obj.task_id.id], vals, context)
        
    
project_skp_verificate_rejected()