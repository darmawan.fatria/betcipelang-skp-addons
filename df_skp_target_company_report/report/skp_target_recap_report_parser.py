import openerp.pooler
import time
from datetime import datetime
import openerp.tools
import logging
from openerp.report import report_sxw
from datetime import datetime
from openerp.addons.report_webkit import webkit_report
from openerp.tools.translate import _
from openerp.osv import osv
import math
import locale

class skp_target_recap_report_parser(report_sxw.rml_parse):

    def __init__(self, cr, uid, name, context=None):
        super(skp_target_recap_report_parser, self).__init__(cr, uid, name, context=context)

    def get_skp_target_recap_report_raw(self,filters,context=None):
        
        
        period_year=filters['form']['period_year']
        company_id=filters['form']['company_id']
        biro_id=filters['form']['biro_id']
        kepala_opd=filters['form']['is_kepala_opd']
        data_filter= [] 
        param_company_id = company_id[0] #because required
        company_id_state = False
        if company_id and company_id[0] == 1:
            company_id_state=True
        domain = period_year,period_year,period_year,period_year,period_year,company_id_state,param_company_id
         
        query = """
                   
        select p.name,p.nip,
                   c.name BALAI,
                   biro.name biro,
                   dept.name Unit_Kerja,
                   job.name Jabatan,
                   ats.nip nip_atasan,
                   ats.name nama_atasan,
                   sum(pro_draft.cnt) draft_new,
                   sum(pro_propose.cnt) propose,
                   sum(pro_bkd.cnt) bkd,
                   sum(pro_confirm.cnt) confirm,
                   sum(pro_all.cnt) all_target,
                   p.user_id
            from res_partner p
            left join 
            ( select user_id,target_period_year,count(id) cnt 
                  from project_project
                  where state in ('draft','new')
                  and target_period_year=%s
                  group by user_id,target_period_year
                )  pro_draft on     
                pro_draft.user_id = p.user_id
            left join 
            ( select user_id,target_period_year,count(id) cnt 
                  from project_project
                  where state = 'propose'
                  and target_period_year=%s
                  group by user_id,target_period_year
                )  pro_propose on     
                pro_propose.user_id = p.user_id
            left join 
            ( select user_id,target_period_year,count(id) cnt 
                  from project_project
                  where state = 'evaluated'
                  and target_period_year=%s
                  group by user_id,target_period_year
                )  pro_bkd on     
                pro_bkd.user_id = p.user_id
            left join 
            ( select user_id,target_period_year,count(id) cnt 
                  from project_project
                  where state = 'confirm'
                  and target_period_year=%s
                  group by user_id,target_period_year
                )  pro_confirm on     
                pro_confirm.user_id = p.user_id
            left join 
            ( select user_id,target_period_year,count(id) cnt 
                  from project_project
                  where state != 'cancelled'
                  and target_period_year=%s
                  group by user_id,target_period_year
                )  pro_all on     
                pro_all.user_id = p.user_id
            left join res_partner ats on ats.id = p.user_id_atasan
            left join res_company c on c.id = p.company_id
            left join partner_employee_biro biro on biro.id = p.biro_id
            left join partner_employee_department dept on dept.id = p.department_id
            left join partner_employee_job job on  job.id = p.job_id   
            where p.employee
             and   p.user_id notnull
             and    (%s OR p.company_id = %s)
            group by p.name,p.nip,
                   c.name,
                   biro.name,
                   dept.name,
                   job.name,
                   ats.nip,
                   ats.name,
                   p.user_id  
        """
        list_data = []
        self.cr.execute(query,domain)
        result = self.cr.fetchall()
        no_idx=1;
        for  employee_name,employee_nip,company_name,biro,unit_kerja,jabatan,nip_atasan,nama_atasan, \
        count_draft_new,count_propose,count_bkd,count_confirm,pro_all, \
        user_id in result:
            new_dict = {}
            new_dict['no_idx'] = no_idx
            new_dict['employee_name'] = employee_name
            new_dict['employee_nip'] = employee_nip
            new_dict['company_name'] = company_name
            new_dict['biro'] = biro
            new_dict['unit_kerja'] = unit_kerja
            new_dict['jabatan'] = jabatan
            new_dict['nip_atasan'] = nip_atasan
            new_dict['nama_atasan'] = nama_atasan
            new_dict['count_draft_new'] = count_draft_new
            new_dict['count_propose'] = count_propose
            new_dict['count_bkd'] = count_bkd
            new_dict['count_confirm'] = count_confirm
            new_dict['pro_all'] = pro_all
            new_dict['user_id'] = user_id
            
            list_data.append(new_dict)
            no_idx+=1
        return list_data
    def get_opd_name_filter(self,prefiks,filters):
        if not filters['form']['company_id'] : return prefiks;

        title_company_name = filters['form']['company_id'][1]
        return prefiks+ " " + title_company_name
    def get_title(self,prefiks,filters):
        if not filters['form']['company_id'] : return prefiks;

        title_company_name = filters['form']['company_id'][1]
        return prefiks+ " " + title_company_name
    def get_period(self,filters):
        
        year='';
        
        if filters['form']['period_year']:
            year = filters['form']['period_year']
        return year
        
   
    
