# -*- encoding: utf-8 -*-
##############################################################################
#
#    Darmawan Fatriananda
#    BKD Pemprov Jabar
#    Copyright (c) 2014 <http://www.asdarfat.wordpress.com.com>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see http://www.gnu.org/licenses/.
#
##############################################################################

from openerp.osv import fields, osv
from datetime import datetime,timedelta
import time
from mx import DateTime
class partner_employee_golongan_history(osv.Model):
    _name = "partner.employee.golongan.history"
    _description = "Riwayat Kepangkatan"
    _columns = {
        'name': fields.char("No SK", size=100,required=True ),
        'partner_id': fields.many2one('res.partner', 'Data Pegawai',required=True),
        'golongan_id_history': fields.many2one('partner.employee.golongan', 'Golongan',required=True),
        'date': fields.date("TMT.Pangkat", required=True ),
        'jenis'     : fields.selection([('cpns', 'CPNS'), 
                                             ('pns', 'PNS'),
                                             ('pangkat_golongan', 'Kenaikan Pangkat/Golongan'),
                                            ], 'Jenis SK', required=True),
        'attachment_file': fields.binary("Upload SK", ),
    }
    _order = "date desc"
partner_employee_golongan_history()
class partner_employee_study_history(osv.Model):
    _name = "partner.employee.study.history"
    _description = "Riwayat Pendidikan"
    _columns = {
        'name': fields.char("No Ijazah", size=100, ),
        'partner_id': fields.many2one('res.partner', 'Data Pegawai',required=True),
        'nama_sekolah_id_history': fields.many2one('partner.employee.school', 'Sekolah',required=True),
        'jurusan_id_history': fields.many2one('partner.employee.study', 'Jurusan',),
        'jenjang_pendidikan_id_history': fields.many2one('partner.employee.study.type', 'Jenjang Pendidikan',required=True,),
        'tahun_lulus': fields.char("Tahun Lulus", size=4 ,required=True),
        'ipk': fields.float("IPK", ),
        'attachment_file': fields.binary("Upload Ijazah", ),
                
    }
    _order = "tahun_lulus desc"
    
partner_employee_study_history()
class partner_employee_hukuman_disiplin_history(osv.Model):
    _name = "partner.employee.hukuman.disiplin.history"
    _description = "Riwayat Hukuman Disiplin"
    _columns = {
        'partner_id': fields.many2one('res.partner', 'Data Pegawai',required=True),
        'date': fields.date("TMT. Hukuman", required=True ),
        'lama_waktu'  : fields.integer("Lama Waktu (Bulan)",),
        'date_end'  : fields.date("Tgl AKhir Hukuman",required=True),
        'name'      : fields.selection([('ringan', 'Ringan'), 
                                             ('sedang', 'Sedang'),
                                             ('berat', 'Berat'),
                                            ], 'Jenis Hukuman Disiplin', required=True),
        'notes': fields.text("Uraian",required=True ),
    }
    _order = "date desc"
partner_employee_hukuman_disiplin_history()
class partner_employee_job_history(osv.Model):
    _name = "partner.employee.job.history"
    _description = "Riwayat Jabatan"
    _columns = {
        'name': fields.char("No SK", size=100,required=False ),
        'partner_id': fields.many2one('res.partner', 'Data Pegawai',required=True),
        'job_id_history': fields.many2one('partner.employee.job', 'Jabatan',required=True),
        'job_type': fields.selection([('struktural', 'Jabatan Struktural'), ('jft', 'Jabatan Fungsional Tertentu'), ('jfu', 'Jabatan Fungsional Umum')], 'Jenis Jabatan',required=False),
        'eselon_id': fields.many2one('partner.employee.eselon', 'Eselon'),
        'department_id': fields.many2one('partner.employee.department', 'Unit Kerja'),
        'date': fields.date("TMT. Jabatan", required=True ),
        'company_name': fields.char("BALAI", size=200,placeholder="Badan/Dinas ....."),
        'instansi_name': fields.char("Instansi", size=200,placeholder="Pemerintahan Provinsi/Daerah ...."),
        'attachment_file': fields.binary("Upload SK", ),
                
    }
    _order = "date desc"
partner_employee_job_history()

class partner_employee_diklat_kepemimpinan_history(osv.Model):
    _name = "partner.employee.diklat.kepemimpinan.history"
    _description = "Riwayat Diklat Kepemimpinan"
    _columns = {
        'name': fields.char("Nama", size=500,required=True ),
        'partner_id': fields.many2one('res.partner', 'Data Pegawai',required=True),
        'tahun': fields.char("Tahun Lulus", size=4),
        'tempat': fields.char("Tempat", size=120),
        'penyelenggara': fields.char("Penyelenggara", size=320),
        'type_id': fields.many2one('diklat.type', 'Jenjang',domain="[('type','=','kepemimpinan')]" ),
        
    }
    _order = "tahun desc"
partner_employee_diklat_kepemimpinan_history()

class partner_employee_diklat_fungsional_history(osv.Model):
    _name = "partner.employee.diklat.fungsional.history"
    _description = "Riwayat Diklat Fungsional"
    _columns = {
        'name': fields.char("Nama", size=500,required=True ),
        'partner_id': fields.many2one('res.partner', 'Data Pegawai',required=True),
        'tahun': fields.char("Tahun Lulus", size=4),
        'tempat': fields.char("Tempat", size=120),
        'penyelenggara': fields.char("Penyelenggara", size=320),
        'type_id': fields.many2one('diklat.type', 'Jenjang',domain="[('type','=','fungsional')]" ),
    }
    _order = "tahun desc"
partner_employee_diklat_fungsional_history()

class partner_employee_diklat_teknik_history(osv.Model):
    _name = "partner.employee.diklat.teknik.history"
    _description = "Riwayat Diklat Teknik"
    _columns = {
        'name': fields.char("Nama", size=500,required=True ),
        'partner_id': fields.many2one('res.partner', 'Data Pegawai',required=True),
        'tahun': fields.char("Tahun Lulus", size=4),
        'tempat': fields.char("Tempat", size=120),
        'penyelenggara': fields.char("Penyelenggara", size=320),
        'type_id': fields.many2one('diklat.type', 'Jenjang',domain="[('type','=','teknik')]" ),
    }
    _order = "tahun desc"
partner_employee_diklat_teknik_history()

class partner_employee_penataran_history(osv.Model):
    _name = "partner.employee.penataran.history"
    _description = "Riwayat Penataran"
    _columns = {
        'name': fields.char("Nama", size=500,required=True ),
        'partner_id': fields.many2one('res.partner', 'Data Pegawai',required=True),
        'tahun': fields.char("Tahun Lulus", size=4),
        'tempat': fields.char("Tempat", size=120),
        'penyelenggara': fields.char("Penyelenggara", size=320),
        
    }
    _order = "tahun desc"
partner_employee_penataran_history()


class partner_employee_bidang_kompetensi_history(osv.Model):
    _name = "partner.employee.bidang.kompetensi.history"
    _description = "Riwayat Bidang Kompetensi"
    _columns = {
        'name': fields.char("No SK", size=50,required=True ),
        'partner_id': fields.many2one('res.partner', 'Data Pegawai',required=True),
        'date': fields.date("TMT.Kompetensi", ),
        'bidang_kompetensi_type_id': fields.many2one('bidang.kompetensi.type', 'Bidang Kompetensi'),
        'type_id': fields.many2one('diklat.type', 'Jenjang',domain="[('type','=','kompetensi')]" ),
    }
    _order = "date desc"
partner_employee_bidang_kompetensi_history()


#-----------------
class temp_partner_employee_data(osv.Model):
    _name = "temp.partner.employee.data"
    _description = "Temporari data pegawai"
    _columns = {

        'name': fields.char ('NAMA', size=400 ),
        'nip': fields.char ('NIP', size=30 ),
        'f1': fields.char ('Notes', size=400 ),
        'f2': fields.char ('NO_DATA', size=400 ),
        'f3': fields.char ('OPD', size=400 ),
        'f4': fields.char ('NIP_SPACE', size=400 ),
        'f5': fields.char ('KARPEG', size=400 ),
        'f6': fields.char ('tempat_lahir', size=400 ),
        'f7': fields.char ('tanggal_lahir', size=400 ),
        'f8': fields.char ('jenis_kelamin', size=400 ),
        'f9': fields.char ('GOL', size=400 ),
        'f10': fields.char ('TMT GOL', size=400 ),
        'f11': fields.char ('TMT CPNS', size=400 ),
        'f12': fields.char ('TMT PNS', size=400 ),
        'f14': fields.char ('JABATAN', size=400 ),
        'f15': fields.char ('KODE', size=400 ),
        'f16': fields.char ('ESELON 1', size=400 ),
        'f17': fields.char ('ESELON 2', size=400 ),
        'f18': fields.char ('ESELON 3', size=400 ),
        'f19': fields.char ('ESELON 4', size=400 ),
        'f20': fields.char ('ESL', size=400 ),
        'f21': fields.char ('JENIS JAB', size=400 ),
        'f22': fields.char ('TAHUN', size=400 ),
        'f23': fields.char ('BULAN', size=400 ),
        'f24': fields.char ('TMT JAB', size=400 ),
        'f25': fields.char ('TAHUN', size=400 ),
        'f26': fields.char ('BULAN', size=400 ),
        'f27': fields.char ('DIKLAT', size=400 ),
        'f28': fields.char ('JENJANG', size=400 ),
        'f29': fields.char ('THN', size=400 ),
        'f30': fields.char ('street1', size=400 ),
        'f31': fields.char ('city', size=400 ),
        'f32': fields.char ('state', size=400 ),
        'f33': fields.char ('postalcode', size=400 ),
        'f34': fields.char ('TELP', size=400 ),


    }
class temp_partner_employee_job_history(osv.Model):
    _name = "temp.partner.employee.job.history"
    _description = "Riwayat Jabatan"
    _columns = {
        'name': fields.char("No SK", size=100 ),
        'data_pegawai': fields.char ('Data Pegawai', size=400 ),
        'nip': fields.char ('nip', size=30 ),
        'jabatan': fields.char ('Jabatan', size=400 ),
        'job_type': fields.selection([('struktural', 'Jabatan Struktural'), ('jft', 'Jabatan Fungsional Tertentu'), ('jfu', 'Jabatan Fungsional Umum')], 'Jenis Jabatan',required=False),
        'eselon': fields.char ('Eselon', size=30 ),
        'unit_kerja': fields.char ('Unit Kerja', size=400 ),
        'date': fields.date("TMT. Jabatan", required=True ),
        'company_name': fields.char("BALAI", size=200,placeholder="Badan/Dinas ....."),
        'instansi_name': fields.char("Instansi", size=200,placeholder="Pemerintahan Provinsi/Daerah ...."),
                
    }
    _order = "date desc"
temp_partner_employee_job_history()
class temp_partner_employee_golongan_history(osv.Model):
    _name = "temp.partner.employee.golongan.history"
    _description = "Riwayat Kepangkatan"
    _columns = {
        'name': fields.char("No SK", size=100 ),
        'data_pegawai': fields.char ('Data Pegawai', size=400 ),
        'nip': fields.char ('nip', size=30 ),
        'golongan': fields.char ('Golongan', size=50 ),
        'date': fields.date("TMT.Pangkat", required=True ),
        'jenis'     : fields.selection([('cpns', 'CPNS'), 
                                             ('pns', 'PNS'),
                                             ('pangkat_golongan', 'Kenaikan Pangkat/Golongan'),
                                            ], 'Jenis SK', required=True),
                
    }
    _order = "date desc"
temp_partner_employee_golongan_history()

#ID    NIP    Nama    BALAI    NIP Atasan    Nama Atasan    Nip Banding    Nama Banding
class temp_partner_employee_atasan(osv.Model):
    _name = "temp.partner.employee.atasan"
    _description = "Daftar Atasan Pegawai"
    _columns = {
        'nip_pegawai': fields.char ('nip', size=30 ),
        'nama_pegawai': fields.char ('Nama Pegawai', size=400 ),
        'nip_atasan': fields.char ('nip atasan', size=30 ),
        'nama_atasan': fields.char ('Nama Atasan', size=400 ),
        'nip_banding': fields.char ('nip banding', size=30 ),
        'nama_banding': fields.char ('Nama Banding', size=400 ),
       'opd': fields.char ('Nama BALAI', size=400 ),
    }
    
temp_partner_employee_atasan()

class temp_diklat_fungsional(osv.Model):
    _name = "temp.diklat.fungsional"
    _description = "Temp Diklat Fungsional"
    _columns = {
        'nip': fields.char ('nip', size=30 ),
        'nama_pegawai': fields.char ('nama_pegawai', size=400 ),
        'tahun': fields.char ('tahun', size=4 ),
        'penyelenggara': fields.char ('penyelenggara', size=400 ),
        'name': fields.char ('sk', size=100 ),
        'tipe_diklat': fields.char ('tipe_diklat', size=400 ),
       'tempat': fields.char ('tempat', size=400 ),
    }
    
temp_diklat_fungsional()
class temp_diklat_kepemimpinan(osv.Model):
    _name = "temp.diklat.kepemimpinan"
    _description = "Temp Diklat kepemimpinan"
    _columns = {
        'nip': fields.char ('nip', size=30 ),
        'nama_pegawai': fields.char ('nama_pegawai', size=400 ),
        'tahun': fields.char ('tahun', size=4 ),
        'penyelenggara': fields.char ('penyelenggara', size=400 ),
        'name': fields.char ('sk', size=100 ),
        'tipe_diklat': fields.char ('tipe_diklat', size=400 ),
       'tempat': fields.char ('tempat', size=400 ),
    }
    
temp_diklat_kepemimpinan()
class temp_diklat_teknik(osv.Model):
    _name = "temp.diklat.teknik"
    _description = "Temp Diklat teknik"
    _columns = {
        'nip': fields.char ('nip', size=30 ),
        'nama_pegawai': fields.char ('nama_pegawai', size=400 ),
        'tahun': fields.char ('tahun', size=4 ),
        'penyelenggara': fields.char ('penyelenggara', size=400 ),
        'name': fields.char ('sk', size=100 ),
        'tipe_diklat': fields.char ('tipe_diklat', size=400 ),
       'tempat': fields.char ('tempat', size=400 ),
    }
    
temp_diklat_teknik()
class temp_diklat_penataran(osv.Model):
    _name = "temp.diklat.penataran"
    _description = "Temp Diklat penataran"
    _columns = {
        'nip': fields.char ('nip', size=30 ),
        'nama_pegawai': fields.char ('nama_pegawai', size=400 ),
        'tahun': fields.char ('tahun', size=4 ),
        'penyelenggara': fields.char ('penyelenggara', size=400 ),
        'name': fields.char ('sk', size=100 ),
        'tempat': fields.char ('tempat', size=400 ),
    }
    
temp_diklat_penataran()
#nip    nama_pegawai    name    tahun_lulus    ipk    nama_sekolah_id_history    jurusan_id_history    jenjang_pendidikan_id_history
class temp_riwayat_sekolah(osv.Model):
    _name = "temp.riwayat.sekolah"
    _description = "Temp Riwayat Sekolah"
    _columns = {
        'nip': fields.char ('nip', size=30 ),
        'nama_pegawai': fields.char ('nama_pegawai', size=400 ),
        'name': fields.char ('sk', size=100 ),
        'tahun_lulus': fields.char ('tahun', size=4 ),
        'ipk': fields.float("IPK", ),
        'nama_sekolah_id_history': fields.char ('nama_sekolah_id_history', size=400 ),
        'jurusan_id_history': fields.char ('jurusan_id_history', size=400 ),
        'jenjang_pendidikan_id_history': fields.char ('jenjang_pendidikan_id_history', size=40 ),
    }
    
temp_riwayat_sekolah()


