{
    "name": "Rekapitulasi Kegiatan Per BALAI",
    "version": "1.0",
    "author": "Darmawan Fatriananda",
    "category": "Penilaian Prestasi Kerja / Rekapitulasi",
    "description": "Laporan Rekapitulasi Kegiatan Per BALAI ",
    "website" : "www.mediasee.net",
    "license" : "GPL-3",
    "depends": ['df_skp_employee'],
    'data': ["skp_recap_report.xml",],
    'installable': True,
    'active': False,
}
