# -*- encoding: utf-8 -*-
##############################################################################
#
#    Darmawan Fatriananda
#    BKD Pemprov Jabar
#    Copyright (c) 2014 <http://www.asdarfat.wordpress.com.com>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see http://www.gnu.org/licenses/.
#
##############################################################################

from openerp.osv import fields, osv
from openerp.tools.translate import _
from datetime import datetime,timedelta
import time
import openerp.addons.decimal_precision as dp

class project_skp(osv.Model):
    _inherit = 'project.skp'
    #copy from project_skp
    def write(self, cr, uid, ids, vals, context=None):
        if not isinstance(ids, list): ids = [ids]
        super(project_skp, self).write(cr, uid, ids, vals, context=context)
        return True
    
    _columns = {
        'state'                 : fields.selection([('draft', 'Draft'), ('realisasi', 'Realisasi'),
                                    ('propose', 'Atasan'), ('rejected_manager', 'Pengajuan Ditolak Atasan'),
                                    ('appeal', 'Banding'), ('evaluated', 'Verifikatur'), ('rejected_bkd', 'Pengajuan Ditolak Verifikatur'),
                                    ('propose_to_close','Pengajuan Closing Target'),('closed','Closed'),
                                    ('propose_to_review','Review'),
                                    ('done', 'Selesai'), 
                                    ('cancelled', 'Cancel')],
                                   'Status Pekerjaan'),
        'is_review'                : fields.boolean('Direview', ),
        'is_dpa_review'                : fields.boolean('Review DPA', ),
        'review_notes'                : fields.text('Catatan Review', ),
        'review_realiasasi_biaya'     : fields.float('Review Realiasasi Biaya'),
        'review_realiasasi_jumlah_kuantitas_output'     : fields.integer('Review Kuantitas Ouput', required=False),
        'review_realiasasi_kualitas'     : fields.float('Review Kualitas', required=False,digits_compute=dp.get_precision('no_digit')),
        'review_realiasasi_waktu'     : fields.integer('Review Waktu', required=False),
        'review_realiasasi_angka_kredit'     : fields.float('Review Angka Kredit', required=False,digits_compute=dp.get_precision('angka_kredit')),
        'nilai_sebelum_review': fields.float('Nilai Sebelum Review'),
    }
    def action_propose_review_dpa(self,cr,uid,ids,context=None):
        if not ids: return []
        dummy, view_id = self.pool.get('ir.model.data').get_object_reference(cr, uid, 'df_project_consolidation', 'action_propose_review_popup_form_view')
        if self.is_user(cr, uid, ids, context):
            task_obj = self.browse(cr, uid, ids[0], context=context)
            return {
                'name':_("Pengajuan Review DPA"),
                'view_mode': 'form',
                'view_id': view_id,
                'view_type': 'form',
                'res_model': 'project.skp.propose.review',
                'type': 'ir.actions.act_window',
                'nodestroy': True,
                'target': 'new',
                'domain': '[]',
                'context': {
                    'default_review_realiasasi_biaya': 0,
                    'default_is_review': True,
                    'default_is_dpa_review': True,
                    'default_nilai_sebelum_review':task_obj.nilai_akhir,
                    'default_task_id':task_obj.id
    
                }
            }
        return False
    def action_propose_review_by_atasan(self,cr,uid,ids,context=None):
        if not ids: return []
        dummy, view_id = self.pool.get('ir.model.data').get_object_reference(cr, uid, 'df_project_consolidation', 'action_propose_review_byatasan_popup_form_view')
        if self.is_manager(cr, uid, ids, context):
            task_obj = self.browse(cr, uid, ids[0], context=context)
            hide_by_target_type_id=False
            if task_obj.target_type_id == 'dpa_opd_biro':
                hide_by_target_type_id=True
            return {
                'name':_("Pengajuan Review Kegiatan Staff"),
                'view_mode': 'form',
                'view_id': view_id,
                'view_type': 'form',
                'res_model': 'project.skp.propose.review',
                'type': 'ir.actions.act_window',
                'nodestroy': True,
                'target': 'new',
                'domain': '[]',
                'context': {
                    'default_review_realiasasi_biaya': 0,
                    'default_review_realiasasi_jumlah_kuantitas_output': 0,
                    'default_review_realiasasi_kualitas': 0,
                    'default_review_realiasasi_waktu': 0,
                    'default_review_realiasasi_angka_kredit': 0,
                    'default_is_review': True,
                    'default_is_dpa_review': False,
                    'default_hide_by_target_type_id':hide_by_target_type_id,
                    'default_nilai_sebelum_review':task_obj.nilai_akhir,
                    'default_task_id':task_obj.id
    
                }
            }
        return False
   
    def action_review_accepted(self, cr, uid, ids, context=None):
        """ Review Diterima Lalu Hitung Ulang 
        """
        if self.is_verificator(cr, uid, ids, context) :
            self.do_recalculate_poin(cr, uid, ids, context=context) #must a list
            for task_obj in self.browse(cr,uid,ids,context=context):
               penambahan_nilai_biaya = task_obj.review_realiasasi_biaya
               realisasi_biaya = task_obj.realisasi_biaya + penambahan_nilai_biaya
               self.write(cr, uid, task_obj.id, {'realisasi_biaya':realisasi_biaya,'use_target_for_calculation':False}, context=context)
            self.action_done(cr, uid, ids, context=context) #must a list
        return True;
    def action_review_accepted_byatasan(self, cr, uid, ids, context=None):
        """ Review Dari Atasan Diterima Lalu Hitung Ulang 
        """
        vals = {}
        if self.is_verificator(cr, uid, ids, context) :
            self.do_recalculate_poin(cr, uid, ids, context=context) #must a list
            for task_obj in self.browse(cr,uid,ids,context=context):
               vals = {}
               realiasasi_jumlah_kuantitas_output = task_obj.review_realiasasi_jumlah_kuantitas_output
               realiasasi_kualitas = task_obj.review_realiasasi_kualitas
               realiasasi_waktu = task_obj.review_realiasasi_waktu
               
               realiasasi_biaya = task_obj.review_realiasasi_biaya
               realiasasi_angka_kredit = task_obj.review_realiasasi_angka_kredit
               vals.update({   'realisasi_jumlah_kuantitas_output':realiasasi_jumlah_kuantitas_output,
                               'realisasi_kualitas':realiasasi_kualitas,
                               'realisasi_waktu':realiasasi_waktu,
                               'use_target_for_calculation':False,
                               'realisasi_angka_kredit':realiasasi_angka_kredit
                            })
               if task_obj.target_type_id != 'dpa_opd_biro' :
                   vals.update({'realisasi_biaya':realiasasi_biaya})
             
                   
               self.write(cr, uid, task_obj.id, vals, context=context)
            self.action_done(cr, uid, ids, context=context) #must a list
        return True;
    def action_review_rejected(self, cr, uid, ids, context=None):
        """ Review Dinilai Status Kembali Ke Done
        """
        vals = {}
        if self.is_verificator(cr, uid, ids, context) :
            vals.update({            'is_review' : False,
                                     'is_dpa_review' : False,
                                    'review_notes' : None,
                                    'review_realiasasi_jumlah_kuantitas_output':None,
                                    'review_realiasasi_kualitas':None,
                                    'review_realiasasi_waktu':None,
                                    'review_realiasasi_biaya':None,
                                    'review_realiasasi_angka_kredit':None,
                                    'nilai_sebelum_review':None,
                                     })
            self.write(cr, uid, ids, vals, context=context)
            return self.set_done(cr, uid, ids, context=context)
project_skp()


