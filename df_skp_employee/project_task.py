# -*- encoding: utf-8 -*-
##############################################################################
#
#    Darmawan Fatriananda
#    BKD Pemprov Jabar
#    Copyright (c) 2014 <http://www.asdarfat.wordpress.com.com>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see http://www.gnu.org/licenses/.
#
##############################################################################

from openerp.osv import fields, osv
from datetime import datetime,timedelta
import time
from mx import DateTime


class project_skp(osv.Model):
    _inherit = 'project.skp'
    def action_done(self, cr, uid, ids, context=None):
        if not isinstance(ids, list): ids = [ids]
        super(project_skp, self).action_done(cr, uid, ids, context=context)   
        self.do_monthly_skp_summary_calculation(cr, uid, ids,1, context=context)
        return True
    def do_recalculate_poin(self, cr, uid, ids, context=None):
         if not isinstance(ids, list): ids = [ids]
         super(project_skp, self).do_recalculate_poin(cr, uid, ids, context=context)   
         self.do_monthly_skp_summary_calculation(cr, uid, ids,-1, context=context)
         return True;
    def action_done_use_target(self, cr, uid, ids, context=None):
         if not isinstance(ids, list): ids = [ids]
         super(project_skp, self).action_done_use_target(cr, uid, ids, context=context)   
         self.do_monthly_skp_summary_calculation(cr, uid, ids,1, context=context)
         return True;
    def do_monthly_skp_summary_calculation(self, cr, uid, ids, sign, context=None):
        """ BKD->Done (Rekap Summary) """
        # Summary Calculation

        skp_employee_pool = self.pool.get('skp.employee')
        if not isinstance(ids, list): ids = [ids]
        for task_obj in self.browse(cr, uid, ids, context=context):
                domain= task_obj.employee_id.id,task_obj.target_period_year,task_obj.target_period_month
                query = """ SELECT  id,coalesce(nilai_perilaku_percent,0),coalesce(nilai_skp_percent,0),coalesce(nilai_skp,0),
                        coalesce(fn_nilai_tambahan,0), coalesce(fn_nilai_kreatifitas,0)
                        FROM SKP_EMPLOYEE WHERE employee_id = %s
                        AND target_period_year = %s
                        AND target_period_month = %s
                                """
                cr.execute(query,domain)
                rs_skp_emp = cr.fetchall()
                #print "end  search skp emp"
                if not rs_skp_emp:
                        company_id = task_obj.employee_id.company_id and task_obj.employee_id.company_id.id or None
                        biro_id = task_obj.employee_id.biro_id and task_obj.employee_id.biro_id.id or None
                        department_id = task_obj.employee_id.department_id and task_obj.employee_id.department_id.id or None
                        is_kepala_opd = task_obj.employee_id.is_kepala_opd

                        self.pool.get('skp.employee').insert_skp_employee_query(cr,task_obj.employee_id.id,task_obj.user_id.id,task_obj.target_period_year,
                                                                                task_obj.target_period_month,company_id,department_id,
                                                                                biro_id,is_kepala_opd)

                        cr.execute(query,domain)
                        rs_skp_emp = cr.fetchall()

                for skp_emp_id,nilai_perilaku_percent,nilai_skp_percent,nilai_skp,fn_nilai_tambahan,fn_nilai_kreatifitas in rs_skp_emp:

                        skp_state_count,jml_skp,jml_all_skp,nilai_skp,nilai_skp_percent=skp_employee_pool._get_detail_query_skp(cr, uid, task_obj.user_id.id,task_obj.target_period_month,task_obj.target_period_year, context=None)
                        if nilai_skp and nilai_skp>0:
                            nilai_skp_tambahan_percent = ( nilai_skp * 0.6 )  + (fn_nilai_tambahan or 0) + (fn_nilai_kreatifitas or 0)
                        else :
                            nilai_skp_tambahan_percent =0.0

                        nilai_total = nilai_perilaku_percent+nilai_skp_tambahan_percent
                        nilai_tpp=0

                        #skp_employee_pool.write(cr , uid,skp_emp_obj['id'], update_values, context=None)
                        update_query = """ UPDATE SKP_EMPLOYEE
                        SET skp_state_count = %s,
                        jml_skp = %s,
                        jml_all_skp = %s,
                        nilai_skp = %s,
                        nilai_skp_percent = %s,
                        nilai_skp_tambahan_percent = %s,
                        nilai_total = %s,
                        nilai_tpp = %s
                        where id = %s

                                """
                        cr.execute(update_query, (skp_state_count, jml_skp,jml_all_skp,
                                                  nilai_skp,nilai_skp_percent,nilai_skp_tambahan_percent,
                                                  nilai_total,nilai_tpp ,
                                                  skp_emp_id))

                    
        return True;
    def do_target_summary_calculation(self, cr, uid, target_obj,  context=None):
        
        target_pool = self.pool.get('project.project')
        if target_obj:
            data_target  = target_pool._get_akumulasi_target_realisasi_tahunan(cr, uid, target_obj, context=context)
            if data_target:
                        update_values = {
                            'total_target_jumlah_kuantitas_output': data_target['total_target_jumlah_kuantitas_output'],
                            'total_target_angka_kredit': data_target['total_target_angka_kredit'],
                            'total_target_kualitas': data_target['total_target_kualitas'],
                            'total_target_waktu': data_target['total_target_waktu'],
                            'total_target_biaya': data_target['total_target_biaya'],
                            'total_realisasi_jumlah_kuantitas_output': data_target['total_realisasi_jumlah_kuantitas_output'],
                            'total_realisasi_angka_kredit': data_target['total_realisasi_angka_kredit'],
                            'total_realisasi_kualitas': data_target['total_realisasi_kualitas'],
                            'total_realisasi_waktu': data_target['total_realisasi_waktu'],
                            'total_realisasi_biaya': data_target['total_realisasi_biaya'],
                            'total_realisasi_biaya': data_target['total_realisasi_biaya'],
                            'total_nilai_target_skp': data_target['total_nilai_target_skp'],
                            'total_capaian_target_skp': data_target['total_capaian_target_skp'],
                            'total_target_satuan_kuantitas_output':data_target['total_target_satuan_kuantitas_output'],
                            'total_realisasi_satuan_kuantitas_output':data_target['total_realisasi_satuan_kuantitas_output'],
                             'count_of_done': data_target['count_of_done'],
                            'count_of_aspect': data_target['count_of_aspect'],
                        }
                        target_pool.write(cr , uid,target_obj.id, update_values, context=None)
        return True;
    def do_skp_summary_calculation(self, cr,uid, user_id,employee_id, target_period_year,  context=None):
        
        sey_pool = self.pool.get('skp.employee.yearly')
        data_skp_summary  = sey_pool._get_akumulasi_realiasai_per_bulan(cr, user_id,employee_id, target_period_year, context=context)
        skp_yearly_ids = sey_pool.search(cr, uid, [('employee_id', '=', employee_id.id),
                                                     ('target_period_year', '=', target_period_year),
                                                   ], context=None)
        if not skp_yearly_ids:
                        values = {
                            'employee_id': employee_id.id,
                            'user_id': user_id.id,
                            'target_period_year': target_period_year,
                        }
                        new_skp_yearly_id=sey_pool.create(cr , uid, values, context=None)
                        skp_yearly_ids.append(new_skp_yearly_id)
                        
        if data_skp_summary and skp_yearly_ids :
            for skp_yearly_obj in  sey_pool.browse(cr, uid, skp_yearly_ids, context=context):
                        count_of_month = data_skp_summary['count_of_month']
                        if data_skp_summary:
                            update_values = {
                                'jml_skp': data_skp_summary['jml_skp'],
                                'jml_skp_done': data_skp_summary['jml_skp_done'],
                                'jml_realisasi_skp':data_skp_summary['jml_realisasi_skp'],
                                'nilai_skp': data_skp_summary['nilai_skp']   ,
                                'nilai_skp_tambahan': data_skp_summary['nilai_skp_tambahan']   ,
                                'nilai_skp_percent': data_skp_summary['nilai_skp_percent'],
                                'nilai_skp_tambahan_percent': data_skp_summary['nilai_skp_tambahan_percent'],
                                'nilai_total': data_skp_summary['nilai_total'],
                            }
                            sey_pool.write(cr , uid,skp_yearly_obj.id, update_values, context=None)
        return True;
    
project_skp()
class project_tambahan_kreatifitas(osv.Model):
    _inherit = 'project.tambahan.kreatifitas'
    def action_done(self, cr, uid, ids, context=None):
        if not isinstance(ids, list): ids = [ids]
        super(project_tambahan_kreatifitas, self).action_done(cr, uid, ids, context=context)   
        self.do_monthly_tambahan_kreatifitas_summary_calculation(cr, uid, ids, context=context)
        return True
    def do_recalculate_poin(self, cr, uid, ids, context=None):
         if not isinstance(ids, list): ids = [ids]
         super(project_tambahan_kreatifitas, self).do_recalculate_poin(cr, uid, ids, context=context)   
         self.do_monthly_tambahan_kreatifitas_summary_calculation(cr, uid, ids, context=context)
         return True;
    def action_done_use_target(self, cr, uid, ids, context=None):
         if not isinstance(ids, list): ids = [ids]
         super(project_tambahan_kreatifitas, self).action_done_use_target(cr, uid, ids, context=context)   
         self.do_monthly_tambahan_kreatifitas_summary_calculation(cr, uid, ids, context=context)
         return True;
    def do_monthly_tambahan_kreatifitas_summary_calculation(self, cr, uid, ids,  context=None):
        """ BKD->Done (Rekap Summary) """
        # Summary Calculation
        skp_employee_pool = self.pool.get('skp.employee')
        if not isinstance(ids, list): ids = [ids]
        for task_obj in self.browse(cr, uid, ids, context=context):
                domain= task_obj.employee_id.id,task_obj.target_period_year,task_obj.target_period_month
                query = """ SELECT  id,coalesce(nilai_perilaku_percent,0),coalesce(nilai_skp_percent,0),coalesce(nilai_skp,0)
                        FROM SKP_EMPLOYEE WHERE employee_id = %s
                        AND target_period_year = %s
                        AND target_period_month = %s
                                """
                cr.execute(query,domain)
                rs_skp_emp = cr.fetchall()
                #print "end  search skp emp"


                if not rs_skp_emp:

                        company_id = task_obj.employee_id.company_id and task_obj.employee_id.company_id.id or None
                        biro_id = task_obj.employee_id.biro_id and task_obj.employee_id.biro_id.id or None
                        department_id = task_obj.employee_id.department_id and task_obj.employee_id.department_id.id or None
                        is_kepala_opd = task_obj.employee_id.is_kepala_opd

                        self.pool.get('skp.employee').insert_skp_employee_query(cr,task_obj.employee_id.id,task_obj.user_id.id,task_obj.target_period_year,
                                                                                task_obj.target_period_month,company_id,department_id,
                                                                                biro_id,is_kepala_opd)
                        cr.execute(query,domain)
                        rs_skp_emp = cr.fetchall()

                for skp_emp_id,nilai_perilaku_percent,nilai_skp_percent,nilai_skp in rs_skp_emp:

                        #only single task in one month
                        fn_nilai_tambahan = task_obj.nilai_tambahan
                        fn_nilai_kreatifitas = task_obj.nilai_kreatifitas
                        if nilai_skp and nilai_skp > 0:
                            nilai_skp_tambahan_percent = ( nilai_skp * 0.6 )  + (fn_nilai_tambahan or 0) + (fn_nilai_kreatifitas or 0)
                        else :
                            nilai_skp_tambahan_percent =0.0

                        nilai_total = nilai_perilaku_percent+nilai_skp_tambahan_percent
                        nilai_tpp=0

                        update_query = """ UPDATE SKP_EMPLOYEE
                        SET fn_nilai_tambahan = %s,
                        fn_nilai_kreatifitas = %s,
                        nilai_skp_tambahan_percent = %s,
                        nilai_total = %s,
                        nilai_tpp = %s
                        where id = %s

                                """
                        cr.execute(update_query, (fn_nilai_tambahan, fn_nilai_kreatifitas,nilai_skp_tambahan_percent,
                                                  nilai_total,nilai_tpp ,
                                                  skp_emp_id))


        return True;

    def do_tambahan_kreatifitas_summary_calculation(self, cr,uid, user_id,employee_id, target_period_year,  context=None):
        
        sey_pool = self.pool.get('skp.employee.yearly')
        data_skp_summary  = sey_pool._get_akumulasi_realiasai_per_bulan(cr, user_id,employee_id, target_period_year, context=context)
        skp_yearly_ids = sey_pool.search(cr, uid, [('employee_id', '=', employee_id.id),
                                                     ('target_period_year', '=', target_period_year),
                                                   ], context=None)
        if not skp_yearly_ids:
                        values = {
                            'employee_id': employee_id.id,
                            'user_id': user_id.id,
                            'target_period_year': target_period_year,
                        }
                        new_skp_yearly_id=sey_pool.create(cr , uid, values, context=None)
                        skp_yearly_ids.append(new_skp_yearly_id)
                        
        if data_skp_summary and skp_yearly_ids :
            for skp_yearly_obj in  sey_pool.browse(cr, uid, skp_yearly_ids, context=context):
                        #count_of_month = data_skp_summary['count_of_month']
                        if data_skp_summary:
                            update_values = {
                                'nilai_skp_tambahan': data_skp_summary['nilai_skp_tambahan']   ,
                                'nilai_skp_tambahan_percent': data_skp_summary['nilai_skp_tambahan_percent'],
                                'fn_nilai_tambahan': data_skp_summary['fn_nilai_tambahan'],
                                'fn_nilai_kreatifitas': data_skp_summary['fn_nilai_kreatifitas'],
                                'nilai_total': data_skp_summary['nilai_total'],
                            }
                            sey_pool.write(cr , uid,skp_yearly_obj.id, update_values, context=None)
        return True;
    
project_tambahan_kreatifitas()
class project_perilaku(osv.Model):
    _inherit = 'project.perilaku'
    def action_done(self, cr, uid, ids, context=None):
        if not isinstance(ids, list): ids = [ids]

        #print "start action done"
        super(project_perilaku, self).action_done(cr, uid, ids, context=context)
        #print "end act done"

        self.do_monthly_perilaku_summary_calculation(cr, uid, ids, 1,context=context)

        return True
    def do_recalculate_poin(self, cr, uid, ids, context=None):
         if not isinstance(ids, list): ids = [ids]
         super(project_perilaku, self).do_recalculate_poin(cr, uid, ids, context=context)
         self.do_monthly_perilaku_summary_calculation(cr, uid, ids, -1,context=context)
         return True;
    def action_done_use_target(self, cr, uid, ids, context=None):
         if not isinstance(ids, list): ids = [ids]
         super(project_perilaku, self).action_done_use_target(cr, uid, ids, context=context)
         self.do_monthly_perilaku_summary_calculation(cr, uid, ids, 1,context=context)
         return True;
    def do_monthly_perilaku_summary_calculation(self, cr, uid, ids,sign,  context=None):
        """ BKD->Done (Rekap Summary) """
        # Summary Calculation

        #print "start  start monthly calc"
        skp_employee_pool = self.pool.get('skp.employee')
        if not isinstance(ids, list): ids = [ids]

        for task_obj in self.browse(cr, uid, ids, context=context):

                #print "start  search skp emp"
                domain= task_obj.employee_id.id,task_obj.target_period_year,task_obj.target_period_month
                query = """ SELECT  id,coalesce(nilai_skp_percent,0),coalesce(nilai_skp_tambahan_percent,0)
                        FROM SKP_EMPLOYEE WHERE employee_id = %s
                        AND target_period_year = %s
                        AND target_period_month = %s
                                """
                cr.execute(query,domain)
                rs_skp_emp = cr.fetchall()
                #print "end  search skp emp"


                if not rs_skp_emp:
                        company_id = task_obj.employee_id.company_id and task_obj.employee_id.company_id.id or None
                        biro_id = task_obj.employee_id.biro_id and task_obj.employee_id.biro_id.id or None
                        department_id = task_obj.employee_id.department_id and task_obj.employee_id.department_id.id or None
                        is_kepala_opd = task_obj.employee_id.is_kepala_opd

                        self.pool.get('skp.employee').insert_skp_employee_query(cr,task_obj.employee_id.id,task_obj.user_id.id,task_obj.target_period_year,
                                                                                task_obj.target_period_month,company_id,department_id,
                                                                                biro_id,is_kepala_opd)

                        cr.execute(query,domain)
                        rs_skp_emp = cr.fetchall()

                #print "stat main calc  search skp emp"
                for skp_emp_id,nilai_skp_percent,nilai_skp_tambahan_percent in rs_skp_emp:
                        if sign > 0 :
                            jml_perilaku =1
                            nilai_perilaku =task_obj.nilai_akhir
                        else :
                            jml_perilaku =0
                            nilai_perilaku =0

                        nilai_perilaku_percent =  nilai_perilaku * 0.4
                        nilai_total = nilai_skp_tambahan_percent+nilai_perilaku_percent
                        nilai_tpp=0

                        #print "start do write (manual query)"
                        #skp_employee_pool.write(cr , uid,skp_emp_id, update_values, context=None)
                        update_query = """ UPDATE SKP_EMPLOYEE
                        SET jml_perilaku = %s,
                        nilai_perilaku = %s,
                        nilai_perilaku_percent = %s,
                        nilai_pelayanan = %s,
                        nilai_integritas = %s,
                        nilai_komitmen = %s,
                        nilai_disiplin = %s,
                        nilai_kerjasama = %s,
                        nilai_kepemimpinan = %s,
                        nilai_total = %s,
                        nilai_tpp = %s
                        where id = %s

                                """
                        cr.execute(update_query, (jml_perilaku, nilai_perilaku,nilai_perilaku_percent,
                                                task_obj.nilai_pelayanan,task_obj.nilai_integritas,
                                                 task_obj.nilai_komitmen,task_obj.nilai_disiplin, task_obj.nilai_kerjasama,
                                                 task_obj.nilai_kepemimpinan,nilai_total,nilai_tpp ,
                                                  skp_emp_id))
                        #print "end do write"
                #kalkulasi akumulasi perilaku tahunan
                #print "end main calc  search skp emp"

                #print "start  do_perilaku_summary_calculation"
                self.do_perilaku_summary_calculation(cr,uid,task_obj.user_id,task_obj.employee_id,task_obj.target_period_year)
                #print "end  do_perilaku_summary_calculation"


        #print "end  start monthly calc"
        return True;
    def do_task_summary_calculation(self, cr, uid, ids,  context=None):
        """ BKD->Done (Rekap Summary) """
        # Summary Calculation
        
        skp_employee_pool = self.pool.get('skp.employee')
        if not isinstance(ids, list): ids = [ids]

        for task_obj in self.browse(cr, uid, ids, context=context):
                skp_employee_ids = skp_employee_pool.search(cr, uid, [('employee_id', '=', task_obj.employee_id.id),
                                                   ('target_period_year', '=', task_obj.target_period_year),
                                                   ('target_period_month', '=', task_obj.target_period_month),
                                                   ], context=None)
                if not skp_employee_ids:
                        values = {
                            'employee_id': task_obj.employee_id.id,
                            'user_id': task_obj.user_id.id,
                            'target_period_year': task_obj.target_period_year,
                            'target_period_month':task_obj.target_period_month,
                        }
                        new_skp_employee_id = skp_employee_pool.create(cr , uid, values, context=None)
                        skp_employee_ids.append(new_skp_employee_id)


                skp_emp_list = skp_employee_pool.read(cr, uid, skp_employee_ids, ['nilai_skp_percent','nilai_skp_tambahan_percent','id'], context=context)


                for skp_emp_obj in skp_emp_list:


                        jml_perilaku,nilai_perilaku,nilai_perilaku_percent,nilai_pelayanan,nilai_disiplin,nilai_komitmen,nilai_integritas,nilai_kerjasama,nilai_kepemimpinan = skp_employee_pool._get_detail_nilai_all_perilaku(cr,uid,task_obj.user_id.id,task_obj.target_period_month,task_obj.target_period_year,context)


                        nilai_total = skp_emp_obj['nilai_skp_tambahan_percent']+nilai_perilaku_percent
                        nilai_tpp=0
                        update_values = {
                            'jml_perilaku': jml_perilaku,
                            'nilai_perilaku': nilai_perilaku,
                            'nilai_perilaku_percent': nilai_perilaku_percent,
                            'nilai_pelayanan': nilai_pelayanan,
                             'nilai_integritas': nilai_integritas,
                             'nilai_komitmen':nilai_komitmen,
                             'nilai_disiplin': nilai_disiplin,
                             'nilai_kerjasama': nilai_kerjasama,
                             'nilai_kepemimpinan':nilai_kepemimpinan,
                            'nilai_total': nilai_total,
                            'nilai_tpp': nilai_tpp,
                        }
                        skp_employee_pool.write(cr , uid,skp_emp_obj['id'], update_values, context=None)
                #kalkulasi akumulasi perilaku tahunan

                self.do_perilaku_summary_calculation(cr,uid,task_obj.user_id,task_obj.employee_id,task_obj.target_period_year)

                    

        return True;
    def do_perilaku_summary_calculation(self, cr,uid, user_id,employee_id, target_period_year,  context=None):
        
        sey_pool = self.pool.get('skp.employee.yearly')
        data_skp_summary  = sey_pool._get_akumulasi_realisasi_perilaku_per_bulan(cr, user_id,employee_id, target_period_year, context=context)
        skp_yearly_ids = sey_pool.search(cr, uid, [('employee_id', '=', employee_id.id),
                                                     ('target_period_year', '=', target_period_year),
                                                   ], context=None)
        if not skp_yearly_ids:
                        values = {
                            'employee_id': employee_id.id,
                            'user_id': user_id.id,
                            'target_period_year': target_period_year,
                        }
                        new_skp_yearly_id=sey_pool.create(cr , uid, values, context=None)
                        skp_yearly_ids.append(new_skp_yearly_id)
                        
        if data_skp_summary and skp_yearly_ids :
            for skp_yearly_obj in  sey_pool.browse(cr, uid, skp_yearly_ids, context=context):
                        count_of_month = data_skp_summary['count_of_month']
                        if data_skp_summary:
                            nilai_total = skp_yearly_obj.nilai_skp_tambahan_percent + data_skp_summary['nilai_perilaku_percent']
                            indeks_nilai_total = sey_pool.get_indeks_nilai(cr,uid,nilai_total,context=None)
                            update_values = {
                                'jml_perilaku': data_skp_summary['jml_perilaku'],
                                'nilai_perilaku': data_skp_summary['nilai_perilaku'],
                                'nilai_perilaku_percent': data_skp_summary['nilai_perilaku_percent'],
                                'nilai_total': round(nilai_total,2),
                                'nilai_pelayanan': data_skp_summary['nilai_pelayanan'],
                                'nilai_integritas': data_skp_summary['nilai_integritas'],
                                'nilai_komitmen': data_skp_summary['nilai_komitmen'],
                                'nilai_disiplin': data_skp_summary['nilai_disiplin'],
                                'nilai_kerjasama': data_skp_summary['nilai_kerjasama'],
                                'nilai_kepemimpinan': data_skp_summary['nilai_kepemimpinan'],
                                'indeks_nilai_pelayanan': data_skp_summary['indeks_nilai_pelayanan'],
                                'indeks_nilai_integritas': data_skp_summary['indeks_nilai_integritas'],
                                'indeks_nilai_komitmen': data_skp_summary['indeks_nilai_komitmen'],
                                'indeks_nilai_disiplin': data_skp_summary['indeks_nilai_disiplin'],
                                'indeks_nilai_kerjasama': data_skp_summary['indeks_nilai_kerjasama'],
                                'indeks_nilai_kepemimpinan': data_skp_summary['indeks_nilai_kepemimpinan'],
                                'indeks_nilai_total': indeks_nilai_total,
                            }
                            sey_pool.write(cr , uid,skp_yearly_obj.id, update_values, context=None)
        return True;
    
project_perilaku()
