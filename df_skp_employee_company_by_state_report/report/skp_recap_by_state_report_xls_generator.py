import time
import xlwt
import cStringIO
from xlwt import Workbook, Formula
from report_engine_xls import report_xls
from skp_recap_by_state_report_parser import skp_recap_by_state_report_parser

class skp_recap_by_state_report_xls_generator(report_xls):
	
    def generate_xls_report(self, parser, filters, obj, workbook):
	worksheet = workbook.add_sheet(('Laporan Realisasi Bulanan'))
        worksheet.panes_frozen = True
        worksheet.remove_splits = True
        worksheet.portrait = True # Landscape
        worksheet.fit_wiresult_datah_to_pages = 1
        worksheet.col(1).wiresult_datah = len("ABCDEFG")*1024
        
        # Styles (It's used for writing rows / headers)
        info_style = xlwt.easyxf('font: height 220, name Arial, colour_index black, bold on, italic off; align: wrap on, , vert centre, horiz center;pattern: pattern solid, fore_color white;', num_format_str='#,##0.00;(#,##0.00)')
        row_normal_style=  xlwt.easyxf("borders: top thin, bottom thin, left thin, right thin;",num_format_str='#,##0.00;(#,##0.00)')
        row_normal_odd_style=  xlwt.easyxf('pattern: pattern solid, fore_color silver_ega;',num_format_str='#,##0.00;(#,##0.00)')
        top_style = xlwt.easyxf('font: height 200, name Arial, colour_index black, bold on, italic off; align: wrap on, vert centre, horiz center;pattern: pattern solid, fore_color white;', num_format_str='#,##0.00;(#,##0.00)')
        header_style = xlwt.easyxf('font: height 200, name Arial, colour_index black, bold on, italic off; align: wrap on, vert centre, horiz center;pattern: pattern solid, fore_color silver_ega;', num_format_str='#,##0.00;(#,##0.00)')
        header_w_border_style = xlwt.easyxf('font: height 200, name Arial, colour_index black, bold on, italic off; align: wrap on, vert centre, horiz center;pattern: pattern solid, fore_color silver_ega;borders: top thin, bottom thin, left thin, right thin;', num_format_str='#,##0.00;(#,##0.00)')
        sum_style = xlwt.easyxf('font: height 200, name Arial, colour_index black, bold on, italic off; align: wrap on, vert centre;pattern: pattern solid, fore_color white;', num_format_str='#,##0.00;(#,##0.00)')
        int_number_style=  xlwt.easyxf("borders: top thin, bottom thin, left thin, right thin;",num_format_str='#,##0;(#,##0)')
		
	# Specifying columns, the order doesn't matter
	# lamda d,f,p: is a function who has filter,data,parser as the parameters it is expected to the value of the column
        cols_specs = [
	    # Infos
	    ('Company', 20, 0, 'text', lambda x, d, p: p.get_title('',filters)),
	    ('Title',  20, 0, 'text', lambda x, d, p: 'Laporan Realisasi Bulanan'),
	    ('Period',  20, 0, 'text', lambda x, d, p: p.get_period(filters)),
	    # Main Headers / Rows
	    ('No', 1, 20, 'number', lambda x, d, p: 0,xlwt.Row.set_cell_number,int_number_style),
	    ('Nama Pegawai', 1, 150, 'text', lambda x, d, p:  d['employee_name'] or ''),
	    ('NIP', 1, 90, 'text', lambda x, d, p:  d['employee_nip'] or '' ),
	    ('Jabatan', 1, 150, 'text', lambda x, d, p: d['jabatan'] or ''),
	    ('Nama BALAI', 1, 150, 'text', lambda x, d, p: d['company_name'] or ''),
	    ('Unit Kerja', 1, 150, 'text', lambda x, d, p: d['unit_kerja'] or ''),
	    ('Eselon', 1, 70, 'text', lambda x, d, p: d['eselon'] or ''),
	    ('Golongan', 1, 70, 'text', lambda x, d, p: d['golongan'] or ''),
	    ('Jns Jabatan', 1, 50, 'text', lambda x, d, p: d['job_type'] or ''),
	    ('Status SKP', 1, 100, 'text', lambda x, d, p: d['skp_state_count'] or ''),
	    ('Realisasi', 1, 40, 'number', lambda x, d, p: d['count_skp_realisasi'],xlwt.Row.set_cell_number,int_number_style),
	    ('Atasan', 1, 40, 'number', lambda x, d, p: d['count_skp_atasan'],xlwt.Row.set_cell_number,int_number_style),
	    ('Verifikatur', 1, 40, 'number', lambda x, d, p: d['count_skp_bkd'],xlwt.Row.set_cell_number,int_number_style),
	    ('Selesai', 1, 40, 'number', lambda x, d, p: d['count_skp_done'],xlwt.Row.set_cell_number,int_number_style),
	    ('Jumlah SKP', 1, 40, 'number', lambda x, d, p: d['count_skp_all'],xlwt.Row.set_cell_number,int_number_style),
	    ('SKP', 1, 40, 'number', lambda x, d, p:  d['nilai_skp']),
	    ('Perilaku', 1, 40, 'number', lambda x, d, p: d['nilai_perilaku']),
	    ('Tugas Tambahan', 1, 40, 'number', lambda x, d, p: d['nilai_tugas_tambahan']),
	    ('Kreatifitas', 1, 40, 'number', lambda x, d, p:  d['nilai_kreatifitas']),
	    ('Nilai Total', 1, 50, 'number', lambda x, d, p:   d['nilai_total']),
	    
        # Misc
	    ('single_empty_column', 1, 0, 'text', lambda x, d, p: ''),
	    ('triple_empty_column', 3, 0, 'text', lambda x, d, p: ''),
	    ('quadruple_empty_column', 4, 0, 'text', lambda x, d, p: ''),
	]
    
			 
        row_spec_value = ['No','Nama Pegawai','NIP','Jabatan','Jns Jabatan','Eselon','Golongan','Unit Kerja','Nama BALAI','Status SKP'
						,'Realisasi','Atasan','Verifikatur','Selesai','Jumlah SKP'
						,'SKP','Perilaku','Tugas Tambahan','Kreatifitas','Nilai Total']
       
        # Row templates (Order Matters, this joins the columns that are specified in the second parameter)
        company_template = self.xls_row_template(cols_specs, ['Company'])
        title_template = self.xls_row_template(cols_specs, ['Title'])
        period_template = self.xls_row_template(cols_specs, ['Period'])
        row_template = self.xls_row_template(cols_specs,row_spec_value)
        empty_row_template = self.xls_row_template(cols_specs, ['single_empty_column'])
        
        # Write infos
        # xls_write_row(worksheet, filters, data parser, row_number, template, style)
        
        self.xls_write_row(worksheet, filters, None, parser, 0, title_template, info_style)
        self.xls_write_row(worksheet, filters, None, parser, 1, company_template, info_style)
        self.xls_write_row(worksheet, filters, None, parser, 2, period_template, info_style)

            # Write headers (It uses the first parameter of cols_specs)
        self.xls_write_row_header(worksheet, 4, row_template, header_w_border_style, set_column_size=True)
        style = row_normal_style
        row_count = 5
        idx=1
        result = parser.get_skp_recap_by_state_report_raw(filters);
        for skp_recap_by_state_data in result:
            # Write Rows
                style = row_normal_style
                self.xls_write_row_with_indeks(worksheet, filters, skp_recap_by_state_data, parser, row_count, row_template, style,idx)
                idx+=1
                row_count+=1

        # Write Totals

    # Override from report_engine_xls.py	
    def create_source_xls(self, cr, uid, ids, filters, report_xml, context=None): 
        if not context: context = {}
	
	# Avoiding context's values change
        context_clone = context.copy()
	
        rml_parser = self.parser(cr, uid, self.name2, context=context_clone)
        objects = self.getObjects(cr, uid, ids, context=context_clone)
        rml_parser.set_context(objects, filters, ids, 'xls')
        io = cStringIO.StringIO()
        workbook = xlwt.Workbook(encoding='utf-8')
        self.generate_xls_report(rml_parser, filters, rml_parser.localcontext['objects'], workbook)
        
        workbook.save(io)
        io.seek(0)
        
        return (io.read(), 'xls')

#Start the reporting service
skp_recap_by_state_report_xls_generator(
    #name (will be referred from skp_recap_by_state_report.py, must add "report." as prefix)
    'report.skp.recap.by.state.xls',
    #model
    'skp.recap.by.state.report',
    #file
    'addons/df_skp_employee_company_by_state_report/report/skp_recap_by_state_report.xls',
    #parser
    parser=skp_recap_by_state_report_parser,
    #header
    header=True
)