# -*- encoding: utf-8 -*-
##############################################################################
#
#    Darmawan Fatriananda
#    BKD Pemprov Jabar
#    Copyright (c) 2014 <http://www.asdarfat.wordpress.com.com>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see http://www.gnu.org/licenses/.
#
##############################################################################

from datetime import datetime, date
from lxml import etree
import time

from openerp import SUPERUSER_ID
from openerp import tools
from openerp.osv import fields, osv
import openerp.addons.decimal_precision as dp
from openerp import workflow
from openerp.tools.translate import _

class project_perilaku(osv.Model):
    _name = 'project.perilaku'
    _description='Realisasi Perilaku' 
    
    _inherit = ['mail.thread', 'ir.needaction_mixin']
    def write(self, cr, uid, ids, vals, context=None):
        if not isinstance(ids, list): ids = [ids]
        for task_obj in self.browse(cr, uid, ids, context=context):
            task_id = task_obj.id
            if uid != 1:
                    if task_obj.state in ('draft', 'realisasi', 'rejected_manager', 'cancelled'):
                        if not self.is_user(cr, uid, ids, context) :
                            return False
                    if task_obj.state in ('propose', 'rejected_bkd'):
                        if not self.is_manager(cr, uid, ids, context) :
                            return False
                    if task_obj.state in ('appeal'):
                        if not self.is_appeal_manager(cr, uid, ids, context) :
                            return False
                    if task_obj.state in ('evaluated'):
                        if not self.is_verificator(cr, uid, ids, context) :
                            return False
                    if task_obj.state in ('done'):
                        if not self.is_verificator(cr, uid, ids, context) :
                            return False
            
        super(project_perilaku, self).write(cr, uid, ids, vals, context=context)           
        return True
    
   
    
    _columns = {
        'name': fields.char('Nama Kegiatan', size=500, readonly=True,),
        'code'     : fields.char('Kode Kegiatan', size=20,),
        'active': fields.boolean('Active', help="If the active field is set to False, it will allow you to hide the project without removing it."),
        'target_period_month'     : fields.selection([('01', 'Januari'), ('02', 'Februari'),
                                                      ('03', 'Maret'), ('04', 'April'),
                                                      ('05', 'Mei'), ('06', 'Juni'),
                                                      ('07', 'Juli'), ('08', 'Agustus'),
                                                      ('09', 'September'), ('10', 'Oktober'),
                                                      ('11', 'November'), ('12', 'Desember')], 'Periode Bulan'
                                                     ),
        'state': fields.selection([('draft', 'Draft'), ('realisasi', 'Realisasi'),
                                        ('propose', 'Atasan'), ('rejected_manager', 'Pengajuan Ditolak Atasan'),
                                        ('appeal', 'Banding'), ('evaluated', 'Verifikatur'), ('rejected_bkd', 'Pengajuan Ditolak Verifikatur'),
                                        ('propose_to_close','Pengajuan Closing Target'),('closed','Closed'),
                                        ('done', 'Selesai'), ('cancelled', 'Cancel')], 'Status Pekerjaan'),
        'target_period_year'     : fields.char('Periode Tahun', size=4, required=True),
        
        'user_id': fields.many2one('res.users', 'Pejabat Penanggung Jawab',),
        'user_id_atasan': fields.many2one('res.users', 'Pejabat Penilai',),
        'user_id_banding': fields.many2one('res.users', 'Atasan Pejabat Penilai',),
        'user_id_bkd': fields.many2one('res.users', 'Pejabat Pengevaluasi (BKD)',),
        'notes'     : fields.text('Catatan', states={'draft': [('required', False)]}),
        'notes_atasan'     : fields.text('Catatan Atasan', ),
        'notes_atasan_banding'     : fields.text('Catatan Atasan Banding', ),
        'notes_bkd'     : fields.text('Catatan Petugas Verifikasi', ),
        'is_suggest': fields.boolean('Tambahkan Koreski Penilaian'),
        'is_appeal': fields.boolean('Tambahkan Koreski Banding'),
        'is_control': fields.boolean('Verifikasi Penilaian'),
        
        #poin Perilaku 
        'realisasi_jumlah_konsumen_pelayanan'     : fields.integer('Jumlah Pelayanan'),
        'realisasi_jumlah_tidakpuas_pelayanan'     : fields.integer('Jumlah Ketidakpuasan Pelayanan'),
        'realisasi_satuan_jumlah_konsumen_pelayanan': fields.many2one('satuan.hitung', 'Satuan Nilai Konsumen'),
        'realisasi_ketepatan_laporan_spj'     : fields.many2one('acuan.penilaian', 'Ketepatan Laporan SPJ', domain=[('type', '=', 'orientasi'),('kategori_orientasi', '=', 'ketepatan_laporan_spj'),('active', '=', True)] ),
        'realisasi_ketepatan_laporan_ukp4'     : fields.many2one('acuan.penilaian', 'Ketepatan Laporan UKP4', domain=[('type', '=', 'orientasi'),('kategori_orientasi', '=', 'ketepatan_laporan_ukp4'),('active', '=', True)] ), 
        'realisasi_efisiensi_biaya_operasional'     : fields.many2one('acuan.penilaian', 'Efisiensi Biaya Operasional Kantor', domain=[('type', '=', 'orientasi'),('kategori_orientasi', '=', 'efisiensi_biaya_operasional'),('active', '=', True)] ),
        'realisasi_apel_pagi'     : fields.integer('Jumlah Apel Pagi'),
        'realisasi_hadir_apel_pagi'     : fields.integer('Kehadiran Apel Pagi'),
        'realisasi_upacara_hari_besar': fields.integer('Upacara Hari Besar'),
        'realisasi_integritas_presiden': fields.boolean('Presiden'),
        'realisasi_integritas_gubernur': fields.boolean('Gubernur'),
        'realisasi_integritas_kepalaopd': fields.boolean('Kepala BALAI'),
        'realisasi_integritas_atasan': fields.boolean('Atasan Langsung'),
        'realisasi_integritas_lainlain': fields.boolean('Lain Lain'),
        'realisasi_integritas_es1': fields.boolean('Pejabat Eselon I'),
        'realisasi_integritas_es2': fields.boolean('Pejabat Eselon II'),
        'realisasi_integritas_es3': fields.boolean('Pejabat Eselon III'),
        'realisasi_integritas_es4': fields.boolean('Pejabat Eselon IV'),
        'realisasi_integritas_hukuman': fields.selection([('ya', 'Terkena Hukuman Disiplin'), ('tidak', 'Tidak Terkena Hukuman Disiplin')],'Ada Hukuman Disiplin',help="Jika Ada Hukuman Disiplin, Atau Tidak "),#fields.boolean('Ada Hukuman Disiplin',help="Jika Ada Hukuman Disiplin, Ceklis Di Kotak Ini "),
        'realisasi_integritas_hukuman_ringan': fields.boolean('Ringan'),
        'realisasi_integritas_hukuman_sedang': fields.boolean('Sedang'),
        'realisasi_integritas_hukuman_berat': fields.boolean('Berat'),
        'realisasi_hadir_hari_kerja'     : fields.integer('Kehadiran Hari Kerja'),
        'realisasi_hadir_jam_kerja': fields.integer('Kehadiran Jam Kerja'),
        'realisasi_jumlah_hari_kerja'     : fields.integer('Jumlah Hari Kerja'),
        'realisasi_jumlah_jam_kerja': fields.integer('Jumlah Jam Kerja'),
        'realisasi_hadir_upacara_hari_besar': fields.integer('Kehadiran Upacara Hari Besar'),
        'realisasi_kerjasama_nasional': fields.boolean('Nasional'),
        'realisasi_kerjasama_gubernur': fields.boolean('Provinsi'),
        'realisasi_kerjasama_kepalaopd': fields.boolean('BALAI'),
        'realisasi_kerjasama_atasan': fields.boolean('Atasan Langsung'),
        'realisasi_kerjasama_lainlain': fields.boolean('Lain Lain'),
        'realisasi_kerjasama_rapat_atasan': fields.boolean('Rapat Atasan Langsung'),
        'realisasi_kerjasama_rapat_perangkat_daerah': fields.boolean('Rapat Perangkat Daerah'),
        'realisasi_kerjasama_rapat_provinsi': fields.boolean('Rapat Provinsi'),
        'realisasi_kerjasama_rapat_nasional': fields.boolean('Rapat Nasional'),
        'realisasi_kepemimpinan_nasional': fields.boolean('Nasional'),
        'realisasi_kepemimpinan_gubernur': fields.boolean('Provinsi'),
        'realisasi_kepemimpinan_kepalaopd': fields.boolean('BALAI'),
        'realisasi_kepemimpinan_atasan': fields.boolean('Atasan Langsung'),
        'realisasi_kepemimpinan_lainlain': fields.boolean('Lain Lain'),
        'realisasi_kepemimpinan_narsum_unitkerja': fields.boolean('Narasumber Kerja Atasan Langsung'),
        'realisasi_kepemimpinan_narsum_perangkat_daerah': fields.boolean('Narasumber Perangkat Daerah'),
        'realisasi_kepemimpinan_narsum_provinsi': fields.boolean('Narasumber Provinsi'),
        'realisasi_kepemimpinan_narsum_nasional': fields.boolean('Narasumber Nasional'),
        
        'suggest_jumlah_konsumen_pelayanan'     : fields.integer('Jumlah Pelayanan'),
        'suggest_jumlah_tidakpuas_pelayanan'     : fields.integer('Jumlah Ketidakpuasan Pelayanan'),
        'suggest_satuan_jumlah_konsumen_pelayanan': fields.many2one('satuan.hitung', 'Satuan Nilai Konsumen'),
        'suggest_ketepatan_laporan_spj'     : fields.many2one('acuan.penilaian', 'Ketepatan Laporan SPJ', domain=[('type', '=', 'orientasi'),('kategori_orientasi', '=', 'ketepatan_laporan_spj'),('active', '=', True)] ),
        'suggest_ketepatan_laporan_ukp4'     : fields.many2one('acuan.penilaian', 'Ketepatan Laporan UKP4', domain=[('type', '=', 'orientasi'),('kategori_orientasi', '=', 'ketepatan_laporan_ukp4'),('active', '=', True)] ), 
        'suggest_efisiensi_biaya_operasional'     : fields.many2one('acuan.penilaian', 'Efisiensi Biaya Operasional Kantor', domain=[('type', '=', 'orientasi'),('kategori_orientasi', '=', 'efisiensi_biaya_operasional'),('active', '=', True)] ),
        'suggest_apel_pagi'     : fields.integer('Jumlah Apel Pagi'),
        'suggest_hadir_apel_pagi'     : fields.integer('Kehadiran Apel Pagi'),
        'suggest_upacara_hari_besar': fields.integer('Kehadiran Upacara Hari Besar'),
        'suggest_integritas_presiden': fields.boolean('Presiden'),
        'suggest_integritas_gubernur': fields.boolean('Gubernur'),
        'suggest_integritas_kepalaopd': fields.boolean('Kepala BALAI'),
        'suggest_integritas_atasan': fields.boolean('Atasan Langsung'),
        'suggest_integritas_lainlain': fields.boolean('Lain Lain'),
        'suggest_integritas_es1': fields.boolean('Pejabat Eselon I'),
        'suggest_integritas_es2': fields.boolean('Pejabat Eselon II'),
        'suggest_integritas_es3': fields.boolean('Pejabat Eselon III'),
        'suggest_integritas_es4': fields.boolean('Pejabat Eselon IV'),
        'suggest_integritas_hukuman': fields.selection([('ya', 'Terkena Hukuman Disiplin'), ('tidak', 'Tidak Terkena Hukuman Disiplin')],'Ada Hukuman Disiplin',help="Jika Ada Hukuman Disiplin, Atau Tidak "),
        'suggest_integritas_hukuman_ringan': fields.boolean('Ringan'),
        'suggest_integritas_hukuman_sedang': fields.boolean('Sedang'),
        'suggest_integritas_hukuman_berat': fields.boolean('Berat'),
        'suggest_hadir_hari_kerja'     : fields.integer('Kehadiran Hari Kerja'),
        'suggest_hadir_jam_kerja': fields.integer('Kehadiran Jam Kerja'),
        'suggest_jumlah_hari_kerja'     : fields.integer('Jumlah Hari Kerja'),
        'suggest_jumlah_jam_kerja': fields.integer('Jumlah Jam Kerja'),
        'suggest_hadir_upacara_hari_besar': fields.integer('Kehadiran Upacara Hari Besar'),
        'suggest_kerjasama_nasional': fields.boolean('Nasional'),
        'suggest_kerjasama_gubernur': fields.boolean('Provinsi'),
        'suggest_kerjasama_kepalaopd': fields.boolean('BALAI'),
        'suggest_kerjasama_atasan': fields.boolean('Atasan Langsung'),
        'suggest_kerjasama_lainlain': fields.boolean('Lain Lain'),
        'suggest_kerjasama_rapat_atasan': fields.boolean('Rapat Atasan Langsung'),
        'suggest_kerjasama_rapat_perangkat_daerah': fields.boolean('Rapat Perangkat Daerah'),
        'suggest_kerjasama_rapat_provinsi': fields.boolean('Rapat Provinsi'),
        'suggest_kerjasama_rapat_nasional': fields.boolean('Rapat Nasional'),
        'suggest_kepemimpinan_nasional': fields.boolean('Nasional'),
        'suggest_kepemimpinan_gubernur': fields.boolean('Provinsi'),
        'suggest_kepemimpinan_kepalaopd': fields.boolean('BALAI'),
        'suggest_kepemimpinan_atasan': fields.boolean('Atasan Langsung'),
        'suggest_kepemimpinan_lainlain': fields.boolean('Lain Lain'),
        'suggest_kepemimpinan_narsum_unitkerja': fields.boolean('Narasumber Kerja Atasan Langsung'),
        'suggest_kepemimpinan_narsum_perangkat_daerah': fields.boolean('Narasumber Perangkat Daerah'),
        'suggest_kepemimpinan_narsum_provinsi': fields.boolean('Narasumber Provinsi'),
        'suggest_kepemimpinan_narsum_nasional': fields.boolean('Narasumber Nasional'),
        
        'appeal_jumlah_konsumen_pelayanan'     : fields.integer('Jumlah Pelayanan'),
        'appeal_jumlah_tidakpuas_pelayanan'     : fields.integer('Jumlah Ketidakpuasan Pelayanan'),
        'appeal_satuan_jumlah_konsumen_pelayanan': fields.many2one('satuan.hitung', 'Satuan Nilai Konsumen'),
        'appeal_ketepatan_laporan_spj'     : fields.many2one('acuan.penilaian', 'Ketepatan Laporan SPJ', domain=[('type', '=', 'orientasi'),('kategori_orientasi', '=', 'ketepatan_laporan_spj'),('active', '=', True)] ),
        'appeal_ketepatan_laporan_ukp4'     : fields.many2one('acuan.penilaian', 'Ketepatan Laporan UKP4', domain=[('type', '=', 'orientasi'),('kategori_orientasi', '=', 'ketepatan_laporan_ukp4'),('active', '=', True)] ), 
        'appeal_efisiensi_biaya_operasional'     : fields.many2one('acuan.penilaian', 'Efisiensi Biaya Operasional Kantor', domain=[('type', '=', 'orientasi'),('kategori_orientasi', '=', 'efisiensi_biaya_operasional'),('active', '=', True)] ),
        'appeal_apel_pagi'     : fields.integer('Jumlah Apel Pagi'),
        'appeal_hadir_apel_pagi'     : fields.integer('Kehadiran Apel Pagi'),
        'appeal_upacara_hari_besar': fields.integer('Kehadiran Upacara Hari Besar'),
        'appeal_integritas_presiden': fields.boolean('Presiden'),
        'appeal_integritas_gubernur': fields.boolean('Gubernur'),
        'appeal_integritas_kepalaopd': fields.boolean('Kepala BALAI'),
        'appeal_integritas_atasan': fields.boolean('Atasan Langsung'),
        'appeal_integritas_lainlain': fields.boolean('Lain Lain'),
        'appeal_integritas_es1': fields.boolean('Pejabat Eselon I'),
        'appeal_integritas_es2': fields.boolean('Pejabat Eselon II'),
        'appeal_integritas_es3': fields.boolean('Pejabat Eselon III'),
        'appeal_integritas_es4': fields.boolean('Pejabat Eselon IV'),
        'appeal_integritas_hukuman': fields.selection([('ya', 'Terkena Hukuman Disiplin'), ('tidak', 'Tidak Terkena Hukuman Disiplin')],'Ada Hukuman Disiplin',help="Jika Ada Hukuman Disiplin, Atau Tidak "),
        'appeal_integritas_hukuman_ringan': fields.boolean('Ringan'),
        'appeal_integritas_hukuman_sedang': fields.boolean('Sedang'),
        'appeal_integritas_hukuman_berat': fields.boolean('Berat'),
        'appeal_hadir_hari_kerja'     : fields.integer('Kehadiran Hari Kerja'),
        'appeal_hadir_jam_kerja': fields.integer('Kehadiran Jam Kerja'),
        'appeal_jumlah_hari_kerja'     : fields.integer('Jumlah Hari Kerja'),
        'appeal_jumlah_jam_kerja': fields.integer('Jumlah Jam Kerja'),
        'appeal_hadir_upacara_hari_besar': fields.integer('Kehadiran Upacara Hari Besar'),
        'appeal_kerjasama_nasional': fields.boolean('Nasional'),
        'appeal_kerjasama_gubernur': fields.boolean('Provinsi'),
        'appeal_kerjasama_kepalaopd': fields.boolean('BALAI'),
        'appeal_kerjasama_atasan': fields.boolean('Atasan Langsung'),
        'appeal_kerjasama_lainlain': fields.boolean('Lain Lain'),
        'appeal_kerjasama_rapat_atasan': fields.boolean('Rapat Atasan Langsung'),
        'appeal_kerjasama_rapat_perangkat_daerah': fields.boolean('Rapat Perangkat Daerah'),
        'appeal_kerjasama_rapat_provinsi': fields.boolean('Rapat Provinsi'),
        'appeal_kerjasama_rapat_nasional': fields.boolean('Rapat Nasional'),
        'appeal_kepemimpinan_nasional': fields.boolean('Nasional'),
        'appeal_kepemimpinan_gubernur': fields.boolean('Provinsi'),
        'appeal_kepemimpinan_kepalaopd': fields.boolean('BALAI'),
        'appeal_kepemimpinan_atasan': fields.boolean('Atasan Langsung'),
        'appeal_kepemimpinan_lainlain': fields.boolean('Lain Lain'),
        'appeal_kepemimpinan_narsum_unitkerja': fields.boolean('Narasumber Kerja Atasan Langsung'),
        'appeal_kepemimpinan_narsum_perangkat_daerah': fields.boolean('Narasumber Perangkat Daerah'),
        'appeal_kepemimpinan_narsum_provinsi': fields.boolean('Narasumber Provinsi'),
        'appeal_kepemimpinan_narsum_nasional': fields.boolean('Narasumber Nasional'),
        
        'lookup_apel_pagi'     : fields.integer('Jumlah Apel Pagi', readonly=True),
        'lookup_jumlah_hari_kerja'     : fields.integer('Jumlah Hari Kerja', readonly=True),
        'lookup_jumlah_jam_kerja'     :fields.integer('Jumlah Jam Kerja', readonly=True),
        'lookup_upacara_hari_besar'     : fields.integer('Upacara Hari Besar', readonly=True),
        'lookup_biaya'     : fields.float('Verifikasi Biaya', readonly=True),
        #end Poin Perilaku
                
        'control_count': fields.integer('Jumlah Pengajuan Verifikasi', readonly=True),
        'nilai_akhir': fields.float('Nilai', readonly=True),
        'nilai_sementara': fields.float('Nilai Sementara', readonly=True),
        
        'nilai_kepemimpinan': fields.float('Nilai Kepemimpinan', readonly=True),
        'nilai_kerjasama': fields.float('Nilai Kerjasama', readonly=True),
        'nilai_integritas': fields.float('Nilai Integritas', readonly=True),
        'nilai_komitmen': fields.float('Nilai Komitmen', readonly=True),
        'nilai_disiplin': fields.float('Nilai Disiplin', readonly=True),
        'nilai_pelayanan': fields.float('Nilai Pelayanan', readonly=True),

        'jumlah_perhitungan': fields.float('Jumlah Perhitungan', readonly=True),
        'indeks_nilai': fields.char('Indeks', size=20, readonly=True),
        'komitmen_verifikasi_notsame': fields.boolean('Status Verifikasi Komitmen',help="Aspek Komitmen Berbeda "),
        
        'company_id': fields.many2one('res.company', 'BALAI'),
        'currency_id': fields.many2one('res.currency', 'Currency'),
        'employee_id': fields.related('user_id', 'partner_id',   type="many2one", relation='res.partner', string='Tipe Jabatan', store=True),
        'employee_job_type': fields.selection([('struktural', 'Jabatan Struktural'), ('jft', 'Jabatan Fungsional Tertentu'), ('jfu', 'Jabatan Fungsional Umum')], 'Tipe Jabatan'),
        'is_kepala_opd': fields.related('employee_id', 'is_kepala_opd', type='boolean', string='Tugas Untuk Kepala BALAI', store=True),
        'use_target_for_calculation': fields.boolean('Perhitungan Menggunakan Target',help="Pengakuan perhitungan berdasar target, Bukan Realisasi"),
        
        #attachment    
        'realisasi_integritas_presiden_attach': fields.char('No Bukti',size=50,placeholder='No SK/Bukti....'),
        'realisasi_integritas_gubernur_attach': fields.char('No Bukti',size=50,placeholder='No SK/Bukti....'),
        'realisasi_integritas_kepalaopd_attach': fields.char('No Bukti',size=50,placeholder='No SK/Bukti....'),
        'realisasi_integritas_es1_attach': fields.char('No Bukti',size=50,placeholder='No SK/Bukti....'),
        'realisasi_integritas_es2_attach': fields.char('No Bukti',size=50,placeholder='No SK/Bukti....'),
        
        'realisasi_kerjasama_nasional_attach': fields.char('No Bukti',size=50,placeholder='No SK/Bukti....'),
        'realisasi_kerjasama_gubernur_attach': fields.char('No Bukti',size=50,placeholder='No SK/Bukti....'),
        'realisasi_kerjasama_kepalaopd_attach': fields.char('No Bukti',size=50,placeholder='No SK/Bukti....'),
        'realisasi_kerjasama_lainlain_attach': fields.char('No Bukti',size=50,placeholder='No SK/Bukti....'),
        'realisasi_kerjasama_rapat_provinsi_attach': fields.char('No Bukti',size=50,placeholder='No SK/Bukti....'),
        'realisasi_kerjasama_rapat_nasional_attach': fields.char('No Bukti',size=50,placeholder='No SK/Bukti....'),
        
        'realisasi_kepemimpinan_nasional_attach': fields.char('No Bukti',size=50,placeholder='No SK/Bukti....'),
        'realisasi_kepemimpinan_gubernur_attach': fields.char('No Bukti',size=50,placeholder='No SK/Bukti....'),
        'realisasi_kepemimpinan_kepalaopd_attach': fields.char('No Bukti',size=50,placeholder='No SK/Bukti....'),
        'realisasi_kepemimpinan_lainlain_attach': fields.char('No Bukti',size=50,placeholder='No SK/Bukti....'),
        'realisasi_kepemimpinan_narsum_unitkerja_attach': fields.char('No Bukti',size=50,placeholder='No SK/Bukti....'),
        'realisasi_kepemimpinan_narsum_provinsi_attach': fields.char('No Bukti',size=50,placeholder='No SK/Bukti....'),
        'realisasi_kepemimpinan_narsum_nasional_attach': fields.char('No Bukti',size=50,placeholder='No SK/Bukti....'),
        
        #date
        'realisasi_integritas_presiden_date' :fields.date('Tanggal SK/Bukti'),
        'realisasi_integritas_gubernur_date' :fields.date('Tanggal SK/Bukti'),
        'realisasi_integritas_kepalaopd_date' :fields.date('Tanggal SK/Bukti'),
        'realisasi_integritas_es1_date' :fields.date('Tanggal SK/Bukti'),
        'realisasi_integritas_es2_date' :fields.date('Tanggal SK/Bukti'),
        
        'realisasi_kerjasama_nasional_date' :fields.date('Tanggal SK/Bukti'),
        'realisasi_kerjasama_gubernur_date' :fields.date('Tanggal SK/Bukti'),
        'realisasi_kerjasama_kepalaopd_date' :fields.date('Tanggal SK/Bukti'),
        'realisasi_kerjasama_lainlain_date' :fields.date('Tanggal SK/Bukti'),
        'realisasi_kerjasama_rapat_provinsi_date' :fields.date('Tanggal SK/Bukti'),
        'realisasi_kerjasama_rapat_nasional_date' :fields.date('Tanggal SK/Bukti'),
        
        'realisasi_kepemimpinan_nasional_date' :fields.date('Tanggal SK/Bukti'),
        'realisasi_kepemimpinan_gubernur_date' :fields.date('Tanggal SK/Bukti'),
        'realisasi_kepemimpinan_kepalaopd_date' :fields.date('Tanggal SK/Bukti'),
        'realisasi_kepemimpinan_lainlain_date' :fields.date('Tanggal SK/Bukti'),
        'realisasi_kepemimpinan_narsum_unitkerja_date' :fields.date('Tanggal SK/Bukti'),
        'realisasi_kepemimpinan_narsum_provinsi_date' :fields.date('Tanggal SK/Bukti'),
        'realisasi_kepemimpinan_narsum_nasional_date' :fields.date('Tanggal SK/Bukti'),
    }
   # _defaults = {
   #    
    #}
    _order = "target_period_year,target_period_month"
    
    #VALIDATOR
    def get_auth_id(self, cr, uid, ids, type, context=None):
        if not isinstance(ids, list): ids = [ids]
        for task in self.browse(cr, uid, ids, context=context):
            
            if type == 'user_id' :
                if task.user_id :
                    if task.user_id.id != uid :
                        raise osv.except_osv(_('Invalid Action!'),
                                             _('Anda Tidak Memiliki Priviledge Untuk Proses Ini.'))
            elif type == 'user_id_atasan' :
                if task.user_id_atasan :
                    if task.user_id_atasan.id != uid :
                        raise osv.except_osv(_('Invalid Action!'),
                                             _('Anda Tidak Memiliki Priviledge Untuk Proses Ini.'))
            elif type == 'user_id_banding' :
                if task.user_id_banding :
                    if task.user_id_banding.id != uid :
                        raise osv.except_osv(_('Invalid Action!'),
                                             _('Anda Tidak Memiliki Priviledge Untuk Proses Ini.'))
            elif type == 'user_id_bkd' :
                if task.user_id_bkd :
                    if task.user_id_bkd.id != uid :
                        raise osv.except_osv(_('Invalid Action!'),
                                             _('Anda Tidak Memiliki Priviledge Untuk Proses Ini.'))
                
            else : 
               return False;
            
        return True;
    def is_user(self,cr,uid,ids,context=None):
        task_id = len(ids) and ids[0] or False
        ret_val = False
        if not task_id: return False
        return self.get_auth_id(cr, uid, [task_id],'user_id', context=context) 
    def is_manager(self,cr,uid,ids,context=None):
        task_id = len(ids) and ids[0] or False
        ret_val = False
        if not task_id: return False
        return self.get_auth_id(cr, uid, [task_id],'user_id_atasan', context=context) 
    def is_appeal_manager(self,cr,uid,ids,context=None):
        task_id = len(ids) and ids[0] or False
        ret_val = False
        if not task_id: return False
        return self.get_auth_id(cr, uid, [task_id],'user_id_banding', context=context) 
    def is_verificator(self,cr,uid,ids,context=None):
        task_id = len(ids) and ids[0] or False
        ret_val = False
        if not task_id: return False
        return self.get_auth_id(cr, uid, [task_id],'user_id_bkd', context=context)
    def is_user_or_is_verificator(self,cr,uid,ids,context=None):
        task_id = len(ids) and ids[0] or False
        if not task_id: return False
        accepted = self.get_auth_id(cr, uid, [task_id],'user_id', context=context) or self.get_auth_id(cr, uid, [task_id],'user_id_bkd', context=context)
        return accepted
    
    # WORKFLOW    
    def action_target_done(self, cr, uid, ids, context=None):
        """ Khusus untuk kegiatan perilaku dan  tambahan & kreatifitas.
            Kondisi pegawai siap mengisikan realisasi
        """
        task_id = len(ids) and ids[0] or False
        ret_val = False
        if not task_id: return False
        if self.is_user(cr, uid, ids, context) :
                self.fill_target_perilaku(cr, uid, [task_id], context);
                self.fill_lookup_perilaku_komitmen(cr, uid, [task_id], context);
        ret_val = self.action_realisasi(cr, uid,ids, context=context)
                    
        return ret_val
    def fill_target_perilaku(self, cr, uid, ids, context=None):
        """ Lookup ke objek hari_kerja_bulanan untuk isi data perilaku kategori komitment"""
        if not isinstance(ids, list): ids = [ids]
        komitmen_pool = self.pool.get('hari.kerja.bulanan')
        for task_obj in self.browse(cr, uid, ids, context=context):
            vals = {}
            if task_obj:
                    komitmen_ids = komitmen_pool.search(cr, uid, [('target_period_year', '=', task_obj.target_period_year), ('target_period_month', '=', task_obj.target_period_month)], context=None)
                    komitmen_objs = komitmen_pool.browse(cr, uid, komitmen_ids, context=context)
                    komitmen_obj = komitmen_objs and komitmen_objs[0] or False
                    if komitmen_obj:
                        #print "komitmen_obj : ", komitmen_obj.jumlah_apel_pagi
                        vals = {
                                        'realisasi_apel_pagi'     : komitmen_obj.jumlah_apel_pagi or 0,
                                        'realisasi_jumlah_hari_kerja'     : komitmen_obj.jumlah_hari_kerja or 0,
                                        'realisasi_jumlah_jam_kerja'     : komitmen_obj.jumlah_jam_kerja or 0,
                                        'realisasi_upacara_hari_besar'     : komitmen_obj.jumlah_upacara_hari_besar or 0,
                                    }
                        self.write(cr, uid, [task_obj.id], vals, context)
                # end if               
                    
        # end for
        return True
    
    
    def set_draft(self, cr, uid, ids, context=None):
        return self.write(cr, uid, ids, {'state':'draft'}, context=context) 
    def set_realisasi(self, cr, uid, ids, context=None):
        return self.write(cr, uid, ids, {'state':'realisasi'}, context=context)
    def set_cancelled(self, cr, uid, ids, context=None):
        return self.write(cr, uid, ids, {'state':'cancelled','active':False}, context=context)
    def set_closed(self, cr, uid, ids, context=None):
        return self.write(cr, uid, ids, {'state':'closed'}, context=context)
    def set_propose(self, cr, uid, ids, context=None):
        return self.write(cr, uid, ids, {'state':'propose'}, context=context)
    def set_appeal(self, cr, uid, ids, context=None):
        return self.write(cr, uid, ids, {'state':'appeal'}, context=context)
    def set_evaluated(self, cr, uid, ids, context=None):
        for task_id in ids:
            self.write(cr, uid, task_id, {'state':'evaluated',
                                         'komitmen_verifikasi_notsame':self.is_status_komitmen_verifikasi_notsame(cr,uid,task_id,context)
                                         }, context=context)
        return True
    def set_done(self, cr, uid, ids, context=None):
        return self.write(cr, uid, ids, {'state':'done'}, context=context)
    def set_rejected_bkd(self, cr, uid, ids, context=None):
        return self.write(cr, uid, ids, {'state':'rejected_bkd'}, context=context)
    def action_realisasi(self, cr, uid, ids, context=None):
        """ Khusus untuk kegiatan perilaku dan  tambahan & kreatifitas.
            Kondisi pegawai siap mengisikan realisasi
        """
        if self.is_user(cr, uid, ids, context) :
            return self.set_realisasi(cr, uid, ids, context)
                    #self.do_target_done_notification(cr, uid, [task_id], context=context)
        return False
    def action_cancel_realisasi(self, cr, uid, ids, context=None):
        """ Batalkan Transaksi 
        """
        if self.is_user(cr, uid, ids, context) :
            return self.set_cancelled(cr, uid, ids, context=context)
    def action_propose(self, cr, uid, ids, context=None):
        """ Selesai mengerjakan input realisasi, 
        """
        if self.is_user(cr, uid, ids, context) :
            return self.set_propose(cr, uid, ids, context=context)
        return False
    def action_propose_rejected_perilaku_popup(self, cr, uid, ids, context=None):
        if not ids: return []
        if self.is_manager(cr, uid, ids, context):
            task_obj = self.browse(cr, uid, ids[0], context=context)
            dummy, view_id = self.pool.get('ir.model.data').get_object_reference(cr, uid, 'df_project', 'action_perilaku_propose_rejected_popup_form_view')
            return {
                    'name':_("Pengajuan Perilaku Ditolak"),
                    'view_mode': 'form',
                    'view_id': view_id,
                    'view_type': 'form',
                    'res_model': 'project.perilaku.propose.rejected',
                    'type': 'ir.actions.act_window',
                    'nodestroy': True,
                    'target': 'new',
                    'domain': '[]',
                    'context': {
                                'default_jumlah_konsumen_pelayanan'       : task_obj.realisasi_jumlah_konsumen_pelayanan,
                                'default_satuan_jumlah_konsumen_pelayanan': task_obj.realisasi_satuan_jumlah_konsumen_pelayanan.id or None,
                                'default_jumlah_tidakpuas_pelayanan'      : task_obj.realisasi_jumlah_tidakpuas_pelayanan,
                                'default_ketepatan_laporan_spj'           : task_obj.realisasi_ketepatan_laporan_spj.id or None,
                                'default_ketepatan_laporan_ukp4'          : task_obj.realisasi_ketepatan_laporan_ukp4.id or None,
                                'default_efisiensi_biaya_operasional'     : task_obj.realisasi_efisiensi_biaya_operasional.id or None,
                                
                                'default_hadir_upacara_hari_besar': task_obj.realisasi_hadir_upacara_hari_besar,
                                'default_hadir_hari_kerja'     : task_obj.realisasi_hadir_hari_kerja,
                                'default_hadir_jam_kerja': task_obj.realisasi_hadir_jam_kerja,
                                'default_hadir_apel_pagi': task_obj.realisasi_hadir_apel_pagi,
                                
                                'default_integritas_presiden': task_obj.realisasi_integritas_presiden,
                                'default_integritas_gubernur': task_obj.realisasi_integritas_gubernur,
                                'default_integritas_kepalaopd': task_obj.realisasi_integritas_kepalaopd,
                                'default_integritas_atasan': task_obj.realisasi_integritas_atasan,
                                'default_integritas_es1': task_obj.realisasi_integritas_es1,
                                'default_integritas_es2': task_obj.realisasi_integritas_es2,
                                'default_integritas_es3': task_obj.realisasi_integritas_es3,
                                'default_integritas_es4': task_obj.realisasi_integritas_es4,
                                
                                'default_integritas_hukuman': task_obj.realisasi_integritas_hukuman,
                                'default_integritas_hukuman_ringan': task_obj.realisasi_integritas_hukuman_ringan,
                                'default_integritas_hukuman_sedang': task_obj.realisasi_integritas_hukuman_sedang,
                                'default_integritas_hukuman_berat': task_obj.realisasi_integritas_hukuman_berat,
                                
                                'default_kerjasama_nasional': task_obj.realisasi_kerjasama_nasional,
                                'default_kerjasama_gubernur': task_obj.realisasi_kerjasama_gubernur,
                                'default_kerjasama_kepalaopd': task_obj.realisasi_kerjasama_kepalaopd,
                                'default_kerjasama_atasan':task_obj.realisasi_kerjasama_atasan,
                                'default_kerjasama_rapat_nasional': task_obj.realisasi_kerjasama_rapat_nasional,
                                'default_kerjasama_rapat_provinsi': task_obj.realisasi_kerjasama_rapat_provinsi,
                                'default_kerjasama_rapat_perangkat_daerah': task_obj.realisasi_kerjasama_rapat_perangkat_daerah,
                                'default_kerjasama_rapat_atasan': task_obj.realisasi_kerjasama_rapat_atasan,
                                
                                'default_kepemimpinan_nasional': task_obj.realisasi_kepemimpinan_nasional,
                                'default_kepemimpinan_gubernur': task_obj.realisasi_kepemimpinan_gubernur,
                                'default_kepemimpinan_kepalaopd': task_obj.realisasi_kepemimpinan_kepalaopd,
                                'default_kepemimpinan_atasan':  task_obj.realisasi_kepemimpinan_atasan,
                                'default_kepemimpinan_narsum_nasional': task_obj.realisasi_kepemimpinan_narsum_nasional,
                                'default_kepemimpinan_narsum_provinsi': task_obj.realisasi_kepemimpinan_narsum_provinsi,
                                'default_kepemimpinan_narsum_perangkat_daerah': task_obj.realisasi_kepemimpinan_narsum_perangkat_daerah,
                                'default_kepemimpinan_narsum_unitkerja': task_obj.realisasi_kepemimpinan_narsum_unitkerja,
                                
                                    'default_is_kepala_opd': task_obj.is_kepala_opd,
                                    'default_employee_job_type': task_obj.employee_job_type,
                                    'default_is_suggest': True,
                                    'default_task_id':task_obj.id
                    }
                }
            
        return False
    
    
    def action_appeal(self, cr, uid, ids, context=None):
        """ Penolakan di Banding ke Atasan 
        """
        if self.is_user(cr, uid, ids, context) :
            self.fill_task_appeal_automatically_with_suggest(cr, uid, ids, context);
            return self.set_appeal(cr, uid, ids, context=context)
    def action_dont_appeal(self, cr, uid, ids, context=None):
        """ Penolakan Diterima, Langsung ajukan Verifikasi,
        """
        if self.is_user(cr, uid, ids, context) :
            self.fill_task_automatically_with_suggest(cr, uid, ids, context);
            return self.set_evaluated(cr, uid, ids, context=context)
    def action_appeal_approve(self, cr, uid, ids, context=None):
        """ Banding DIterima, Langsung ajukan Verifikasi,
        """
        if self.is_appeal_manager(cr, uid, ids, context) : 
            return self.set_evaluated(cr, uid, ids, context=context)
    def action_appeal_rejected_perilaku_popup(self, cr, uid, ids, context=None):
        if not ids: return []
        if self.is_appeal_manager(cr, uid, ids, context):
            task_obj = self.browse(cr, uid, ids[0], context=context)
            dummy, view_id = self.pool.get('ir.model.data').get_object_reference(cr, uid, 'df_project', 'action_perilaku_appeal_rejected_popup_form_view')
            return {
                    'name':_("Pengajuan Banding Perilaku Ditolak"),
                    'view_mode': 'form',
                    'view_id': view_id,
                    'view_type': 'form',
                    'res_model': 'project.perilaku.appeal.rejected',
                    'type': 'ir.actions.act_window',
                    'nodestroy': True,
                    'target': 'new',
                    'domain': '[]',
                    'context': {
                                'default_jumlah_konsumen_pelayanan'       : task_obj.suggest_jumlah_konsumen_pelayanan,
                                'default_satuan_jumlah_konsumen_pelayanan': task_obj.suggest_satuan_jumlah_konsumen_pelayanan.id or None,
                                'default_jumlah_tidakpuas_pelayanan'      : task_obj.suggest_jumlah_tidakpuas_pelayanan,
                                'default_ketepatan_laporan_spj'           : task_obj.suggest_ketepatan_laporan_spj.id or None,
                                'default_ketepatan_laporan_ukp4'          : task_obj.suggest_ketepatan_laporan_ukp4.id or None,
                                'default_efisiensi_biaya_operasional'     : task_obj.suggest_efisiensi_biaya_operasional.id or None,
                                
                                'default_hadir_upacara_hari_besar': task_obj.suggest_hadir_upacara_hari_besar,
                                'default_hadir_hari_kerja'     : task_obj.suggest_hadir_hari_kerja,
                                'default_hadir_jam_kerja': task_obj.suggest_hadir_jam_kerja,
                                'default_hadir_apel_pagi': task_obj.suggest_hadir_apel_pagi,
                                
                                'default_integritas_presiden': task_obj.suggest_integritas_presiden,
                                'default_integritas_gubernur': task_obj.suggest_integritas_gubernur,
                                'default_integritas_kepalaopd': task_obj.suggest_integritas_kepalaopd,
                                'default_integritas_atasan': task_obj.suggest_integritas_atasan,
                                'default_integritas_es1': task_obj.suggest_integritas_es1,
                                'default_integritas_es2': task_obj.suggest_integritas_es2,
                                'default_integritas_es3': task_obj.suggest_integritas_es3,
                                'default_integritas_es4': task_obj.suggest_integritas_es4,
                                
                                'default_integritas_hukuman': task_obj.suggest_integritas_hukuman,
                                'default_integritas_hukuman_ringan': task_obj.suggest_integritas_hukuman_ringan,
                                'default_integritas_hukuman_sedang': task_obj.suggest_integritas_hukuman_sedang,
                                'default_integritas_hukuman_berat': task_obj.suggest_integritas_hukuman_berat,
                                
                                'default_kerjasama_nasional': task_obj.suggest_kerjasama_nasional,
                                'default_kerjasama_gubernur': task_obj.suggest_kerjasama_gubernur,
                                'default_kerjasama_kepalaopd': task_obj.suggest_kerjasama_kepalaopd,
                                'default_kerjasama_atasan':task_obj.suggest_kerjasama_atasan,
                                'default_kerjasama_rapat_nasional': task_obj.suggest_kerjasama_rapat_nasional,
                                'default_kerjasama_rapat_provinsi': task_obj.suggest_kerjasama_rapat_provinsi,
                                'default_kerjasama_rapat_perangkat_daerah': task_obj.suggest_kerjasama_rapat_perangkat_daerah,
                                'default_kerjasama_rapat_atasan': task_obj.suggest_kerjasama_rapat_atasan,
                                
                                'default_kepemimpinan_nasional': task_obj.suggest_kepemimpinan_nasional,
                                'default_kepemimpinan_gubernur': task_obj.suggest_kepemimpinan_gubernur,
                                'default_kepemimpinan_kepalaopd': task_obj.suggest_kepemimpinan_kepalaopd,
                                'default_kepemimpinan_atasan':  task_obj.suggest_kepemimpinan_atasan,
                                'default_kepemimpinan_narsum_nasional': task_obj.suggest_kepemimpinan_narsum_nasional,
                                'default_kepemimpinan_narsum_provinsi': task_obj.suggest_kepemimpinan_narsum_provinsi,
                                'default_kepemimpinan_narsum_perangkat_daerah': task_obj.suggest_kepemimpinan_narsum_perangkat_daerah,
                                'default_kepemimpinan_narsum_unitkerja': task_obj.suggest_kepemimpinan_narsum_unitkerja,
                                
                                    'default_is_kepala_opd': task_obj.is_kepala_opd,
                                    'default_employee_job_type': task_obj.employee_job_type,
                                    'default_is_appeal': True,
                                    'default_task_id':task_obj.id
                    }
                }
            
        return False
    
    
    def action_evaluated(self, cr, uid, ids, context=None):
        """ Ajukan Verifikasi
        """
        if self.is_manager(cr, uid, ids, context) :
            return self.set_evaluated(cr, uid, ids, context=context)
    def action_done(self, cr, uid, ids, context=None):
        """ Selesai Perhitungan
        """
        for task_id in ids:
            if self.is_verificator(cr, uid, [task_id], context) :
                self.do_task_poin_calculation(cr, uid, [task_id], context=context)
                self.set_done(cr, uid, [task_id], context=context)
        return True
    def action_work_rejected(self, cr, uid, ids, context=None):
        """ Verifikasi Ditolak
        """
        
        if not ids: return []
        
        if self.is_verificator(cr, uid, ids, context) :
            dummy, view_id = self.pool.get('ir.model.data').get_object_reference(cr, uid, 'df_project', 'action_perilaku_verificate_rejected_popup_form_view')
            task_obj = self.browse(cr, uid, ids[0], context=context)
            if task_obj.control_count >= 2 :
                raise osv.except_osv(_('Invalid Action, Limit Action'),
                                         _('Hasil Verifikasi Tidak Dapat Ditolak, Karena Sudah Dilakukan Pengajuan Dan Verifikasi Sebanyak 2 Kali.'))
            return {
                'name':_("Verifikasi Ditolak"),
                'view_mode': 'form',
                'view_id': view_id,
                'view_type': 'form',
                'res_model': 'project.perilaku.verificate.rejected',
                'type': 'ir.actions.act_window',
                'nodestroy': True,
                'target': 'new',
                'domain': '[]',
                'context': {
                    'default_control_count':task_obj.control_count + 1,
                    'default_is_control': True,
                    'default_task_id':task_obj.id
    
                }
            }
        return False
    def action_done_use_target(self, cr, uid, ids, context=None):
        """ Selesai Perhitungan
        """
        use_target = {'use_target_for_calculation': True,}
        for task_id in ids:
            if self.is_verificator(cr, uid, [task_id], context) :
                self.write(cr, uid, [task_id], use_target, context=context)
                self.do_task_poin_calculation(cr, uid, [task_id], context=context)
                self.set_done(cr, uid, [task_id], context=context)
        return True
    def do_recalculate_poin(self, cr, uid, ids, context=None):
         if self.is_verificator(cr, uid, ids, context) :
            
            update_poin = {
                                        'nilai_akhir': 0,
                                        'indeks_nilai': False,
                                        'jumlah_perhitungan':0,
                                        'nilai_pelayanan': 0,
                                        'nilai_integritas': 0,
                                        'nilai_komitmen':0,
                                        'nilai_disiplin': 0,
                                        'nilai_kerjasama': 0,
                                        'nilai_kepemimpinan':0,
                                        'use_target_for_calculation': False,
                                         }
            self.write(cr, uid, ids, update_poin)
            return self.set_evaluated(cr, uid, ids, context=context)
                 
         return True;
    def action_verificate_revision(self, cr, uid, ids, context=None):
        """ Selesai Perhitungan
        """
        if self.is_manager(cr, uid, ids, context) :
            self.fill_task_automatically_with_suggest(cr, uid, ids, context);
            return self.set_evaluated(cr, uid, ids, context=context)
    #Calculation
    def do_task_poin_calculation(self, cr, uid, ids, context=None):
        """ BKD->Done (Keuangan) """
        for task_id in ids:
            update_poin = self.prepare_task_poin_calculation(cr, uid, [task_id], context=context)
            if update_poin:
                self.write(cr, uid, [task_id], update_poin,context)
        return True
    def do_task_poin_calculation_temporary(self, cr, uid, ids, context=None):
        for task_id in ids :
            update_poin = self.prepare_task_poin_calculation(cr, uid, [task_id], context=context)
            if update_poin:
                nilai_sementara = update_poin.get('nilai_akhir',0)
                self.write(cr, uid, [task_id], {
                                    'nilai_sementara': nilai_sementara}
                           ,context)
        return True
    
    def prepare_task_poin_calculation(self, cr, uid, ids, context=None):
        """ BKD->Done (Keuangan) """
        #print "Poin Calculation"
        target_pool = self.pool.get('project.project')
        lookup_nilai_pool = self.pool.get('acuan.penilaian')
        if not isinstance(ids, list): ids = [ids]
        for task_obj in self.browse(cr, uid, ids, context=context):
            vals = {}
            
            # close task...
            nilai_orientasi_pelayanan = 0
            nilai_integritas = 0
            nilai_komitment = 0
            nilai_disiplin = 0
            nilai_kerjasama = 0
            nilai_kepemimpinan = 0
            nilai_akhir = 0
            jumlah_perhitungan = 0
            indeks_nilai = 'a'
            
            employee = task_obj.user_id and task_obj.user_id.partner_id
            
            
            if not employee :
                 raise osv.except_osv(_('Invalid Action, Data Pegawai Tidak Lengkap'),
                                _('Proses Tidak Dapat Dilanjutkan Karena User Login Belum Dikaitkan Dengan Data Pegawai.'))
            else :
                job_type = employee.job_type
                if not job_type :
                     raise osv.except_osv(_('Invalid Action, Data Pegawai Tidak Lengkap'),
                                _('Jenis Jabatan Pegawai Belum Diisi, Harap Dilengkapi Terlebih Dahulu Di Data Pegawai, Atau Data Jabatan.'))
            if task_obj:
                if task_obj.target_period_month:
                     # if not task_obj.realisasi_integritas_atasan :
                     #    raise osv.except_osv(_('Invalid Process,'),
                     #               _('Proses Penilaian Kinerja Pegawai Tidak Bisa Dilanjutkan, Untuk Kategori Integritas Pegawai Minimal Mendapat Penghargaan Dari Atasan Langsung.'))
                     # if not task_obj.realisasi_kerjasama_atasan :
                     #    raise osv.except_osv(_('Invalid Process,'),
                     #               _('Proses Penilaian Kinerja Pegawai Tidak Bisa Dilanjutkan, Untuk Kategori Kerjasama Pegawai Minimal Melakukan Kerjasama Dengan Atasan Langsung.'))
                     # if not task_obj.realisasi_kepemimpinan_atasan :
                     #    raise osv.except_osv(_('Invalid Process,'),
                     #               _('Proses Penilaian Kinerja Pegawai Tidak Bisa Dilanjutkan, Untuk Kategori Kepemimpinan Pegawai Minimal Mendapat Penghargaan Kepemimpinan Dari Atasan Langsung.'))
                     
                    #ORIENTASI PELAYANAN
                     if task_obj.realisasi_jumlah_konsumen_pelayanan > 0 :
                        x = float (task_obj.realisasi_jumlah_konsumen_pelayanan)
                        y = float (task_obj.realisasi_jumlah_tidakpuas_pelayanan)
                        a = ((x - y) / x) * 100
                        #PENILAIAN TAMBAHAN JIKA KEPALA BALAI
                        if task_obj.is_kepala_opd :
                            a_efisiensi=a_ukp4=a_spj=0
                            lookup_nilai_id = lookup_nilai_pool.search(cr, uid, [('kategori_nilai', '=', 'threshold'), ('active', '=', True), ('type', '=', 'orientasi')
                                                                                 ,('nilai_bawah', '<=', task_obj.realisasi_jumlah_konsumen_pelayanan ), ('nilai_atas', '>=', task_obj.realisasi_jumlah_konsumen_pelayanan)], context=None)
                            if lookup_nilai_id : 
                                lookup_nilai = lookup_nilai_pool.browse(cr, uid, lookup_nilai_id, context=None)[0]
                                a =  lookup_nilai.nilai_tunggal
                            if task_obj.realisasi_ketepatan_laporan_spj:
                                    a_spj=task_obj.realisasi_ketepatan_laporan_spj.nilai_tunggal
                            if task_obj.realisasi_ketepatan_laporan_ukp4:
                                    a_ukp4=task_obj.realisasi_ketepatan_laporan_ukp4.nilai_tunggal
                            if task_obj.realisasi_efisiensi_biaya_operasional:
                                    a_efisiensi=task_obj.realisasi_efisiensi_biaya_operasional.nilai_tunggal
                                
                            nilai_orientasi_pelayanan = a + a_efisiensi+a_ukp4+a_spj
                        else :
                            nilai_orientasi_pelayanan = a
                     
                     #print "Total Nilai nilai_rientasi_pelayanan : ", nilai_orientasi_pelayanan
                     if nilai_orientasi_pelayanan > 100 :
                         nilai_orientasi_pelayanan = 100;
                     # Komitment : =(H66/G66)*100
                     nilai_apel_pagi = nilai_upacara_hari_besar = 0
                     
                     if task_obj.realisasi_apel_pagi==0 or task_obj.realisasi_jumlah_jam_kerja==0 or task_obj.realisasi_jumlah_hari_kerja==0   :
                         raise osv.except_osv(_('Invalid Action, Data Inisiasi Tidak Lengkap'),
                                    _('Proses Penilaian Data Tidak Bisa Dilanjutkan, Silakan Klik Tombol Lookup Data Inisiasi. Jika Masih 0 Silakan Isi Di Menu Inisiasi Hari Kerja'))
                         
                     if task_obj.realisasi_apel_pagi > 0 :
                        x = float (task_obj.realisasi_hadir_apel_pagi)
                        y = float (task_obj.realisasi_apel_pagi)
                        a = (x / y) * 100
                        
                        nilai_apel_pagi = a
                        nilai_upacara_hari_besar = task_obj.realisasi_hadir_upacara_hari_besar
                        
                        if nilai_apel_pagi < 50.0 and  ( nilai_upacara_hari_besar == 0 and task_obj.realisasi_upacara_hari_besar != 0) :
                            nilai_komitment = (nilai_apel_pagi - 10) + nilai_upacara_hari_besar
                        else :
                            nilai_komitment = nilai_apel_pagi #+ nilai_upacara_hari_besar
                        #print "Nilai Komitment| Apel  : ", nilai_apel_pagi , "| Upacara :", nilai_upacara_hari_besar, " = ", nilai_komitment
                     if nilai_komitment > 100 :
                         nilai_komitment = 100;
                     # Disiplin : =(H68/G68)G68*100
                     nilai_hari_kerja = nilai_hadir_jam_kerja = 0
                     if task_obj.realisasi_hadir_hari_kerja > 0 and task_obj.realisasi_hadir_jam_kerja > 0 :
                          x = float (task_obj.realisasi_hadir_hari_kerja)
                          y = float (task_obj.realisasi_jumlah_hari_kerja)
                          a = (x / y) * 100
                          nilai_hari_kerja = a
                          x = float (task_obj.realisasi_hadir_jam_kerja)
                          y = float (task_obj.realisasi_jumlah_jam_kerja)
                          a = (x / y) * 100
                          nilai_hadir_jam_kerja = a
                          
                          nilai_disiplin = (nilai_hari_kerja + nilai_hadir_jam_kerja) / 2
                      #    print 'nilai_hari_kerja ', nilai_hari_kerja, ' dan jam kerja ', nilai_hadir_jam_kerja, " = ", nilai_disiplin
                     if nilai_disiplin > 100 :
                         nilai_disiplin = 100;
                     #INTEGRITAS!!!!!!!!!!!!!!!!!!!!!
                     integritas = 0
                     nilai_integritas_lainlain = nilai_integritas_atasan = nilai_integritas_kepalaopd = nilai_integritas_gubernur = nilai_integritas_presiden = 0
                     nilai_hukuman_disiplin=nilai_hukuman_ringan=nilai_hukuman_sedang=nilai_hukuman_berat=0
                     if task_obj.realisasi_integritas_atasan :
                         lookup_nilai_id = lookup_nilai_pool.search(cr, uid, [('kategori_integritas', '=', 'atasan'), ('active', '=', True), ('type', '=', 'integritas')], context=None)
                         lookup_nilai = lookup_nilai_pool.browse(cr, uid, lookup_nilai_id, context=None)[0]
                         nilai_integritas = lookup_nilai.nilai_tunggal
                         integritas += 1
                     if task_obj.realisasi_integritas_presiden :
                        lookup_nilai_id = lookup_nilai_pool.search(cr, uid, [('kategori_integritas', '=', 'presiden'), ('active', '=', True), ('type', '=', 'integritas')], context=None)
                        lookup_nilai = lookup_nilai_pool.browse(cr, uid, lookup_nilai_id, context=None)[0]
                        if task_obj.realisasi_integritas_atasan :
                            nilai_integritas = nilai_integritas + lookup_nilai.nilai_tambahan
                        else :
                            nilai_integritas = lookup_nilai.nilai_tunggal  
                        integritas += 1
                     if task_obj.realisasi_integritas_gubernur :
                         lookup_nilai_id = lookup_nilai_pool.search(cr, uid, [('kategori_integritas', '=', 'gubernur'), ('active', '=', True), ('type', '=', 'integritas')], context=None)
                         lookup_nilai = lookup_nilai_pool.browse(cr, uid, lookup_nilai_id, context=None)[0]
                         if integritas != 0 :
                            nilai_integritas = nilai_integritas + lookup_nilai.nilai_tambahan
                         else :
                            nilai_integritas = lookup_nilai.nilai_tunggal  
                         integritas += 1
                     if task_obj.realisasi_integritas_kepalaopd :
                         lookup_nilai_id = lookup_nilai_pool.search(cr, uid, [('kategori_integritas', '=', 'kepalaopd'), ('active', '=', True), ('type', '=', 'integritas')], context=None)
                         lookup_nilai = lookup_nilai_pool.browse(cr, uid, lookup_nilai_id, context=None)[0]
                         if integritas != 0 :
                            nilai_integritas = nilai_integritas + lookup_nilai.nilai_tambahan
                         else :
                            nilai_integritas = lookup_nilai.nilai_tunggal  
                         integritas += 1
                     # if task_obj.realisasi_integritas_lainlain :
                     if task_obj.realisasi_integritas_es4 :
                             lookup_nilai_id = lookup_nilai_pool.search(cr, uid, [('kategori_integritas', '=', 'pejabat_es4'), ('active', '=', True), ('type', '=', 'integritas')], context=None)
                             lookup_nilai = lookup_nilai_pool.browse(cr, uid, lookup_nilai_id, context=None)[0]
                             nilai_integritas = nilai_integritas + lookup_nilai.nilai_tambahan
                             integritas += 1
                     if task_obj.realisasi_integritas_es3 :
                             lookup_nilai_id = lookup_nilai_pool.search(cr, uid, [('kategori_integritas', '=', 'pejabat_es3'), ('active', '=', True), ('type', '=', 'integritas')], context=None)
                             lookup_nilai = lookup_nilai_pool.browse(cr, uid, lookup_nilai_id, context=None)[0]
                             nilai_integritas = nilai_integritas + lookup_nilai.nilai_tambahan
                             integritas += 1
                     if task_obj.realisasi_integritas_es3 :
                             lookup_nilai_id = lookup_nilai_pool.search(cr, uid, [('kategori_integritas', '=', 'pejabat_es2'), ('active', '=', True), ('type', '=', 'integritas')], context=None)
                             lookup_nilai = lookup_nilai_pool.browse(cr, uid, lookup_nilai_id, context=None)[0]
                             nilai_integritas = nilai_integritas + lookup_nilai.nilai_tambahan
                             integritas += 1
                     if task_obj.realisasi_integritas_es1 :
                             lookup_nilai_id = lookup_nilai_pool.search(cr, uid, [('kategori_integritas', '=', 'pejabat_es1'), ('active', '=', True), ('type', '=', 'integritas')], context=None)
                             lookup_nilai = lookup_nilai_pool.browse(cr, uid, lookup_nilai_id, context=None)[0]
                             nilai_integritas = nilai_integritas + lookup_nilai.nilai_tambahan
                             integritas += 1
                     #HUKUMAN DISIPLIN
                     if task_obj.realisasi_integritas_hukuman and task_obj.realisasi_integritas_hukuman == 'tidak' :
                        lookup_nilai_id = lookup_nilai_pool.search(cr, uid, [('kategori_integritas', '=', 'tidak_hukuman'), ('active', '=', True), ('type', '=', 'integritas')], context=None)
                        lookup_nilai = lookup_nilai_pool.browse(cr, uid, lookup_nilai_id, context=None)[0]
                        nilai_hukuman_disiplin = lookup_nilai.nilai_tunggal
                     elif task_obj.realisasi_integritas_hukuman and task_obj.realisasi_integritas_hukuman  == 'ya':
                         if task_obj.realisasi_integritas_hukuman_berat :
                             lookup_nilai_id = lookup_nilai_pool.search(cr, uid, [('kategori_integritas', '=', 'hukuman_berat'), ('active', '=', True), ('type', '=', 'integritas')], context=None)
                             lookup_nilai = lookup_nilai_pool.browse(cr, uid, lookup_nilai_id, context=None)[0]
                             nilai_hukuman_berat = lookup_nilai.nilai_tunggal
                         if task_obj.realisasi_integritas_hukuman_sedang :
                             lookup_nilai_id = lookup_nilai_pool.search(cr, uid, [('kategori_integritas', '=', 'hukuman_sedang'), ('active', '=', True), ('type', '=', 'integritas')], context=None)
                             lookup_nilai = lookup_nilai_pool.browse(cr, uid, lookup_nilai_id, context=None)[0]
                             if nilai_hukuman_berat == 0 :
                                nilai_hukuman_sedang = lookup_nilai.nilai_tunggal
                         if task_obj.realisasi_integritas_hukuman_ringan :
                             lookup_nilai_id = lookup_nilai_pool.search(cr, uid, [('kategori_integritas', '=', 'hukuman_ringan'), ('active', '=', True), ('type', '=', 'integritas')], context=None)
                             lookup_nilai = lookup_nilai_pool.browse(cr, uid, lookup_nilai_id, context=None)[0]
                             if nilai_hukuman_berat == 0  and nilai_hukuman_sedang == 0 :
                                nilai_hukuman_ringan = lookup_nilai.nilai_tunggal
                         nilai_hukuman_disiplin = nilai_hukuman_ringan + nilai_hukuman_sedang + nilai_hukuman_berat
                         
                         
                     #print "Nilai Integritas : ", nilai_integritas, " Dan  ", nilai_hukuman_disiplin, " -"
                     if nilai_integritas > 100 :
                         nilai_integritas = 100;
                     #nilai_integritas = nilai_integritas/10
                     nilai_integritas = nilai_hukuman_disiplin + nilai_integritas
                     #print "Total Nilai Integritas : ", nilai_integritas
                     if nilai_integritas > 100 :
                         nilai_integritas = 100;
                     # Kerja Sama
                     kerjasama = 0
                     nilai_kerjasama_lainlain = nilai_kerjasama_atasan = nilai_kerjasama_kepalaopd = nilai_kerjasama_gubernur = nilai_kerjasama_presiden = 0
                     if task_obj.realisasi_kerjasama_atasan :
                         lookup_nilai_id = lookup_nilai_pool.search(cr, uid, [('kategori_kerjasama', '=', 'atasan'), ('active', '=', True), ('type', '=', 'kerjasama')], context=None)
                         lookup_nilai = lookup_nilai_pool.browse(cr, uid, lookup_nilai_id, context=None)[0]
                         nilai_kerjasama = lookup_nilai.nilai_tunggal
                         kerjasama += 1
                     if task_obj.realisasi_kerjasama_nasional :
                        lookup_nilai_id = lookup_nilai_pool.search(cr, uid, [('kategori_kerjasama', '=', 'nasional'), ('active', '=', True), ('type', '=', 'kerjasama')], context=None)
                        lookup_nilai = lookup_nilai_pool.browse(cr, uid, lookup_nilai_id, context=None)[0]
                        if task_obj.realisasi_kerjasama_atasan :
                            nilai_kerjasama = nilai_kerjasama + lookup_nilai.nilai_tambahan
                        else :
                            nilai_kerjasama = lookup_nilai.nilai_tunggal  
                        kerjasama += 1
                     if task_obj.realisasi_kerjasama_gubernur :
                         lookup_nilai_id = lookup_nilai_pool.search(cr, uid, [('kategori_kerjasama', '=', 'provinsi'), ('active', '=', True), ('type', '=', 'kerjasama')], context=None)
                         lookup_nilai = lookup_nilai_pool.browse(cr, uid, lookup_nilai_id, context=None)[0]
                         if kerjasama != 0 :
                            nilai_kerjasama = nilai_kerjasama + lookup_nilai.nilai_tambahan
                         else :
                            nilai_kerjasama = lookup_nilai.nilai_tunggal  
                         kerjasama += 1
                     if task_obj.realisasi_kerjasama_kepalaopd :
                         lookup_nilai_id = lookup_nilai_pool.search(cr, uid, [('kategori_kerjasama', '=', 'perangkat_daerah'), ('active', '=', True), ('type', '=', 'kerjasama')], context=None)
                         lookup_nilai = lookup_nilai_pool.browse(cr, uid, lookup_nilai_id, context=None)[0]
                         if kerjasama != 0 :
                            nilai_kerjasama = nilai_kerjasama + lookup_nilai.nilai_tambahan
                         else :
                            nilai_kerjasama = lookup_nilai.nilai_tunggal  
                         kerjasama += 1
                     
                     
                     if task_obj.realisasi_kerjasama_rapat_atasan :
                             lookup_nilai_id = lookup_nilai_pool.search(cr, uid, [('kategori_kerjasama', '=', 'rapat_atasan'), ('active', '=', True), ('type', '=', 'kerjasama')], context=None)
                             lookup_nilai = lookup_nilai_pool.browse(cr, uid, lookup_nilai_id, context=None)[0]
                             nilai_kerjasama = nilai_kerjasama + lookup_nilai.nilai_tambahan
                             kerjasama += 1
                     if task_obj.realisasi_kerjasama_rapat_perangkat_daerah :
                             lookup_nilai_id = lookup_nilai_pool.search(cr, uid, [('kategori_kerjasama', '=', 'rapat_perangkat_daerah'), ('active', '=', True), ('type', '=', 'kerjasama')], context=None)
                             lookup_nilai = lookup_nilai_pool.browse(cr, uid, lookup_nilai_id, context=None)[0]
                             nilai_kerjasama = nilai_kerjasama + lookup_nilai.nilai_tambahan
                             kerjasama += 1
                     if task_obj.realisasi_kerjasama_rapat_provinsi :
                             lookup_nilai_id = lookup_nilai_pool.search(cr, uid, [('kategori_kerjasama', '=', 'rapat_provinsi'), ('active', '=', True), ('type', '=', 'kerjasama')], context=None)
                             lookup_nilai = lookup_nilai_pool.browse(cr, uid, lookup_nilai_id, context=None)[0]
                             nilai_kerjasama = nilai_kerjasama + lookup_nilai.nilai_tambahan
                             kerjasama += 1
                     if task_obj.realisasi_kerjasama_rapat_nasional :
                             lookup_nilai_id = lookup_nilai_pool.search(cr, uid, [('kategori_kerjasama', '=', 'rapat_nasional'), ('active', '=', True), ('type', '=', 'kerjasama')], context=None)
                             lookup_nilai = lookup_nilai_pool.browse(cr, uid, lookup_nilai_id, context=None)[0]
                             nilai_kerjasama = nilai_kerjasama + lookup_nilai.nilai_tambahan
                             kerjasama += 1
                    
                     #print "Nilai kerjasama : ", nilai_kerjasama, " Dari ", kerjasama, " Aspek"
                     if nilai_kerjasama > 100 :
                         nilai_kerjasama = 100;
                     
                    # Kepemimpinan
                    
                     kepemimpinan = 0
                     nilai_kepemimpinan_lainlain = nilai_kepemimpinan_atasan = nilai_kepemimpinan_kepalaopd = nilai_kepemimpinan_gubernur = nilai_kepemimpinan_presiden = 0
                     if job_type in ('struktural') :
                         if task_obj.realisasi_kepemimpinan_atasan :
                             lookup_nilai_id = lookup_nilai_pool.search(cr, uid, [('kategori_kepemimpinan', '=', 'unitkerja'), ('active', '=', True), ('type', '=', 'kepemimpinan')], context=None)
                             lookup_nilai = lookup_nilai_pool.browse(cr, uid, lookup_nilai_id, context=None)[0]
                             nilai_kepemimpinan = lookup_nilai.nilai_tunggal
                             kepemimpinan += 1
                         if task_obj.realisasi_kepemimpinan_nasional :
                            lookup_nilai_id = lookup_nilai_pool.search(cr, uid, [('kategori_kepemimpinan', '=', 'nasional'), ('active', '=', True), ('type', '=', 'kepemimpinan')], context=None)
                            lookup_nilai = lookup_nilai_pool.browse(cr, uid, lookup_nilai_id, context=None)[0]
                            if task_obj.realisasi_kepemimpinan_atasan :
                                nilai_kepemimpinan = nilai_kepemimpinan + lookup_nilai.nilai_tambahan
                            else :
                                nilai_kepemimpinan = lookup_nilai.nilai_tunggal  
                            kepemimpinan += 1
                         if task_obj.realisasi_kepemimpinan_gubernur :
                             lookup_nilai_id = lookup_nilai_pool.search(cr, uid, [('kategori_kepemimpinan', '=', 'provinsi'), ('active', '=', True), ('type', '=', 'kepemimpinan')], context=None)
                             lookup_nilai = lookup_nilai_pool.browse(cr, uid, lookup_nilai_id, context=None)[0]
                             if kepemimpinan != 0 :
                                nilai_kepemimpinan = nilai_kepemimpinan + lookup_nilai.nilai_tambahan
                             else :
                                nilai_kepemimpinan = lookup_nilai.nilai_tunggal  
                             kepemimpinan += 1
                         if task_obj.realisasi_kepemimpinan_kepalaopd :
                             lookup_nilai_id = lookup_nilai_pool.search(cr, uid, [('kategori_kepemimpinan', '=', 'perangkat_daerah'), ('active', '=', True), ('type', '=', 'kepemimpinan')], context=None)
                             lookup_nilai = lookup_nilai_pool.browse(cr, uid, lookup_nilai_id, context=None)[0]
                             if kepemimpinan != 0 :
                                nilai_kepemimpinan = nilai_kepemimpinan + lookup_nilai.nilai_tambahan
                             else :
                                nilai_kepemimpinan = lookup_nilai.nilai_tunggal  
                             kepemimpinan += 1
                         # if task_obj.realisasi_kepemimpinan_lainlain :
                         if task_obj.realisasi_kepemimpinan_narsum_unitkerja :
                                 lookup_nilai_id = lookup_nilai_pool.search(cr, uid, [('kategori_kepemimpinan', '=', 'narsum_unitkerja'), ('active', '=', True), ('type', '=', 'kepemimpinan')], context=None)
                                 lookup_nilai = lookup_nilai_pool.browse(cr, uid, lookup_nilai_id, context=None)[0]
                                 nilai_kepemimpinan = nilai_kepemimpinan + lookup_nilai.nilai_tambahan
                                 kepemimpinan += 1
                         if task_obj.realisasi_kepemimpinan_narsum_perangkat_daerah :
                                 lookup_nilai_id = lookup_nilai_pool.search(cr, uid, [('kategori_kepemimpinan', '=', 'narsum_perangkat_daerah'), ('active', '=', True), ('type', '=', 'kepemimpinan')], context=None)
                                 lookup_nilai = lookup_nilai_pool.browse(cr, uid, lookup_nilai_id, context=None)[0]
                                 nilai_kepemimpinan = nilai_kepemimpinan + lookup_nilai.nilai_tambahan
                                 kepemimpinan += 1
                         if task_obj.realisasi_kepemimpinan_narsum_provinsi :
                                 lookup_nilai_id = lookup_nilai_pool.search(cr, uid, [('kategori_kepemimpinan', '=', 'narsum_provinsi'), ('active', '=', True), ('type', '=', 'kepemimpinan')], context=None)
                                 lookup_nilai = lookup_nilai_pool.browse(cr, uid, lookup_nilai_id, context=None)[0]
                                 nilai_kepemimpinan = nilai_kepemimpinan + lookup_nilai.nilai_tambahan
                                 kepemimpinan += 1
                         if task_obj.realisasi_kepemimpinan_narsum_nasional :
                                 lookup_nilai_id = lookup_nilai_pool.search(cr, uid, [('kategori_kepemimpinan', '=', 'narsum_nasional'), ('active', '=', True), ('type', '=', 'kepemimpinan')], context=None)
                                 lookup_nilai = lookup_nilai_pool.browse(cr, uid, lookup_nilai_id, context=None)[0]
                                 nilai_kepemimpinan = nilai_kepemimpinan + lookup_nilai.nilai_tambahan
                                 kepemimpinan += 1
                        
                         #print "Nilai kepemimpinan : ", nilai_kepemimpinan, " Dari ", kepemimpinan, " Aspek"
                         if nilai_kepemimpinan > 100 :
                             nilai_kepemimpinan = 100;
                     
                     #print "nilai_orientasi_pelayanan =", nilai_orientasi_pelayanan
                     #print "nilai_integritas =", nilai_integritas
                     #print "nilai_komitment=", nilai_komitment
                     #print "nilai_disiplin=", nilai_disiplin
                     #print "nilai_kerjasama=", nilai_kerjasama
                     #print "nilai_kepemimpinan=", nilai_kepemimpinan
                     jumlah_perhitungan = nilai_orientasi_pelayanan + nilai_integritas + nilai_komitment + nilai_disiplin + nilai_kerjasama + nilai_kepemimpinan
                     if job_type in ('struktural') :
                         nilai_akhir = jumlah_perhitungan / 6
                     else :
                         nilai_akhir = jumlah_perhitungan / 5
                     #print "Hasil : ", jumlah_perhitungan, " -> ", nilai_akhir
                # end if          
            update_poin = {
                                'nilai_akhir': nilai_akhir,
                                'indeks_nilai': self.get_value_poin(cr, uid, ids, nilai_akhir),
                                'jumlah_perhitungan':jumlah_perhitungan,
                                'nilai_pelayanan': nilai_orientasi_pelayanan,
                                'nilai_integritas': nilai_integritas,
                                'nilai_komitmen':nilai_komitment,
                                'nilai_disiplin': nilai_disiplin,
                                'nilai_kerjasama': nilai_kerjasama,
                                'nilai_kepemimpinan':nilai_kepemimpinan,
                                
                                 }
            return update_poin;
        return False
    def get_value_realisasi_or_target(self, target,realisasi, use_target_for_calculation):
        if use_target_for_calculation : 
           return target
        return realisasi
    def get_value_poin(self, cr, uid, ids, nilai_akhir, context=None):
            # Tingkat kepuasan sebanyak 91 s.d 100 persen, maka nilai perilaku orientasi pelayanan Sangat Baik, dengan nilai antara 91 s.d 100 poin sesuai prosentase kepuasan    
            # Tingkat kepuasan sebanyak 76 s.d 90 persen, maka nilai perilaku orientasi pelayanan Baik, 
            # Tingkat kepuasan sebanyak 61 s.d 75 persen, maka nilai perilaku orientasi pelayanan Cukup, dengan nilai antara 61 s.d 75 poin sesuai prosentase kepuasan       
            # Tingkat kepuasan sebanyak 51 s.d 60 persen, maka nilai perilaku orientasi pelayanan Kurang, dengan nilai antara 51 s.d 60 poin sesuai prosentase kepuasan            
            # Tingkat kepuasan sebanyak kurang dari 50 persen, maka nilai perilaku orientasi pelayanan Buruk, dengan nilai antara 0 s.d 50 poin sesuai prosentase kepuasan
            value = '-'
            if nilai_akhir >= 91.0 and nilai_akhir <= 100.0 :
                value = 'Sangat Baik'; 
            if nilai_akhir >= 76.0 and nilai_akhir <= 90.0 :
                value = 'Baik'; 
            if nilai_akhir >= 61.0 and nilai_akhir <= 75.0 :
                value = 'Cukup'; 
            if nilai_akhir >= 51.0 and nilai_akhir <= 60.0 :
                value = 'Kurang'; 
            if nilai_akhir <= 50.0 :
                value = 'Buruk'; 
            return value;
    #Automation
    def fill_lookup_perilaku_komitmen(self, cr, uid, ids, context=None):
        """ Lookup ke objek verifikasi_absen_pegawai  untuk isi data perilaku kategori komitmen"""
        if not isinstance(ids, list): ids = [ids]
        lookup_pool = self.pool.get('verifikasi.absen.pegawai')
        for task_obj in self.browse(cr, uid, ids, context=context):
            vals = {}
            if task_obj:
                    lookup_ids = lookup_pool.search(cr, uid, [('employee_id', '=', task_obj.employee_id.id), ('target_period_year', '=', task_obj.target_period_year), ('target_period_month', '=', task_obj.target_period_month)], context=None)
                    lookup_objs = lookup_pool.browse(cr, uid, lookup_ids, context=context)
                    lookup_obj = lookup_objs and lookup_objs[0] or False
                    if lookup_obj:
                        vals = {
                                        'lookup_apel_pagi'     : lookup_obj.jumlah_apel_pagi or 0,
                                        'lookup_jumlah_hari_kerja'     : lookup_obj.jumlah_hari_kerja or 0,
                                        'lookup_jumlah_jam_kerja'     : lookup_obj.jumlah_jam_kerja or 0,
                                        'lookup_upacara_hari_besar'     : lookup_obj.jumlah_upacara_hari_besar or 0,
                                    }
                        self.write(cr, uid, [task_obj.id], vals, context)
                # end if               
                    
        # end for
        return True
    def fill_lookup_inisiasi_perilaku_komitmen(self, cr, uid, ids, context=None):
        """ Lookup ke objek verifikasi_absen_pegawai  untuk isi data perilaku kategori komitmen"""
        if not isinstance(ids, list): ids = [ids]
        lookup_pool = self.pool.get('hari.kerja.bulanan')
        for task_obj in self.browse(cr, uid, ids, context=context):
                vals = {}
                if task_obj:
                    lookup_ids = lookup_pool.search(cr, uid, [ ('target_period_year', '=', task_obj.target_period_year), ('target_period_month', '=', task_obj.target_period_month)], context=None)
                    lookup_objs = lookup_pool.browse(cr, uid, lookup_ids, context=context)
                    lookup_obj = lookup_objs and lookup_objs[0] or False
                    if lookup_obj:
                        vals = {
                                        'realisasi_apel_pagi'     : lookup_obj.jumlah_apel_pagi or 0,
                                        'realisasi_jumlah_hari_kerja'     : lookup_obj.jumlah_hari_kerja or 0,
                                        'realisasi_jumlah_jam_kerja'     : lookup_obj.jumlah_jam_kerja or 0,
                                        'realisasi_upacara_hari_besar'     : lookup_obj.jumlah_upacara_hari_besar or 0,
                                    }
                        self.write(cr, uid, [task_obj.id], vals, context)
                # end if               
                    
        # end for
        return True
    def is_status_komitmen_verifikasi_notsame(self, cr, uid, task_id, context=None):
        if not task_id:
            return False
        isNotSame = False
        task_obj = self.browse(cr,uid,task_id,context) 
        if  task_obj :
                if task_obj.realisasi_hadir_apel_pagi < task_obj.realisasi_apel_pagi :
                    isNotSame = True
                if  task_obj.realisasi_hadir_hari_kerja < task_obj.realisasi_jumlah_hari_kerja :
                    isNotSame = True
                if  task_obj.realisasi_hadir_jam_kerja < task_obj.realisasi_jumlah_jam_kerja :
                    isNotSame = True
                if task_obj.realisasi_hadir_upacara_hari_besar < task_obj.realisasi_upacara_hari_besar :
                    isNotSame = True
            # print "target_jumlah_kuantitas_output_notsame : ",target_jumlah_kuantitas_output_notsame 
        return isNotSame 
    
    def fill_target_automatically_with_task(self, cr, uid, ids, context=None):
        """ Default Target di isi task"""
        if not isinstance(ids, list): ids = [ids]
        for task_obj in self.browse(cr, uid, ids, context=context):
                vals = {}
                
                vals.update({
                                'suggest_jumlah_konsumen_pelayanan'       : task_obj.realisasi_jumlah_konsumen_pelayanan,
                                'suggest_satuan_jumlah_konsumen_pelayanan': task_obj.realisasi_satuan_jumlah_konsumen_pelayanan.id or None,
                                'suggest_jumlah_tidakpuas_pelayanan'      : task_obj.realisasi_jumlah_tidakpuas_pelayanan,
                                'suggest_ketepatan_laporan_spj'           : task_obj.realisasi_ketepatan_laporan_spj.id or None,
                                'suggest_ketepatan_laporan_ukp4'          : task_obj.realisasi_ketepatan_laporan_ukp4.id or None,
                                'suggest_efisiensi_biaya_operasional'     : task_obj.realisasi_efisiensi_biaya_operasional.id or None,
                                
                                #'suggest_apel_pagi'     : task_obj.realisasi_apel_pagi,
                                #'suggest_upacara_hari_besar': task_obj.realisasi_upacara_hari_besar,
                                'suggest_hadir_upacara_hari_besar': task_obj.realisasi_hadir_upacara_hari_besar,
                                #'suggest_jumlah_hari_kerja'     : task_obj.realisasi_jumlah_hari_kerja,
                                #'suggest_jumlah_jam_kerja': task_obj.realisasi_jumlah_jam_kerja,
                                'suggest_hadir_hari_kerja'     : task_obj.realisasi_hadir_hari_kerja,
                                'suggest_hadir_jam_kerja': task_obj.realisasi_hadir_jam_kerja,
                                'suggest_hadir_apel_pagi': task_obj.realisasi_hadir_apel_pagi,
                                
                                'suggest_integritas_presiden': task_obj.realisasi_integritas_presiden,
                                'suggest_integritas_gubernur': task_obj.realisasi_integritas_gubernur,
                                'suggest_integritas_kepalaopd': task_obj.realisasi_integritas_kepalaopd,
                                'suggest_integritas_atasan': task_obj.realisasi_integritas_atasan,
                                'suggest_integritas_es1': task_obj.realisasi_integritas_es1,
                                'suggest_integritas_es2': task_obj.realisasi_integritas_es2,
                                'suggest_integritas_es3': task_obj.realisasi_integritas_es3,
                                'suggest_integritas_es4': task_obj.realisasi_integritas_es4,
                                
                                'suggest_integritas_hukuman': task_obj.realisasi_integritas_hukuman,
                                'suggest_integritas_hukuman_ringan': task_obj.realisasi_integritas_hukuman_ringan,
                                'suggest_integritas_hukuman_sedang': task_obj.realisasi_integritas_hukuman_sedang,
                                'suggest_integritas_hukuman_berat': task_obj.realisasi_integritas_hukuman_berat,
                                
                                'suggest_kerjasama_nasional': task_obj.realisasi_kerjasama_nasional,
                                'suggest_kerjasama_gubernur': task_obj.realisasi_kerjasama_gubernur,
                                'suggest_kerjasama_kepalaopd': task_obj.realisasi_kerjasama_kepalaopd,
                                'suggest_kerjasama_atasan':task_obj.realisasi_kerjasama_atasan,
                                'suggest_kerjasama_rapat_nasional': task_obj.realisasi_kerjasama_rapat_nasional,
                                'suggest_kerjasama_rapat_provinsi': task_obj.realisasi_kerjasama_rapat_provinsi,
                                'suggest_kerjasama_rapat_perangkat_daerah': task_obj.realisasi_kerjasama_rapat_perangkat_daerah,
                                'suggest_kerjasama_rapat_atasan': task_obj.realisasi_kerjasama_rapat_atasan,
                                
                                'suggest_kepemimpinan_nasional': task_obj.realisasi_kepemimpinan_nasional,
                                'suggest_kepemimpinan_gubernur': task_obj.realisasi_kepemimpinan_gubernur,
                                'suggest_kepemimpinan_kepalaopd': task_obj.realisasi_kepemimpinan_kepalaopd,
                                'suggest_kepemimpinan_atasan':  task_obj.realisasi_kepemimpinan_atasan,
                                'suggest_kepemimpinan_narsum_nasional': task_obj.realisasi_kepemimpinan_narsum_nasional,
                                'suggest_kepemimpinan_narsum_provinsi': task_obj.realisasi_kepemimpinan_narsum_provinsi,
                                'suggest_kepemimpinan_narsum_perangkat_daerah': task_obj.realisasi_kepemimpinan_narsum_perangkat_daerah,
                                'suggest_kepemimpinan_narsum_unitkerja': task_obj.realisasi_kepemimpinan_narsum_unitkerja,
                                })               
                # end if               
                self.write(cr, uid, [task_obj.id], vals, context)
        # end for
        return True
    def fill_task_automatically_with_appeal(self, cr, uid, ids, context=None):
        """ Default Target di isi task"""
        if not isinstance(ids, list): ids = [ids]
        for task_obj in self.browse(cr, uid, ids, context=context):
                vals = {}
                
                vals.update({
                                'realisasi_jumlah_konsumen_pelayanan'       : task_obj.appeal_jumlah_konsumen_pelayanan,
                                'realisasi_satuan_jumlah_konsumen_pelayanan': task_obj.appeal_satuan_jumlah_konsumen_pelayanan.id or None,
                                'realisasi_jumlah_tidakpuas_pelayanan'      : task_obj.appeal_jumlah_tidakpuas_pelayanan,
                                'realisasi_ketepatan_laporan_spj'           : task_obj.appeal_ketepatan_laporan_spj.id or None,
                                'realisasi_ketepatan_laporan_ukp4'          : task_obj.appeal_ketepatan_laporan_ukp4.id or None,
                                'realisasi_efisiensi_biaya_operasional'     : task_obj.appeal_efisiensi_biaya_operasional.id or None,
                                
                                #'realisasi_apel_pagi'     : task_obj.appeal_apel_pagi,
                                #'realisasi_upacara_hari_besar': task_obj.appeal_upacara_hari_besar,
                                'realisasi_hadir_upacara_hari_besar': task_obj.appeal_hadir_upacara_hari_besar,
                                #'realisasi_jumlah_hari_kerja'     : task_obj.appeal_jumlah_hari_kerja,
                                #'realisasi_jumlah_jam_kerja': task_obj.appeal_jumlah_jam_kerja,
                                'realisasi_hadir_hari_kerja'     : task_obj.appeal_hadir_hari_kerja,
                                'realisasi_hadir_jam_kerja': task_obj.appeal_hadir_jam_kerja,
                                'realisasi_hadir_apel_pagi': task_obj.appeal_hadir_apel_pagi,
                                
                                'realisasi_integritas_presiden': task_obj.appeal_integritas_presiden,
                                'realisasi_integritas_gubernur': task_obj.appeal_integritas_gubernur,
                                'realisasi_integritas_kepalaopd': task_obj.appeal_integritas_kepalaopd,
                                'realisasi_integritas_atasan': task_obj.appeal_integritas_atasan,
                                'realisasi_integritas_es1': task_obj.appeal_integritas_es1,
                                'realisasi_integritas_es2': task_obj.appeal_integritas_es2,
                                'realisasi_integritas_es3': task_obj.appeal_integritas_es3,
                                'realisasi_integritas_es4': task_obj.appeal_integritas_es4,
                                
                                'realisasi_integritas_hukuman': task_obj.appeal_integritas_hukuman,
                                'realisasi_integritas_hukuman_ringan': task_obj.appeal_integritas_hukuman_ringan,
                                'realisasi_integritas_hukuman_sedang': task_obj.appeal_integritas_hukuman_sedang,
                                'realisasi_integritas_hukuman_berat': task_obj.appeal_integritas_hukuman_berat,
                                
                                'realisasi_kerjasama_nasional': task_obj.appeal_kerjasama_nasional,
                                'realisasi_kerjasama_gubernur': task_obj.appeal_kerjasama_gubernur,
                                'realisasi_kerjasama_kepalaopd': task_obj.appeal_kerjasama_kepalaopd,
                                'realisasi_kerjasama_atasan':task_obj.appeal_kerjasama_atasan,
                                'realisasi_kerjasama_rapat_nasional': task_obj.appeal_kerjasama_rapat_nasional,
                                'realisasi_kerjasama_rapat_provinsi': task_obj.appeal_kerjasama_rapat_provinsi,
                                'realisasi_kerjasama_rapat_perangkat_daerah': task_obj.appeal_kerjasama_rapat_perangkat_daerah,
                                'realisasi_kerjasama_rapat_atasan': task_obj.appeal_kerjasama_rapat_atasan,
                                
                                'realisasi_kepemimpinan_nasional': task_obj.appeal_kepemimpinan_nasional,
                                'realisasi_kepemimpinan_gubernur': task_obj.appeal_kepemimpinan_gubernur,
                                'realisasi_kepemimpinan_kepalaopd': task_obj.appeal_kepemimpinan_kepalaopd,
                                'realisasi_kepemimpinan_atasan':  task_obj.appeal_kepemimpinan_atasan,
                                'realisasi_kepemimpinan_narsum_nasional': task_obj.appeal_kepemimpinan_narsum_nasional,
                                'realisasi_kepemimpinan_narsum_provinsi': task_obj.appeal_kepemimpinan_narsum_provinsi,
                                'realisasi_kepemimpinan_narsum_perangkat_daerah': task_obj.appeal_kepemimpinan_narsum_perangkat_daerah,
                                'realisasi_kepemimpinan_narsum_unitkerja': task_obj.appeal_kepemimpinan_narsum_unitkerja,
                                })               
                # end if               
                self.write(cr, uid, [task_obj.id], vals, context)
        # end for
        return True
    def fill_task_automatically_with_suggest(self, cr, uid, ids, context=None):
        """ Default Target di isi task"""
        if not isinstance(ids, list): ids = [ids]
        for task_obj in self.browse(cr, uid, ids, context=context):
                vals = {}
                
                vals.update({
                                'realisasi_jumlah_konsumen_pelayanan'       : task_obj.suggest_jumlah_konsumen_pelayanan,
                                'realisasi_satuan_jumlah_konsumen_pelayanan': task_obj.suggest_satuan_jumlah_konsumen_pelayanan.id or None,
                                'realisasi_jumlah_tidakpuas_pelayanan'      : task_obj.suggest_jumlah_tidakpuas_pelayanan,
                                'realisasi_ketepatan_laporan_spj'           : task_obj.suggest_ketepatan_laporan_spj.id or None,
                                'realisasi_ketepatan_laporan_ukp4'          : task_obj.suggest_ketepatan_laporan_ukp4.id or None,
                                'realisasi_efisiensi_biaya_operasional'     : task_obj.suggest_efisiensi_biaya_operasional.id or None,
                                
                                #'realisasi_apel_pagi'     : task_obj.suggest_apel_pagi,
                                #'realisasi_upacara_hari_besar': task_obj.suggest_upacara_hari_besar,
                                'realisasi_hadir_upacara_hari_besar': task_obj.suggest_hadir_upacara_hari_besar,
                                #'realisasi_jumlah_hari_kerja'     : task_obj.suggest_jumlah_hari_kerja,
                                #'realisasi_jumlah_jam_kerja': task_obj.suggest_jumlah_jam_kerja,
                                'realisasi_hadir_hari_kerja'     : task_obj.suggest_hadir_hari_kerja,
                                'realisasi_hadir_jam_kerja': task_obj.suggest_hadir_jam_kerja,
                                'realisasi_hadir_apel_pagi': task_obj.suggest_hadir_apel_pagi,
                                
                                'realisasi_integritas_presiden': task_obj.suggest_integritas_presiden,
                                'realisasi_integritas_gubernur': task_obj.suggest_integritas_gubernur,
                                'realisasi_integritas_kepalaopd': task_obj.suggest_integritas_kepalaopd,
                                'realisasi_integritas_atasan': task_obj.suggest_integritas_atasan,
                                'realisasi_integritas_es1': task_obj.suggest_integritas_es1,
                                'realisasi_integritas_es2': task_obj.suggest_integritas_es2,
                                'realisasi_integritas_es3': task_obj.suggest_integritas_es3,
                                'realisasi_integritas_es4': task_obj.suggest_integritas_es4,
                                
                                'realisasi_integritas_hukuman': task_obj.suggest_integritas_hukuman,
                                'realisasi_integritas_hukuman_ringan': task_obj.suggest_integritas_hukuman_ringan,
                                'realisasi_integritas_hukuman_sedang': task_obj.suggest_integritas_hukuman_sedang,
                                'realisasi_integritas_hukuman_berat': task_obj.suggest_integritas_hukuman_berat,
                                
                                'realisasi_kerjasama_nasional': task_obj.suggest_kerjasama_nasional,
                                'realisasi_kerjasama_gubernur': task_obj.suggest_kerjasama_gubernur,
                                'realisasi_kerjasama_kepalaopd': task_obj.suggest_kerjasama_kepalaopd,
                                'realisasi_kerjasama_atasan':task_obj.suggest_kerjasama_atasan,
                                'realisasi_kerjasama_rapat_nasional': task_obj.suggest_kerjasama_rapat_nasional,
                                'realisasi_kerjasama_rapat_provinsi': task_obj.suggest_kerjasama_rapat_provinsi,
                                'realisasi_kerjasama_rapat_perangkat_daerah': task_obj.suggest_kerjasama_rapat_perangkat_daerah,
                                'realisasi_kerjasama_rapat_atasan': task_obj.suggest_kerjasama_rapat_atasan,
                                
                                'realisasi_kepemimpinan_nasional': task_obj.suggest_kepemimpinan_nasional,
                                'realisasi_kepemimpinan_gubernur': task_obj.suggest_kepemimpinan_gubernur,
                                'realisasi_kepemimpinan_kepalaopd': task_obj.suggest_kepemimpinan_kepalaopd,
                                'realisasi_kepemimpinan_atasan':  task_obj.suggest_kepemimpinan_atasan,
                                'realisasi_kepemimpinan_narsum_nasional': task_obj.suggest_kepemimpinan_narsum_nasional,
                                'realisasi_kepemimpinan_narsum_provinsi': task_obj.suggest_kepemimpinan_narsum_provinsi,
                                'realisasi_kepemimpinan_narsum_perangkat_daerah': task_obj.suggest_kepemimpinan_narsum_perangkat_daerah,
                                'realisasi_kepemimpinan_narsum_unitkerja': task_obj.suggest_kepemimpinan_narsum_unitkerja,
                                })               
                # end if               
                self.write(cr, uid, [task_obj.id], vals, context)
        # end for
        return True
    def fill_task_appeal_automatically_with_suggest(self, cr, uid, ids, context=None):
        """ Default Target di isi task"""
        if not isinstance(ids, list): ids = [ids]
        for task_obj in self.browse(cr, uid, ids, context=context):
                vals = {}
                
                vals.update({
                                'appeal_jumlah_konsumen_pelayanan'       : task_obj.suggest_jumlah_konsumen_pelayanan,
                                'appeal_satuan_jumlah_konsumen_pelayanan': task_obj.suggest_satuan_jumlah_konsumen_pelayanan.id or None,
                                'appeal_jumlah_tidakpuas_pelayanan'      : task_obj.suggest_jumlah_tidakpuas_pelayanan,
                                'appeal_ketepatan_laporan_spj'           : task_obj.suggest_ketepatan_laporan_spj.id or None,
                                'appeal_ketepatan_laporan_ukp4'          : task_obj.suggest_ketepatan_laporan_ukp4.id or None,
                                'appeal_efisiensi_biaya_operasional'     : task_obj.suggest_efisiensi_biaya_operasional.id or None,
                                
                                #'appeal_apel_pagi'     : task_obj.suggest_apel_pagi,
                                #s'appeal_upacara_hari_besar': task_obj.suggest_upacara_hari_besar,
                                'appeal_hadir_upacara_hari_besar': task_obj.suggest_hadir_upacara_hari_besar,
                                #'appeal_jumlah_hari_kerja'     : task_obj.suggest_jumlah_hari_kerja,
                                #'appeal_jumlah_jam_kerja': task_obj.suggest_jumlah_jam_kerja,
                                'appeal_hadir_hari_kerja'     : task_obj.suggest_hadir_hari_kerja,
                                'appeal_hadir_jam_kerja': task_obj.suggest_hadir_jam_kerja,
                                'appeal_hadir_apel_pagi': task_obj.suggest_hadir_apel_pagi,
                                
                                'appeal_integritas_presiden': task_obj.suggest_integritas_presiden,
                                'appeal_integritas_gubernur': task_obj.suggest_integritas_gubernur,
                                'appeal_integritas_kepalaopd': task_obj.suggest_integritas_kepalaopd,
                                'appeal_integritas_atasan': task_obj.suggest_integritas_atasan,
                                'appeal_integritas_es1': task_obj.suggest_integritas_es1,
                                'appeal_integritas_es2': task_obj.suggest_integritas_es2,
                                'appeal_integritas_es3': task_obj.suggest_integritas_es3,
                                'appeal_integritas_es4': task_obj.suggest_integritas_es4,
                                
                                'appeal_integritas_hukuman': task_obj.suggest_integritas_hukuman,
                                'appeal_integritas_hukuman_ringan': task_obj.suggest_integritas_hukuman_ringan,
                                'appeal_integritas_hukuman_sedang': task_obj.suggest_integritas_hukuman_sedang,
                                'appeal_integritas_hukuman_berat': task_obj.suggest_integritas_hukuman_berat,
                                
                                'appeal_kerjasama_nasional': task_obj.suggest_kerjasama_nasional,
                                'appeal_kerjasama_gubernur': task_obj.suggest_kerjasama_gubernur,
                                'appeal_kerjasama_kepalaopd': task_obj.suggest_kerjasama_kepalaopd,
                                'appeal_kerjasama_atasan':task_obj.suggest_kerjasama_atasan,
                                'appeal_kerjasama_rapat_nasional': task_obj.suggest_kerjasama_rapat_nasional,
                                'appeal_kerjasama_rapat_provinsi': task_obj.suggest_kerjasama_rapat_provinsi,
                                'appeal_kerjasama_rapat_perangkat_daerah': task_obj.suggest_kerjasama_rapat_perangkat_daerah,
                                'appeal_kerjasama_rapat_atasan': task_obj.suggest_kerjasama_rapat_atasan,
                                
                                'appeal_kepemimpinan_nasional': task_obj.suggest_kepemimpinan_nasional,
                                'appeal_kepemimpinan_gubernur': task_obj.suggest_kepemimpinan_gubernur,
                                'appeal_kepemimpinan_kepalaopd': task_obj.suggest_kepemimpinan_kepalaopd,
                                'appeal_kepemimpinan_atasan':  task_obj.suggest_kepemimpinan_atasan,
                                'appeal_kepemimpinan_narsum_nasional': task_obj.suggest_kepemimpinan_narsum_nasional,
                                'appeal_kepemimpinan_narsum_provinsi': task_obj.suggest_kepemimpinan_narsum_provinsi,
                                'appeal_kepemimpinan_narsum_perangkat_daerah': task_obj.suggest_kepemimpinan_narsum_perangkat_daerah,
                                'appeal_kepemimpinan_narsum_unitkerja': task_obj.suggest_kepemimpinan_narsum_unitkerja,
                                })               
                # end if               
                self.write(cr, uid, [task_obj.id], vals, context)
        # end for
        return True
    
    
project_perilaku()