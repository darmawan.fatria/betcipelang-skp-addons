from openerp.osv import fields, osv
from openerp.tools.translate import _
from openerp import pooler

class project_skp_massive(osv.Model):
    _name = "project.skp.massive"
    _description = "Proses Select All SKP"
    def verify_all_task(self, cr, uid, ids, context=None):
        
        if context is None:
            context = {}
        pool_obj = pooler.get_pool(cr.dbname)
        task_pool = pool_obj.get('project.skp')
        task_ids = task_pool.read(cr, uid, context['active_ids'], ['state','target_realiasi_notsame','biaya_verifikasi_notsame','project_id','project_state','employee_id','employee_job_type'], context=context)
        
        update_task_ids=[]
        for record in task_ids:
            if not record['employee_id'] :
                continue;
            if record['project_id'] and record['project_state'] not in( 'confirm','closed'):
                continue;
            if record['state'] not in ('evaluated'):
                continue;
            if record['target_realiasi_notsame']:
                continue;
            if record['biaya_verifikasi_notsame']:
                continue;
            if task_pool.get_auth_id(cr, uid, record['id'], 'user_id_bkd', context):
                update_task_ids.append(record['id'])
        
        if update_task_ids:
           task_pool.action_done(cr, uid, update_task_ids, context)
                
                
        return {'type': 'ir.actions.act_window_close'}
   

project_skp_massive()
class project_skp_evaluated_massive(osv.Model):
    _name = "project.skp.evaluated.massive"
    _description = "Proses Select All SKP | Atasan-BKD"
    def evaluated_all_task(self, cr, uid, ids, context=None):
        
        if context is None:
            context = {}
        pool_obj = pooler.get_pool(cr.dbname)
        task_pool = pool_obj.get('project.skp')
        task_ids = task_pool.read(cr, uid, context['active_ids'], ['state','project_id','project_state','employee_id','employee_job_type'], context=context)
        
        update_task_ids=[]
        for record in task_ids:
            if not record['employee_id'] :
                continue;
            if record['project_id'] and record['project_state'] not in( 'confirm','closed'):
                continue;
            if record['state'] not in ('propose'):
                continue;
            update_task_ids.append(record['id'])
        
        if update_task_ids:
           task_pool.set_evaluated(cr, 1, update_task_ids, context)
                
                
        return {'type': 'ir.actions.act_window_close'}
   

project_skp_evaluated_massive()

class project_skp_evaluated_massive(osv.Model):
    _name = "project.skp.evaluated.massive"
    _description = "Proses Select All SKP | Atasan-BKD"
    def evaluated_all_task(self, cr, uid, ids, context=None):
        
        if context is None:
            context = {}
        pool_obj = pooler.get_pool(cr.dbname)
        task_pool = pool_obj.get('project.skp')
        task_ids = task_pool.read(cr, uid, context['active_ids'], ['state','project_id','project_state','employee_id','employee_job_type'], context=context)
        
        update_task_ids=[]
        for record in task_ids:
            if not record['employee_id'] :
                continue;
            if record['project_id'] and record['project_state'] not in( 'confirm','closed'):
                continue;
            if record['state'] not in ('propose'):
                continue;
            update_task_ids.append(record['id'])
        
        if update_task_ids:
           task_pool.set_evaluated(cr, 1, update_task_ids, context)
                
                
        return {'type': 'ir.actions.act_window_close'}
   

project_skp_evaluated_massive()

class project_skp_recalculated_massive(osv.Model):
    _name = "project.skp.recalculated.massive"
    _description = "Proses Select All SKP | Hitung Ulang"
    def recalculated_all_task(self, cr, uid, ids, context=None):
        
        if context is None:
            context = {}
        pool_obj = pooler.get_pool(cr.dbname)
        task_pool = pool_obj.get('project.skp')
        task_ids = task_pool.read(cr, uid, context['active_ids'], ['state','project_id','project_state','employee_id','employee_job_type'], context=context)
        
        update_task_ids=[]
        for record in task_ids:
            if not record['employee_id'] :
                continue;
            if record['project_id'] and record['project_state'] not in( 'confirm','closed'):
                continue;
            if record['state'] not in ('done'):
                continue;
            update_task_ids.append(record['id'])
        
        if update_task_ids:
           task_pool.do_recalculate_poin(cr, 1, update_task_ids, context)
                
                
        return {'type': 'ir.actions.act_window_close'}
   

project_skp_recalculated_massive()


class project_skp_temp_calc_massive(osv.Model):
    _name = "project.skp.temp.calc.massive"
    _description = "Proses Select All SKP | Hitung Nilai Sementara"
    def poin_calculation_temporary_all_task(self, cr, uid, ids, context=None):
        
        if context is None:
            context = {}
        pool_obj = pooler.get_pool(cr.dbname)
        task_pool = pool_obj.get('project.skp')
        task_ids = task_pool.read(cr, uid, context['active_ids'], ['state','project_id','project_state','employee_id',], context=context)
        
        update_task_ids=[]
        for record in task_ids:
            if not record['employee_id'] :
                continue;
            if record['project_id'] and record['project_state'] not in( 'confirm','closed'):
                continue;
            if record['state'] not in ('evaluated'):
                continue;
            update_task_ids.append(record['id'])
        
        if update_task_ids:
           task_pool.do_task_poin_calculation_temporary(cr, uid, update_task_ids, context)
                
                
        return {'type': 'ir.actions.act_window_close'}
   

project_skp_temp_calc_massive()


